# PASTE (Platform for Automated Simulation and Tools in Epidemiology)

![Version](https://img.shields.io/badge/version-1.0-f16152.svg)
![License](https://img.shields.io/badge/license-Apache--2.0-8cd0c3.svg)

This README file describes the installation of decision tools according to yaml file and how to run it locally.

# License

PASTE is released under the [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0) license (see files `LICENSE` and `NOTICE`)

# General Information

This software pipeline is aimed at generating a web architecture for **decision-support tools** based on epidemiological models developed with framework **EMULSION**.
Framework EMULSION is intended for modellers in epidemiology, to help them design,
simulate, and revise complex mechanistic **stochastic models**, without having
to write or rewrite huge amounts of code.

## INPUT FILES

  - EMULSION model input file
  - YAML File to describe the tool
  - CSV files linked to the block parameters in the tool description yaml file
  - The README: steps for installing the application
  - The required modules to install for the application
  - Examples of django files that are generated when the tool description yaml file is parsed
  - Some static files containing the logos and pages CSS description

## Requirements

## On remote server
### Install R

1. Check the version of your R (version == R version 3.6.1 (2019-07-05))   

       R --version

2. If not installed, run

       sudo apt install r-base

3. Install R packages, run

       sudo R -e "install.packages('shiny', repos='https://cran.rstudio.com/')"
       sudo R -e "install.packages('shinythemes', repos='https://cran.rstudio.com/')"
       sudo R -e "install.packages('shinyBS', repos='https://cran.rstudio.com/')"
       sudo R -e "install.packages('tidyverse', repos='https://cran.rstudio.com/')"
       sudo R -e "install.packages('readr', repos='https://cran.rstudio.com/')"
       sudo R -e "install.packages('dplyr', repos='https://cran.rstudio.com/')"
       sudo R -e "install.packages('tidyr', repos='https://cran.rstudio.com/')"
       sudo R -e "install.packages('ggplot2', repos='https://cran.rstudio.com/')"


### Install Redis server

- On Linux:

       sudo apt install redis-server

- On MacOS, with `homebrew`:

       brew install redis

### Install Shiny server

- On Linux:

       sudo apt-get install gdebi-core
       wget https://download3.rstudio.org/ubuntu-18.04/x86_64/shiny-server-1.5.20.1002-amd64.deb
       sudo gdebi shiny-server-1.5.20.1002-amd64.deb

## On remote and local server 

### Install Python3

    1. Check the version of your python (version >= Python 3.7.5)   

            python3 -V

    2. If not installed, run

            sudo apt-get install python3

### Install Pip3

    1. Check the version of your pip3 (version >= pip 20.3.3)   

            pip3 --version

    2. If not installed, run

            sudo apt-get install python3-pip

    3. (Optional) Upgrade the module installer

            python3 -m pip install --upgrade pip

PS: If you don't want to use a virtual environment, you can still install everything locally.
I will recommend you to create a virtual environment. It is better to avoid conflicts between modules.

### Create a virtual environment

    1. Check if you have a virtual environment (version >= 20.2.2)

            pip show virtualenv  

    2. If not installed, run

            sudo pip3 install virtualenv

    3. If installed, create a virtual environment

            python3 -m venv <myenvname>

    4. Activate the virtual environment

            source <myenvname>/bin/activate

## On the local server

### Clone the **PASTE** repository 

From Sourcesup (replace `<username>` by your username on SourceSup)

    1. Check if you have git installed (version >= 2.20.1)

            git --version

    2. If not installed, run

            sudo apt install git

    3. Clone the PASTE repository:

        - From Forgemia:

            git clone git@forgemia.inra.fr:dynamo/software/paste-public-release.git

    4. Go to paste directory. This directory contains the **PASTE_CORE** directory.

            cd paste-public-release/

    PS: Make sure you have an account or Forgemia and that you have rights on the folder you want to clone.

    Do not forget to activate ssh key adding it to ssh configuration ssh-add ~/.ssh/id_rsa_inra

## On the local server and remote server

### Install the python3 requirements packages

      1. Run the requirements file from the documentation directory.

              pip3 install -r PASTE_CORE/documentation/requirements_important_modules.txt

      This command will install the following components :
  - django
  - emulsion==1.2b13
  - django-formtools 
  - django-select2
  - django-mathfilters
  - channels
  - Celery
  - redis
  - redis-server
  - bs4
  - requests
  - ansicolors


PS: If you have an error like:    
  ``ERROR: Failed building wheel for <a module>`` and ``error: invalid command 'bdist_wheel'``        
    No worries, that means pip tries to build a wheel for the module via **setup.py bdist_wheel**, but it fails.
    Then, pip falls back and install it directly using **setup.py install**.      
    Please have a look at the last line in the terminal. If it says that everything is installed
    successfully, then everything went well. If you are not sure try rerun the
    modules installation command, you will see that all the requirements are already satisfied.        
<br>

## On the local server

## Modify the tool description yaml file ``paste_specification/paste.yaml`` according to your needs.
NB: The paste_specification folder can be copied in any wanted folder. You just have to make sure to inform the path to the folder.
- In **PASTE_CORE** directory, open paste.yaml file in paste_specification directory using an editor.

    1. In section ``django_project_name: paste_project``, replace ``paste_project`` with your project name.

    2. In section ``django_app_name: paste_app``, replace ``paste_app`` with your application name.

    3. Make sure you entered the path to your csv input files (corresponds to block_values) correctly, i.e. relatively to the paste_specification directory.

        For example, for a csv file "input_files.csv" in the paste_specification/input_files directory, the path will be "paste_specification/input_files/input_files.csv"  

        PS: For the CSV files, look at the examples in PASTE_CORE/paste_specification/input_files directory to create yours
        and be aware of the headers. For example, if a *basin* contains multiple *breeds*, add "breed" as foreign
        key in the ``basin.csv`` file to make the link between the two. The breeds must be separated by "|" in ``basin.csv``
        Having the same name between foreign key and name of the block_values in the paste.yaml file is important.
  
       
## Create Django project and application according to paste.yaml file

- Run the script create_app.sh from the **paste-public-release/** directory to create the django application.
    If you rerun this script to create a new django project, if the name of your django project is the same.
    It is removed and replaced by the new one.
    
    The paste_specification directory <path_to_paste_specification> containing input files: the paste.yaml file and the csv files for block_values and 
    the django project directory <path_to_django_project> to be created can be located anywhere in your path.
    Make sure to inform the path for these two directories before executing create_app.sh script to create your django application. 
    The create_app.sh script uses management, static and paste_specification directories from PASTE_CORE.

        chmod +x create_app.sh
        ./create_app.sh -i <path_to_paste_specification> -d <path_to_django_project>
    
- Go to the django project directory that correspond to the **base directory**.

        cd <path_to_django_project>/<project_name>

- Modify the django settings file  **settings_paste.py** by replacing localhost by remote machine IP address or its name in Channels and celery configuration
  
      CHANNEL_LAYERS = {
          'default': {
          'BACKEND': 'channels_redis.core.RedisChannelLayer',
          'CONFIG': {
          'hosts': [('redis://remote_machine_ip:port_number/0'),],
      }
      }
      }
      CELERY_BROKER_URL = 'redis://remote_machine_ip:port_number/0'
      CELERY_RESULT_BACKEND = 'redis://remote_machine_ip:port_number/0'
    
PS: Make sure you entered the right port number if you have redirection

- Export the new Django settings file created. This file contains the configuration of Celery and static files. Run the command from the **base directory**.

        export DJANGO_SETTINGS_MODULE=<project_name>.settings_paste

- Run the Django development server. Run the command from the **base directory**.
  
        python3 manage.py runserver

- Open a web browser with the following url:

        http://127.0.0.1:8000
  
PS: Do not forget to create and activate virtual environment as you did previously.

## On the remote server

### For Celery worker to run on remote machine

- Copy the django project from local to the remote machine

- Create a virtual environment as you did previously on the local machine

- Install requirements as you did previously on the local machine

- Go to the django project **base directory**.

- Modify the django settings project file **settings_paste.py** by replacing localhost with the remote machine ip address and port number in Channels and celery configuration or
with the name of your remote machine

      CHANNEL_LAYERS = {
      'default': {
              'BACKEND': 'channels_redis.core.RedisChannelLayer',
              'CONFIG': {
              'hosts': [('redis://remote_machine_ip:port_number/0'),],
                    }
              }
      }
      CELERY_BROKER_URL = 'redis://remote_machine_ip:port_number/0'
      CELERY_RESULT_BACKEND = 'redis://remote_machine_ip:port_number/0'

PS: Make sure you entered the right port number if you have redirection

- Change in views.py file and tasks.py file, the path to the input files to match the path of your django project **base directory** into your remote machine.
The variable is called BASE_DIR in both files.

- Export django project settings. Run this command from the **base directory**

      export DJANGO_SETTINGS_MODULE=<project_name>.settings_paste

- Make sure you Allowed incoming connections from local machine on port 6379 for redis/celery worker by running this command

      sudo apt-get install ufw
      sudo ufw allow 6379/tcp
      sudo ufw enable
      sudo ufw status

- Open new terminal and Run redis-server to be sure that it is started or to start it

         redis-server --protected-mode no

- Run the Celery worker. Run this command from the django project **base directory** 

         celery -A <project_name> worker -l info

By doing these steps, when you go back to your website of django project in the local machine, 
as you submit the form the celery tasks will be sent and ran in your remote machine.

### For Shiny application

## On the remote machine

-Log into your remote machine

      sudo ufw allow <port_number>

- Copy shinyapp directory containing app.R into /srv/shiny-server directory

- Modify shinyapp/app.R file DJANGO_PROJECT_DIR to match the path of django project **base directory**  in the remote machine

- Make sure you Allowed incoming connections from local machine on port 3838 for shiny app by running this command
      
      sudo ufw allow 3838/tcp
      sudo ufw status

- Run the Shiny server

      sudo service shiny-server start

- Verify status of the shiny server
  
      sudo service shiny-server start

## on the local machine

- Modify django project views.py file by changing shiny_contents function **remote_machine_ip** and **port_number**:

      def shiny_contents(request):
          response = requests.get('http://remote_machine_ip:port_number/shinyapp/')
          soup = bs4.BeautifulSoup(response.content, 'html.parser')
      return JsonResponse({'html_contents': str(soup)})

- Modify also in django project templates file shiny.html  by changing shiny_contents function **remote_machine_ip** and **port_number** in the url of the iframe
        
      <iframe src="http://remote_machine_ip:port_number/shinyapp/" style="top:0; left:0; bottom:0; right:0; width:100%; height:100%; border:none; margin:0; padding:0; overflow:hidden; z-index:999999;">
          Your browser doesn't support iframes
      </iframe>

By doing these steps, your shiny app will be redirected to your app.R located in your remote machine

- Go back to the local django project website and navigate through the form, submit the form, laugh celery tasks and get graphics in shiny app

- PASTE website

When you open the paste website, the first page corresponds to the form. The form has many pages, each page correspond to
input_forms informed in the paste.yaml file. A scenario can correspond to one or multiple page.

The step number indicates in which page number of the form we are, out of how many pages are there in the form.
The icons Next and Previous allow user to navigate through the different pages of the form.
And the Icon run submit the form.

When you reach the last page, as you submit the form, the EMULSION simulation runs in the remote machine and the result files added in subdiretories (which is named 
according to simulation name) in the emulsion outputs directory that is located in the **base directory**. 


# Useful Links

1. Django
    - Tutorial: [https://docs.djangoproject.com/en/3.0/](https://docs.djangoproject.com/en/3.0/)
    - Licence: [https://github.com/django/django/blob/master/LICENSE](https://github.com/django/django/blob/master/LICENSE)

2. Celery
    - Tutorial: [https://docs.celeryproject.org/en/master/django/first-steps-with-django.html](https://docs.celeryproject.org/en/master/django/first-steps-with-django.html)
    - Licence: [https://github.com/celery/celery/blob/master/LICENSE](https://github.com/celery/celery/blob/master/LICENSE)

3. Redis
    - Tutorial: [https://redis.io](https://redis.io)
    - Licence: [https://redis.io/docs/about/license/](https://redis.io/docs/about/license/)

4. Channels
    - Tutorial: [https://channels.readthedocs.io/en/stable/introduction.html](https://channels.readthedocs.io/en/stable/introduction.html)
    - Licence: [https://github.com/django/channels/blob/main/LICENSE](https://github.com/django/channels/blob/main/LICENSE)

5. Requests
   - Tutorial: [https://requests.readthedocs.io/en/latest/](https://requests.readthedocs.io/en/latest/)
   - Licence: [https://github.com/psf/requests/blob/main/LICENSE](https://github.com/psf/requests/blob/main/LICENSE)
