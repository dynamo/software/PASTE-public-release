#!/bin/bash

echo "###################################################################################################"
echo "#Script Name : create_app.sh                                                                      #"
echo "#Description : The aim of this script is to create the paste app according to your paste.yaml file  #"
echo "#Email : guita.niang@inrae.fr                                                                     #"
echo "###################################################################################################"

# Shell script for create the paste Django project.

# Colors
red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`

echo -e "\n"

# Get the name of the OS
OS=`uname`

Help()
{
   # Display Help
   echo "To run the script inform the following arguments (i, d)"
   echo
   echo "Syntax: scriptTemplate [-i|d|h]"
   echo "options:"
   echo "i     Add the path to paste specification containing input files (paste.yaml and CSV files)."
   echo "d     Add the path where the django project directory will be created."
   echo "h     Print Help."
   echo "Example:  ./create_app.sh -i <path_to_paste_specification> -d <path_to_django_project>"
}

if [ $# -eq 0 ]
  then
    echo "No arguments supplied"
    echo 'Expecting "i" arg for path to input file and "d" arg for django project path' >&2
       exit 1
fi

while getopts i:d:h flag
do
    case "${flag}" in
        i) input_dir_path=${OPTARG};;
        d) django_dir_path=${OPTARG};;
        h) Help
          exit;;
        \?) # Invalid option
         echo "Error: Invalid option"
         exit;;
        *) echo 'Expecting "i" arg for path to input file and "d" arg for django project path' >&2
       exit 1 ;;
    esac
done

paste_pwd=$(pwd)
# if not MacOS, alias gsed => regular sed, otherwise test that GNU sed (gsed) is installed
if [[ "${OS}" == "Darwin" ]]
then
    echo "Running create_app under MacOS..."
    GSED=`which gsed`
    if [ -z "${GSED}" ]
    then
	echo -e "${red}No path found for GNU sed.{$reset}\nPlease make sure GNU sed is installed (e.g. brew install gnu-sed)"
	exit 0
    else
	echo "${green}GNU sed is installed.${reset}"
    fi
else
    GSED='sed'
fi

PROJECT=$(grep "django_project_name:" "$input_dir_path/paste.yaml" | ${GSED} 's/django_project_name://' | ${GSED} 's/ //')
echo "You have entered ${green}$PROJECT${reset} for the name of your django project in paste.yaml file"

if [[ -d "$django_dir_path/$PROJECT" ]]
then

    echo "The directory with the project name ${green}$PROJECT${reset} already exist on your filesystem."
    echo "${red}It will be deleted and a new on will be created...${reset}"
    read -p "Are you sure you want to continue? <y/N> " prompt
    if [[ $prompt =~ [yY](es)* ]]
    then

    	echo "${green}>>> Remove djangoproject${reset}"

      rm -rf $django_dir_path/$PROJECT
      mkdir -p $django_dir_path/$PROJECT
      cd "$django_dir_path/$PROJECT" || exit ## Moving to django project directory
      export DJANGO_SETTINGS_MODULE=$PROJECT.settings
      django-admin startproject $PROJECT $django_dir_path/$PROJECT
    else
      echo "${green}>>> Open the paste.yaml file and modify it accordingly to the project name you want :)${reset}"
      exit 0
    fi
else

	mkdir -p $django_dir_path/$PROJECT
	cd "$django_dir_path/$PROJECT" || exit ## Moving to django project directory
	django-admin startproject $PROJECT $django_dir_path/$PROJECT

fi

echo "${green}>>> Creating the app ...${reset}"
appname=$(grep "django_app_name:" "$input_dir_path/paste.yaml" | ${GSED} 's/django_app_name://' | ${GSED} 's/ //')

echo "You have entered ${green}$appname${reset} for the name of your django application"
read -p "Are you sure you want to continue? <y/N> " prompt
cd $django_dir_path/$PROJECT/ || exit
if [[ $prompt =~ [yY](es)* ]]
then
  pwd
  python3 manage.py startapp $appname

else
  echo "${green}>>> Open the paste.yaml file and modify it accordingly to the application name you want :)${reset}"
  exit 0
fi

echo "${green}>>> Copy management directory from ``PASTE_CORE`` directory into application folder ...${reset}"

cp -r $paste_pwd/PASTE_CORE/management $django_dir_path/$PROJECT/$appname
# Copy ``static`` directory also into application folder.

echo "${green}>>> Copy ``static`` directory also into application folder ...${reset}"
cp -r $paste_pwd/PASTE_CORE/static $django_dir_path/$PROJECT/$appname

# Copy (or create) ``paste_specification`` directory (containing ``paste.yaml`` file and CSV files) in the project folder.

echo "${green}>>> Copy (or create) ``paste_specification`` directory (containing ``paste.yaml`` file and CSV files) in the project folder ...${reset}"
cp -r $input_dir_path $django_dir_path/$PROJECT/


var=$(echo $appname | ${GSED} 's/[^_]\+/\L\u&/g' | ${GSED} 's/_//' | ${GSED} -e "s/\(.*\)/\1Config/")

echo "${green}>>> Editing project settings.py by adding application name ...${reset}"
${GSED} -i "/django.contrib.staticfiles/a\    '$appname.apps.$var'," $django_dir_path/$PROJECT/$PROJECT/settings.py

# # migrate
echo "${green}>>> Running Migrate ...${reset}"
python3 manage.py makemigrations
python3 manage.py migrate

echo "${green}>>> Run the command to create application files according to yaml file ...${reset}"
python3 manage.py parse_yaml_file --filename "$input_dir_path/paste.yaml" --inputdir "$input_dir_path" | tee log.txt
import=$(grep 'manage.py import_data_file' log.txt)

echo "${green}>>> Running Migrate ...${reset}"
python3 manage.py makemigrations
python3 manage.py migrate

echo "${green}>>> Import block_values data in the database using the csv files ...${reset}"
python3 $import

#Export the new Django settings file created. This file contains the configuration of Celery and static files. Run the command from the **base directory**.

echo "${green}>>> Export the new Django settings file created. This file contains the configuration of Celery and static files ...${reset}"
export DJANGO_SETTINGS_MODULE=$PROJECT.settings_paste

#Collect static files especially for CSS and images. Run the command from the **base directory**.

echo "${green}>>> Collect static files especially for CSS and images ...${reset}"
python3 manage.py collectstatic

echo -e "\n"
echo "${green}>>> Congratulations, you have successfully done parsing the paste.yaml file${reset}"
