This README file explain how the django project files are created by parsing the paste.yaml file.

# PASTE file
- **django_project_name:**
  - Used in settings files of the django project: 
    - brd_project/brd_project/asgi.py 
    - brd_project/brd_project/celery.py 
    - brd_project/brd_project/myjson.py 
    - brd_project/brd_project/routing.py 
    - brd_project/brd_project/settings.py
    - brd_project/brd_project/settings_paste.py 
    - brd_project/brd_project/urls.py 
    - brd_project/brd_project/wsgi.py
    
- **emulsion_setup:**     
    <br>
    Add these emulsion parameters in emulsion command to run in **views.py**      

  - **input_parameters:**<br>
    
      It contains three blocks of parameters: block_values, user_parameters_values and user_parameters_matrices 
      that is used to create **forms.py** and **templates** html file so that user can change there value in the website.  
      <br>
      - Block_values:        
          Used in creating **models.py** , **urls.py** and **import_data_file.py** in management/commands directory.        
        <br>
      - User_parameters_values:
          Added in creating emulsion command to run in **views.py** when it is emulsion parameters or 
          to a dictionary also in **views.py** also when it is non emulsion parameters. 
        <br>
      - User_parameters_matrices:
        Added in creating emulsion command to run in **views.py** when it is emulsion parameters.
        <br>
        
      <br>
- **Scenarios and Scenarios_components:**  
     Used to create many files:
  - Name of scenario associated to each input form template html file. These names are displayed in each input form page of the website.
  - Creating the select_scenario form in **forms.py** according to optional scenarios and associated html template file **select_scenario.html**. And also the associed code 
    in **views.py** to get the list of chosen scenarios by the user.
  - Creating emulsion command to run according to each scenario parameters in **views.py** for emulsion parameters and non emulsion parameters.
    Non emulsion parameters is also used in **views.py**.
  - Name of the scenario is used to name the output directory of each emulsion run output directory in **views.py**. 
  - Creating lists of scenario chosen by the user (optional: yes) which is given as parameters in **tasks.py**.

- **input_forms:**
  - Creating fields in **forms.py** (new fields or input_parameters fields) and associated name html templates with the name of the input forms 
  - Input form fields associated to each scenario in **views.py**

- **statistics:**
  - Creating **statistics.R** and **statistics_comparison_scenarios.R**
  
- **graphics:**
  - Creating shiny app **app.R**

Some other files like **base.html**, **done.html**, **result.html**, **shiny.html**, **tasks.py** are files that 
does not need the paste.yaml to be created.
