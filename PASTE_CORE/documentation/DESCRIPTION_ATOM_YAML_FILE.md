This README file explain how to write the paste.yaml file description tool.

# General Information

The aim of this project is developing the software 
infrastructure to automatize the production of **decision tools** from the framework **EMULSION** epidemiological models.
Framework EMULSION is intended for modellers in epidemiology, to help them design,
simulate, and revise complex mechanistic **stochastic models**, without having 
to write or rewrite huge amounts of code.

- **Main contributors and contact:**
    - Guita Niang (`guita.niang@inrae.fr`)
    - Sébastien Picault (`sebastien.picault@inrae.fr`)
    - Vianney Sicard (`vianney.sicard@inrae.fr`)
    - Pauline Ezanno (`pauline.ezanno@inrae.fr`)
    


# Main blocks parameters
The keywords will be represented in bold and red. The optional values are only in black and can be changed by the user as desired.

##Django and emulsion settings:

<pre>
    <b>django_project_name</b>: paste_project 
    
    <b>django_app_name</b>: paste_app
    
    <b>input_files_path</b>:
      <b>path</b>: paste_specification/input_files
    
    <b>model_emulsion_file</b>:
      <b>file_name</b>: model_emulsion_file.yaml
    
    <b>emulsion_output_directory</b>:
      <b>path</b>: outputs/
</pre>
##emulsion_setup:   
    <br>
    It contains emulsion parameters that are shared by all emulsion scenarios and the syntax of emulsion to run the command.
<pre>
    <b>emulsion_setup</b>:

      model_name: BRD
      number_of_repetitions: 20
      figure_text_size: 20
    
      emulsion_syntax:
        command: 'emulsion run'
        runs: '-r'
        outputs: '--output-dir'
        options: '--silent' # autres options
        params: '-p'
</pre>

##input_parameters:
<br>
<br>
It contains three blocks of parameters: block_values, user_parameters_values and user_parameters_matrices. 
   
- block_values:        
    These parameters can be chained or not to each other.
    Each parameter is associated to a csv file. 
    If some parameters are linked, then the user uses the name of one parameter as key in the csv file of the other parameter.
    PS: In the example below, basin and breed are chained. For each basin, one or more breed are associated.
<br>
- user_parameters_values:          
    These parameters correspond to emulsion parameters. They can be added as list, integer, float values or boolean values.
<br>
<br>
- user_parameters_matrices:
    These parameters correspond also to emulsion parameters, but they are created using: the name of the matrice, the name of the rows and the name of the columns
    separated with "_". The name resulting from this combination will correspond to a name of a parameter in the emulsion yaml file.

<pre>
<b>input_parameters</b>:
  - <b>block_values</b>:
      basin:
        - <b>file_path</b>: "paste_specification/input_files/basin.csv"
      breed:
        - <b>file_path</b>: "paste_specification/input_files/breed.csv"
      vaccination:
        - <b>file_path</b>: "paste_specification/input_files/vaccination.csv"
      antibacterien:
        - <b>file_path</b>: "paste_specification/input_files/antibacterien.csv"
      

  - <b>user_parameters_values</b>:
    - initial_herd_size: [50, 60, 70, 80, 90]
    - initial_infected: 5
    - transmission_I: 0.1
    - use_antibioprevention: True

  - <b>user_parameters_matrices</b>:
      - matrice_name:
          nb_animals: #### nb_animals_Low_NV verify that these variables exist in Emulsion model parameters
            <b>rows</b>:
              <b>name</b>: "risk level"
              <b>values</b>: ["Low", "Medium", "High"]
            <b>columns</b>:
              <b>name</b>: "vaccinal status"
              <b>values</b>: ["NV", "PV", "SV", "FV"]
            <b>default_value</b>: 0 #0 ou [0,1,2,3]
</pre>

##Scenarios:    

They define different conditions of simulation (scenario) for the same model.   
In scenarios_components, the user changes different emulsion parameters for each scenario. Each scenario is associated to parameters and forms. 
A form can be associated to one or many scenarios.
- Forms: associated forms to a specific scenario       
- Specific_Parameters: can correspond to Emulsion parameters specific to a scenario or to non emulsion parameters
- Option yes or no
<pre>

<b>scenarios_components</b>:
  - health_conditions:
      - no_disease:
          <b>optional</b>: "no"
          proportion_carrier_low: 0
          proportion_carrier_medium: 0
          proportion_carrier_high: 0
      - disease:
          <b>optional</b>: "yes" 

  - batch_composition:
      - mixed_batches:
          <b>optional</b>: "yes"
          <b>forms</b>:
            <b>name</b>:
              - initial_conditions
            <b>specific_parameters</b>:
              - sorted_batches: 0
              - non_emulsion_param: 3

      - sorted_batches:
          <b>optional</b>: "yes"
          <b>forms</b>:
            <b>name</b>:
              - initial_conditions
            <b>specific_parameters</b>:
              - sorted_batches: 1
</pre>

The list of scenarios below allow user to define scenarios to run informed in scenarios_components.
The name of final scenarios are made using the two components (health_conditions and batch_composition) of scenarios_components.
For example here, the final scenarios will be: no_disease_mixed_batches, no_disease_sorted_batches, disease_mixed_batches, disease_sorted_batches. 
Because in scenarios section all components of scenarios_components are added. The user are allowed to use only one component of scenarios_components in scenarios section.

<pre>
<b>scenarios</b>:
  - ["health_conditions", "batch_composition"]
</pre>

##Input_forms:     
They define each page of the global form in the interface. It contains for each step of the forms, fields that corresponds to input_parameters 
(block_values, user_parameters_values, user_parameters_matrices), some other free parameters that doesn't belong to input_parameters.            
    
In the input_form, we have different fields. Each field has different attributes.
The mandatory attributes are: **type**, **label**, **required**, **widget**.
The optional ones are: **help_text** and **widget_attrs**.

- For type = IntegerField, the widget can be:
  - forms.Select if user want a list of integer in the interface 
  - NumberInput represents one-line integer editing text area
  - NumberInput with slider when we add a widget_attrs ({'<b>data-min</b>': 0,'<b>data-step</b>': 1,'<b>data-max</b>': 100})

- For type = FloatField, the widget can be:
  - NumberInput represents one-line decimal editing text area
  - NumberInput with slider when we add a widget_attrs ({'<b>data-min</b>': 0,'<b>data-step</b>': 0.1,'<b>data-max</b>': 100})

- For type = CharField, the widget can be:
  - forms.Select if user want a list of strings in the interface
  - or it can be None. For example, for dropdown dependant fields, the widget and widget_attrs are added automatically while parsing
  - forms.TextInput represents one-line plain-text editing text area.
  - forms.Textarea represents multi-line plain-text editing text area. 

- For type = FileField, the widget can be:
  - forms.FileInput

- For type = BooleanField, the widget can be:
  - CheckboxInput with widget_attrs {'<b>data-toggle</b>':"switchbutton",'<b>data-onlabel</b>':"Yes",'<b>data-offlabel</b>':"No"}

- For matrices fields, the type **Matrix** and the widget **HiddenInput** is mandatory.

<pre>
input_forms:
  - initial_conditions:
      - external_files:
          <b>type</b>: FileField
          <b>label</b>: "Download external files"
          <b>required</b>: False
          <b>widget</b>: forms.FileInput

      - initial_herd_size:
          <b>type</b>: IntegerField
          <b>label</b>: "Initial Herd size"
          <b>required</b>: True
          <b>help_text</b>: "number of initial herd size"
          <b>widget</b>: forms.Select

      - initial_infected:
          <b>type</b>: IntegerField
          <b>label</b>: "Initial infected animals"
          <b>required</b>: True
          <b>help_text</b>: "number of Initial infected animals"
          <b>widget</b>: NumberInput
          <b>widget_attrs</b>: {'<b>data-min</b>': 0,
                         '<b>data-step</b>': 1,
                         '<b>data-max</b>': 100
                        }

      - basin:
          <b>type</b>: CharField
          <b>label</b>: "Basin/region"
          <b>required</b>: False

      - breed:
          <b>type</b>: CharField
          <b>label</b>: "Breed"
          <b>required</b>: False

      - matrice_name:
          <b>type</b>: Matrix
          <b>label</b>: "Name of the matrice 1"
          <b>required</b>: False
          <b>widget</b>: HiddenInput

      - use_antibioprevention:
          <b>type</b>: BooleanField
          <b>label</b>: "Select Antibioprevention"
          <b>required</b>: False
          <b>widget</b>: CheckboxInput
          <b>widget_attrs</b>: {'<b>data-toggle</b>':"switchbutton",
                          '<b>data-onlabel</b>':"Yes",
                          '<b>data-offlabel</b>':"No"}
      - vaccination:
          <b>type</b>: CharField
          <b>label</b>: "Vaccination"
          <b>required</b>: False
          <b>widget</b>: forms.Select

The form execution is mandatory and must have the three fields below.
  - execution:
      - total_duration:
          <b>type</b>: IntegerField
          <b>label</b>: "Simulation duration"
          <b>required</b>: True
          <b>widget</b>: NumberInput
          <b>widget_attrs</b>: { '<b>data-min</b>': 0,
                          '<b>data-step</b>': 1,
                          '<b>data-max</b>': 10 }
      - simulation_name:
          <b>type</b>: CharField
          <b>label</b>: "Simulation name"
          <b>required</b>: True
          <b>widget</b>: forms.TextInput

      - simulation_description:
          <b>type</b>: CharField
          <b>label</b>: "Simulation description"
          <b>required</b>: False
          <b>widget</b>: forms.Textarea
</pre>

##Statistics:

This section will allow adding statistics based on the output counts.csv of each emulsion scenario ran.
All the commands will be transformed after parsing to dplyr R commands. The order of values in the command is important. 

We have two subsections. They are linked together by the name of the command. The commands in for_each_scenario can be reused in the 
comparison_of_scenarios section. Every keyword has special meaning in dplyr R command.
- for_each_scenario: commands is executed for each scenario and can be used in the other section.
  - required keywords: desc, target_variable
  - optional keywords: groupby, summarise, condition, sort, add_new_column, inner_join
- comparison_of_scenarios: a new command can be executed without using the result of command in the for_each_scenario subsection.
  - required keywords: left_join, scenarios_to_compare
  - optional keywords: groupby, summarise, condition, sort, add_new_column, inner_join

**Keywords**

- **target_variable** keyword is added as lists, is used in summarise, add_new_column function.


- **groupby** is also added as lists and lead to **group_by** function in the R dplyr command.


- **sort** is also added as lists and lead to **arrange** function in the R dplyr command.


- **condition** is also added as list and lead to **filter** function in the R dplyr command.


- **inner_join** allow combining already calculated commands with raw data. It is a dictionary with keywords **with** and by **by**. 


- **scenarios_to_compare** is added as list with the name of the scenarios to compare.


- **left_join** is a dictionary with the keywords **with** and **by**. In the keyword **with**, we can add 
a name of a command that is already calculated or add the keyword **raw data** to use raw data directly. For the 
**by** keyword, we add a list of columns in which the join will be done.


- In the keyword **summarise**: 
  - You can add a function name related to R dplyr function like (median, mean, max, min, etc). If the function 
  must be written median with parenthesis () and the target variable (I) for example median(I), then add in summarise only the name
  of the function, here median. The name of the function is then modified according to **target_variable** as follows: 
  <**name-of-the-function_target_variable**>. The user can add multiple functions as lists.
  This will lead for example to : mean_**target_variable** = mean(<**target_variable**>). 
  <br><br>
  - You can also add another specific functions as dictionaries. The keywords of the dictionary is **value** and **desc**. 
The value corresponds to the R dplyr function. If the function must be written for example quantile(I, 0.05), then add in
summarise quantile(**...**, 0.05). The **...** is related to the **target_variable**. The name of the dictionary is also modified according 
to **target_variable** as follows: <**name-of-the-function_target_variable**>. After parsing the dictionary, the function will be 
perc_0.05_<**target_variable**> = quantile(<**target_variable**>, 0.05). 
  <br><br>
  - You can also have functions as dictionaries where the value of the keyword **value** does not have
  "**...**" in his expression. So here, we only modify the name of the function by adding the **target_variable** value and then affect the 
  function value as it is written in the yaml.  The final expression of the mutate function will: **summarise(Name_of_dictionary = expression)** 
  For example: ratio_I  = sum(Is_infected)/!!NB.SIMU. <br>
<br>
- **Add_new_column** is written as dictionary too and corresponds to mutate R dplyr function. The name of the dictionary is used as it is. The keywords
of the dictionary are **value** and **desc**. The final expression of the mutate function will: **mutate(Name_of_dictionary = keyword_value)**.
Example: mutate(Is_infected=I>0)



<pre>
<b>statistics</b>:
 <b>for_each_scenario</b>:
      - command1:
          - <b>desc</b>: "maximum value of infected animals"
          - <b>target_variable</b>: [ "I" ]
          - <b>groupby</b>: [ "simu_id","step" ]
          - <b>summarise</b>:
              - median
              - mean
              - perc_0.5:
                    <b>value</b>: "quantile(..., 0.05)" # quantile(I,0.05) selon target_variable
                    <b>desc</b>: "quantile of 5%" ## penser à faire la différence entre la fonction quantile et ratio dans persitence_t
              - perc_0.95:
                    <b>value</b>: "quantile(..., 0.95)"
                    <b>desc</b>: "quantile of 95%"
          - <b>sort</b>: [ "median_I" ]

      - command2:
          - <b>desc</b>: "number of resistants animals in last step"
          - <b>condition</b>: ['step == MAX.STEP']

      - command3:
          - <b>desc</b>: "Simulation with the maximum of infected animals"
          - <b>target_variable</b>: [ "I", "step" ]
          - <b>condition</b>: ['I == max(I)']

      - persistence_t:
          - <b>target_variable</b>: [ "I" ]
          - <b>add_new_column</b>:
              Is_infected:
                  <b>value</b>: 'I>0'
                  <b>desc</b>: "Number of infected animals must be superior to 0"
          - <b>groupby</b>: [ "step" ]
          - <b>summarise</b>:
              - ratio:
                  <b>value</b>: 'sum(Is_infected)/!!NB.SIMU'
                  <b>desc</b>: "persistence of simulations over time"

      - persistence_end:
          - <b>target_variable</b>: [ "I" ]
          - <b>condition</b>: ['step == MAX.STEP']
          - <b>add_new_column</b>:
              Is_infected:
                  <b>value</b>: 'I>0'
                  <b>desc</b>: "Number of infected animals must be superior to 0"
          - <b>summarise</b>:
              - ratio:
                  <b>value</b>: "sum(Is_infected)/!!NB.SIMU"
                  <b>desc</b>: "persistence of simulations at the last step"

      - command4:
          - <b>target_variable</b>: [ "I" ]
          - <b>groupby</b>: [ "simu_id" ]
          - <b>summarise</b>:
              - max
          - <b>sort</b>: [ "max_I" ]
          - <b>add_new_column</b>:
              ranking:
                  <b>value</b>: 'rank(max_I)'
                  <b>desc</b>: "Rank maximum infected animals in each simu_id"

      - command5:
          - <b>inner_join</b>:
              <b>with</b>: "command4"
              <b>by</b>: "simu_id"


 <b>comparison_of_scenarios</b>:
      - command6:
          - <b>scenarios_to_compare</b>: [ "disease_with_control","disease_without_control" ]
          - <b>left_join</b>:
               <b>with</b>: "command5"
               <b>by</b>: ["ranking","step"]
          - <b>add_new_column</b>:
               diff:
                  <b>value</b>: 'R_disease_with_control - R_disease_without_control'
                  <b>desc</b>: 'Difference between Resistant animals in with_control scenario and without_control scenario'

      - command6_bis:
          - <b>scenarios_to_compare</b>: [ "disease_with_control","disease_without_control" ]
          - <b>left_join</b>:
               <b>with</b>: "command5"
               <b>by</b>: ["ranking","step"]
          - <b>add_new_column</b>:
               diff:
                  <b>value</b>: 'S_disease_with_control - S_disease_without_control'
                  <b>desc</b>: 'Difference between Susceptible animals in with_control scenario and without_control scenario'

      - command7:
          - <b>scenarios_to_compare</b>: [ "disease_with_control","disease_without_control" ]
          - <b>left_join</b>:
               <b>with</b>: "raw data"
               <b>by</b>: [ "step" ]
          - <b>target_variable</b>: [ "I" ]
          - <b>groupby</b>: [ "step" ]
          - <b>summarise</b>:
              - mean
          - <b>sort</b>: [ "mean_I" ]
          - <b>add_new_column</b>:
               diff:
                  <b>value</b>: 'mean_I_disease_with_control - mean_I_disease_without_control'
                  <b>desc</b>: 'Difference between Median of Infected animals in with_control scenario and without_control scenario'

      - command8:
          - <b>scenarios_to_compare</b>: [ "disease_without_control" , "without_disease" ]
          - <b>left_join</b>:
               <b>with</b>: "raw data"
               <b>by</b>: [ "step" ]
          - <b>target_variable</b>: [ "I" ]
          - <b>sort</b>: [ "I" ]
          - <b>add_new_column</b>:
               diff:
                  <b>value</b>: 'I_disease_without_control - I_without_disease'
                  <b>desc</b>: 'Difference between Infected animals in without_control scenario and without_disease scenario'

</pre>

##Output pages
Lists the subpages where the plots in the section below will be plotted.

##Graphics
In the graphics section we add how to plot the command calculated in the statistics section. The parsing will lead to an RShiny app file. 
GGplot2 is used to create the plots.
The Keywords are:
- **data**: we add the commands to plot as lists or string (for one command). The command is linked to the list of scenario.
- **on_page**: we add a list of page as lists. The name of the pages corresponds to the output_pages section.
- **single_graph**: **yes** if all the graphs (according to the lists of scenarios) are in the same plot. **No** if they are in different plots but in the same page.
- **scenario**: specifies the lists of scenarios to plot. 
- **plot_variables**: add general aesthetics in ggplot that will be shared by all plots lines added below. The keywords are **x** and **y**. 
We have plot_variables that are linked to the plot_type. In that cas the plot_variables are specific to the plot type. The value of the aesthetics
can be a variable of the command, a string or an integer. 
- **plot_type**: add using geom the type of plot you want with their corresponding aesthetics. 
- **plot_options**: has to be outside the aes function in the geom. The values of the plot_options can be string or integer.
- **plot_annotations**: Has three keys: title, x_title and y_title. 
<pre>
<b>graphics</b>:
 plot_command1:
      - <b>data</b>: "command1"
      - <b>on_page</b>: ["page1", "page2", "page3"]
      - <b>single_graph</b>: "no"
      - <b>scenario</b>: ["without_disease", "disease_without_control" , "disease_with_control"]
      - <b>plot_variables</b>: ### liés aux données, dans aes
          <b>x</b>: "step" ## là pas de chaines de caractères, vérifier que c'est bien un nom de colonnes de la command1 ici par exemple
          <b>y</b>: "median_I" ## si c'est pas un nom de colonnes, mettre un warning
      - <b>plot_type</b>:
          - line:
              <b>plot_variables</b>:
                group: "step"
                colour: "median" ### warning ici
              <b>plot_options</b>:
                alpha: "0.5"
                size: "5"
          - point:
              <b>plot_variables</b>:
                  <b>x</b>: "step"
                  <b>y</b>: "median_I"
                  group: "step"
              <b>plot_options</b>:
                alpha: "0.5"
                size: "5"
          - ribbon:
              <b>plot_variables</b>:
                <b>ymax</b>: "perc_0.95_I"
                <b>ymin</b>: "perc_0.5_I"

      - <b>plot_annotations</b>:
          <b>title</b>: "Median of maximum infected grouped by step and simu_id"
          <b>x_title</b>: "step"
          <b>y_title</b>: "Infected animals"

 plot_command2:
      - <b>data</b>: "command2"
      - <b>on_page</b>: ["page2", "page3"]
      - <b>single_graph</b>: "yes"
      - <b>scenario</b>: ["without_disease", "disease_without_control"]
      - <b>plot_variables</b>:
          <b>x</b>: "step"
          <b>y</b>: "I"
      - <b>plot_type</b>:
          - line
      - <b>plot_annotations</b>:
          <b>title</b>: "Number of Infected animals at last step by simulation"
          <b>x_title</b>: "step"
          <b>y_title</b>: "Infected animals"

 plot_command3:
     - <b>data</b>: "command3"
     - <b>on_page</b>: ["page2", "page3"]
     - <b>single_graph</b>: "no"
     - <b>scenario</b>: ["disease_without_control" , "disease_with_control"]
     - <b>plot_variables</b>:
          <b>x</b>: "step"
          <b>y</b>: "I"
     - <b>plot_type</b>:
          - point
     - <b>plot_annotations</b>:
          <b>title</b>: "la simulation ayant le maximum d'infectés"
          <b>x_title</b>: "step"
          <b>y_title</b>: "Infected animals"

 plot_persistence_t:
      - <b>data</b>: "persistence_t"
      - <b>on_page</b>: ["page1", "page2", "page3"]
      - <b>single_graph</b>: "yes"
      - <b>scenario</b>: ["without_disease", "disease_without_control" , "disease_with_control"]
      - <b>plot_variables</b>:
          <b>x</b>: "step"
          <b>y</b>: "ratio_I"
      - <b>plot_type</b>:
          - line:
              <b>plot_variables</b>:
                alpha: "0.3"
              <b>plot_options</b>:
                group: "step"
      - <b>plot_annotations</b>:
          <b>title</b>: "persistence of simulations over time"
          <b>x_title</b>: "step"
          <b>y_title</b>: "persistence Infected animals"

 plot_persistence_end:
      - <b>data</b>: "persistence_end"
      - <b>on_page</b>: ["page2", "page3"]
      - <b>single_graph</b>: "no"
      - <b>scenario</b>: ["disease_without_control" , "disease_with_control"]
      - <b>plot_variables</b>:
          <b>x</b>: "step"
          <b>y</b>: "ratio_I"
      - <b>plot_type</b>:
          - line:
              <b>plot_options</b>:
                group: "step"
      - <b>plot_annotations</b>:
          <b>title</b>: "persistence of simulations in the last step"
          <b>x_title</b>: "step"
          <b>y_title</b>: "persistence Infected animals"

 plot_command6:
      - <b>data</b>: ["command6", "command6_bis"]
      - <b>on_page</b>: ["page2", "page3", "page4"]
      - <b>single_graph</b>: "yes"
      - <b>plot_variables</b>:
          <b>x</b>: "step"
          <b>y</b>: "diff"
      - <b>plot_type</b>:
          - line:
              <b>plot_variables</b>:
                colour: "data"
                group: "ranking"
              <b>plot_options</b>:
                group: "step"
      - <b>plot_annotations</b>:
          <b>title</b>: "Difference of resistant animals between scenario with_control and without_control"
          <b>x_title</b>: "step"
          <b>y_title</b>: "difference of Resistant animals"

 plot_command7:
      - <b>data</b>: "command7"
      - <b>on_page</b>: ["page2", "page3", "page4"]
      - <b>single_graph</b>: "no"
      - <b>plot_variables</b>:
          <b>x</b>: "step"
          <b>y</b>: "diff"
      - <b>plot_type</b>:
          - line
          - point
      - <b>plot_annotations</b>:
          <b>title</b>: "mean_I_disease_with_control - mean_I_disease_without_control"
          <b>x_title</b>: "Median of Infected animals"
          <b>y_title</b>: "difference of Median Infected animals"

 plot_command8:
      - <b>data</b>: "command8"
      - <b>on_page</b>: ["page1","page2", "page4"]
      - <b>single_graph</b>: "no"
      - <b>plot_variables</b>:
          <b>x</b>: "step"
          <b>y</b>: "diff"
      - <b>plot_type</b>:
          - point
      - <b>plot_annotations</b>:
          <b>title</b>: "I_disease_without_control - I_without_disease grouped by step"
          <b>x_title</b>: "step"
          <b>y_title</b>: "diff Infected animals"
</pre>

# Example of paste.yaml file
          
## For integer field:

- **without slider**           

| input_forms   | input_parameters | django_forms | 
| :------------- | :------------- | :------------- | 
| test_frequence:<br>&emsp;&emsp;type: IntegerField<br>&emsp;&emsp;label: "Test frequence"<br>&emsp;&emsp;required: False<br>&emsp;&emsp;widget: NumberInput | - | 	test_frequence=<br>&emsp;&emsp;forms.IntegerField(<br>&emsp;&emsp;label="Test frequence",<br>&emsp;&emsp;required="False",widget=NumberInput(<br>&emsp;&emsp;attrs={'class': 'form-control'})) |

| Interface   |
| :-------------: |
|![Getting Started](img/test_frequence.png) |
          
- **with slider**          

| input_forms   | input_parameters | django_forms |
| :------------- | :------------- | :------------- | 
| initial_infected: <br>&emsp;&emsp;type: IntegerField<br>&emsp;&emsp;label: "Initial Infected" <br>&emsp;&emsp;required: True<br>&emsp;&emsp;widget: NumberInput<br>&emsp;&emsp;widget_attrs: {'data-min': 0,'data-step': 1,'data-max': 100} | initial_infected: 5 |initial_infected = <br>&emsp;&emsp;forms.IntegerField<br>&emsp;&emsp;(label="Initial Infected",<br>&emsp;&emsp;required="True",<br>&emsp;&emsp;widget=NumberInput(attrs={'data-min': 0, 'data-step': 1, 'data-max': 100,'data-grid': 'true', 'data-from': 5,'data-postfix': ' initial_infected','class': 'js-range-slider'}))|

| Interface   |
| :-------------: |
|![Getting Started](img/initial_infected_slider.png)|

               
- **with list**         

| input_forms   | input_parameters | django_forms | 
| :------------- | :------------- | :------------- |  
| initial_herd_size:<br>&emsp;&emsp;type: IntegerField<br>&emsp;&emsp;label: "Initial Herd size" <br>&emsp;&emsp;required: True<br>&emsp;&emsp;widget: forms.Select |initial_herd_size:<br>&emsp;&emsp;[50, 60, 70, 80, 90]| initial_herd_size = <br>&emsp;&emsp;forms.IntegerField<br>&emsp;&emsp;(label="Initial Herd Size",<br>&emsp;&emsp;required="True",<br>&emsp;&emsp;widget=(forms.Select(choices=((50, 50), (60, 60), (70, 70), (80, 80), (90, 90)),<br>&emsp;&emsp;attrs={'class': 'form-control'}))<br>&emsp;&emsp;|

| Interface   |
| :-------------: |
|![Getting Started](img/initial_herd_size_list.png)|

## For Float field:

- **Simple Float Field**   
 
| input_forms   | input_parameters | django_forms |
| :------------- | :------------- | :------------- |
|Float_field:<br>&emsp;&emsp;type: FloatField<br>&emsp;&emsp;label: "Float Field"<br>&emsp;&emsp;required: False<br>&emsp;&emsp;widget: NumberInput| - |Float_field=<br>&emsp;&emsp;forms.FloatField(<br>&emsp;&emsp;label="Float field",<br>&emsp;&emsp;required="False",<br>&emsp;&emsp;widget=NumberInput<br>&emsp;&emsp;(attrs={'step': 0.01, 'max': 1.0,<br>&emsp;&emsp; 'min': 0.0,'class': 'form-control'}))|

| Interface   |
| :-------------: |
|![Getting Started](img/float_field_simple.png) |

- **with slider**      

| input_forms   | input_parameters | django_forms |
| :------------- | :------------- | :------------- |
| transmission_I:<br>&emsp;&emsp;type: FloatField<br>&emsp;&emsp;label: "Transmission_I"<br>&emsp;&emsp;required: True<br>&emsp;&emsp;widget: NumberInput<br>&emsp;&emsp;widget_attrs: { <br>&emsp;&emsp;'data-min': 0,<br>&emsp;&emsp;'data-step': 0.1,<br>&emsp;&emsp;'data-max': 1 }  | transmission_I: 0.1 | transmission_I=<br>&emsp;&emsp;forms.FloatField(<br>&emsp;&emsp;label="Transmission_I",<br>&emsp;&emsp;required="True",<br>&emsp;&emsp;widget=NumberInput(<br>&emsp;&emsp;attrs={'data-min': 0, 'data-step': 0.1<br>&emsp;&emsp;, 'data-max': 1, 'data-grid': 'true'<br>&emsp;&emsp;, 'data-from': 0.1, 'data-postfix': ' transmission_I', <br>&emsp;&emsp;'class': 'js-range-slider'}))| 

| Interface   |
| :-------------: |
|![Getting Started](img/transmission_I.png) |

## Block_values

- **chained values**   

Here for each basin we have one or more breeds associated. The csv files should be like follows:
In basin.csv file, add "breed" as key to make the link between them. 

![Getting Started](img/block_values_csv.png)


1. In the interface, basin is shown as a dropdown list:

| input_forms   | input_parameters | django_forms | 
| :------------- | :------------- | :------------- | 
| basin:<br>&emsp;&emsp;type: CharField<br>&emsp;&emsp;label: "Basin/region"<br>&emsp;&emsp;required: False |  basin: <br>&emsp;&emsp;- file_path: "input_files/basin.csv" | basin = <br>&emsp;&emsp;forms.CharField(<br>&emsp;&emsp;label=u"basin",<br>&emsp;&emsp;widget=ModelSelect2Widget(model=basin,<br>&emsp;&emsp;search_fields=['name__icontains'],<br>&emsp;&emsp;attrs={'data-minimum-input-length': 0,<br>&emsp;&emsp;'data-placeholder': 'Choose a basin'}))  | 

| Interface   |
| :-------------: |
|![Getting Started](img/basin.png)|

2. Breed is updated according to the value selected in basin.

breed=forms.CharField(label=u"breed",widget=ModelSelect2Widget(model=breed,search_fields=['name__icontains'],dependent_fields={'0-basin': 'basin'},attrs={'data-minimum-input-length': 0,'data-placeholder': 'Choose a breed'}))      
	milk_production=forms.IntegerField(widget=forms.HiddenInput())      
	milk_price=forms.IntegerField(widget=forms.HiddenInput())      
	cow_price=forms.IntegerField(widget=forms.HiddenInput())      
	adult_age=forms.IntegerField(widget=forms.HiddenInput())       

| input_forms   | input_parameters | django_forms | 
| :------------- | :------------- | :------------- | 
| breed:<br>&emsp;&emsp;type: CharField<br>&emsp;&emsp;label: "Breed/race"<br>&emsp;&emsp;required: False | breed: <br>&emsp;&emsp;- file_path: "input_files/breed.csv" | breed = <br>&emsp;&emsp;forms.CharField(<br>&emsp;&emsp;label=u"breed",<br>&emsp;&emsp;widget=ModelSelect2Widget(model=breed,<br>&emsp;&emsp;search_fields=['name__icontains'],<br>&emsp;&emsp;dependent_fields={'0-basin': 'basin'},<br>&emsp;&emsp;attrs={'data-minimum-input-length': 0,<br>&emsp;&emsp;'data-placeholder': 'Choose a breed'}))  | 

| Interface   |
| :-------------: |
|![Getting Started](img/breed.png)|


3. Finally, when the user selects a basin and an associated breed, hidden input of breed are displayed.

| Interface   |
| :-------------: |
|![Getting Started](img/block_values_hidden_fields.png)|

- **Unchained**       

| input_forms   | input_parameters                                                                           | django_forms | 
| :------------- |:-------------------------------------------------------------------------------------------| :------------- | 
|vaccination:<br>&emsp;&emsp;type: CharField<br>&emsp;&emsp;label: "Vaccination"<br>&emsp;&emsp;required: True<br>&emsp;&emsp;widget: forms.Select| vaccination: <br>&emsp;&emsp;- file_path: "paste_specification/input_files/vaccination.csv" |vaccination= <br>&emsp;&emsp;forms.ModelChoiceField(<br>&emsp;&emsp;queryset=vaccination.objects.all(),<br>&emsp;&emsp;required=False,<br>&emsp;&emsp;widget=forms.Select(attrs={'class': 'form-control'}))<br>description=forms.CharField(widget=forms.HiddenInput())<br>administration_duration=forms.IntegerField(<br>&emsp;&emsp;widget=forms.HiddenInput())<br>immunity_duration=forms.IntegerField(<br>&emsp;&emsp;widget=forms.HiddenInput())<br>price=forms.IntegerField(<br>&emsp;&emsp;widget=forms.HiddenInput())<br>vaccine_administration_age=forms.IntegerField(<br>&emsp;&emsp;widget=forms.HiddenInput())<br>number_of_animals_treated=forms.IntegerField(<br>&emsp;&emsp;widget=forms.HiddenInput())|

| Interface   |
| :-------------: |
|![Getting Started](img/block_values_unchained.png)|

|          Interface when one value of the list is picked          |
|:----------------------------------------------------------------:|
| ![Getting Started](img/block_values_unchained_hidden_fields.png) |

## For CharField:

- **Simple CharField**    

| input_forms   | input_parameters | django_forms | 
| :------------- | :------------- | :------------- |
| simulation_name:<br>&emsp;&emsp;type: CharField<br>&emsp;&emsp;label: "Simulation name"<br>&emsp;&emsp;required: True<br>&emsp;&emsp;widget: forms.TextInput | - | 	simulation_name=<br>&emsp;&emsp;forms.CharField(<br>&emsp;&emsp;label="Simulation name",<br>&emsp;&emsp;required="True",widget=forms.TextInput(<br>&emsp;&emsp;attrs={'class': 'form-control'})) | 

| Interface   |
| :-------------: |
|![Getting Started](img/simulation_name.png)|


- **Textarea**     

| input_forms   | input_parameters | django_forms | 
| :------------- | :------------- | :------------- |
| simulation_description:<br>&emsp;&emsp;type: CharField<br>&emsp;&emsp;label: "Simulation description"<br>&emsp;&emsp;required: False<br>&emsp;&emsp;widget: forms.Textarea | - | simulation_description=<br>&emsp;&emsp;forms.CharField(<br>&emsp;&emsp;label="Simulation description",<br>&emsp;&emsp;required="False",widget=forms.Textarea(<br>&emsp;&emsp;attrs={'class': 'form-control'})) | 

| Interface   |
| :-------------: |
|![Getting Started](img/simulation_description.png)|

## Matrix Field

| input_forms                                                                                                                                                  | input_parameters                                                                                                                                                                                                                                                                                                                                                                                                                  | django_forms                                                                    | 
|:-------------------------------------------------------------------------------------------------------------------------------------------------------------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:--------------------------------------------------------------------------------|
| param_matrix:<br>&emsp;&emsp;type: Matrix<br>&emsp;&emsp;label: "Initial herd composition"<br>&emsp;&emsp;required: False<br>&emsp;&emsp;widget: HiddenInput | -  param_matrix:<br>&emsp;&emsp;nb_initial:<br>&emsp;&emsp;&emsp; rows:<br>&emsp;&emsp;&emsp;help_text: "Le niveau de risque"<br>&emsp;&emsp;&emsp;name: "breeder risk level"<br>&emsp;&emsp;&emsp;values:\["LowRisk", "MediumRisk", "HighRisk" \]<br>&emsp;&emsp;columns:<br>&emsp;&emsp;&emsp;help_text: "Le statut vaccinal"<br>&emsp;&emsp;&emsp;name: "vaccinal status"<br>&emsp;&emsp;&emsp;values:\["NV", "Vacc1", "Vacc2", "Imm2" \]<br>&emsp;&emsp;default_value:0 | <br>&emsp;&emsp; __init__ function in forms.py file (cf see the function below) | 

    def __init__(self, *args, **kwargs):

        super(initial_conditions, self).__init__(*args, **kwargs)
    
        self.param_matrix_cells = []
        self.param_matrix_rows_names = ['LowRisk', 'MediumRisk', 'HighRisk']
        self.param_matrix_columns_names = ['NV', 'Vacc1', 'Vacc2', 'Imm2']
        self.param_matrix_values=0
        for row in range(0, 3):
            self.param_matrix_cells.append({"row_label": f"{self.param_matrix_rows_names[row]}","data": []})
            for col in range(0, 4):
                self.param_matrix_cells[row]["data"].append({"name": f"nb_initial_{self.param_matrix_rows_names[row]}_{self.param_matrix_columns_names[col]}","value": self.param_matrix_values})

| Interface   |
| :-------------: |
|![Getting Started](img/matrix.png)|

## File Field

| input_forms   | input_parameters | django_forms | 
| :------------- | :------------- | :------------- | 
| external_files:<br>&emsp;&emsp;type: FileField<br>&emsp;&emsp;label: "Download external files"<br>&emsp;&emsp;required: False<br>&emsp;&emsp;widget: forms.FileInput  | - | 	external_files=<br>&emsp;&emsp;forms.FileField(<br>&emsp;&emsp;label="Download external files",<br>&emsp;&emsp;required="False",<br>&emsp;&emsp;widget=forms.FileInput(<br>&emsp;&emsp;attrs={'class': 'form-control'}))  | 

| Interface   |
| :-------------: |
|![Getting Started](img/file_export.png) |


## Summary
| input_forms   | input_parameters | django_forms                                                                                                                                                                                                                                                                                                                                                                            |                      Interface                      |
| :------------- | :------------- |:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:---------------------------------------------------:| 
| initial_herd_size:<br>&emsp;&emsp;type: IntegerField<br>&emsp;&emsp;label: "Initial Herd size" <br>&emsp;&emsp;required: True<br>&emsp;&emsp;widget: forms.Select |initial_herd_size:<br>&emsp;&emsp;[50, 60, 70, 80, 90]| initial_herd_size = <br>&emsp;&emsp;forms.IntegerField<br>&emsp;&emsp;(label="Initial Herd Size",<br>&emsp;&emsp;required="True",<br>&emsp;&emsp;widget=(forms.Select(choices=((50, 50), (60, 60), (70, 70), (80, 80), (90, 90)),<br>&emsp;&emsp;attrs={'class': 'form-control'}))<br>&emsp;&emsp;                                                                                      | ![Getting Started](img/initial_herd_size_list.png)  |
| initial_infected: <br>&emsp;&emsp;type: IntegerField<br>&emsp;&emsp;label: "Initial Infected" <br>&emsp;&emsp;required: True<br>&emsp;&emsp;widget: NumberInput<br>&emsp;&emsp;widget_attrs: {'data-min': 0,'data-step': 1,'data-max': 100} | initial_infected: 5 | initial_infected = <br>&emsp;&emsp;forms.IntegerField<br>&emsp;&emsp;(label="Initial Infected",<br>&emsp;&emsp;required="True",<br>&emsp;&emsp;widget=NumberInput(attrs={'data-min': 0, 'data-step': 1, 'data-max': 100,'data-grid': 'true', 'data-from': 5,'data-postfix': ' initial_infected','class': 'js-range-slider'}))                                                           | ![Getting Started](img/initial_infected_slider.png) |
| basin:<br>&emsp;&emsp;type: CharField<br>&emsp;&emsp;label: "Basin/region"<br>&emsp;&emsp;required: False |  basin: <br>&emsp;&emsp;- file_path: "input_files/basin.csv" | basin = <br>&emsp;&emsp;forms.CharField(<br>&emsp;&emsp;label=u"basin",<br>&emsp;&emsp;widget=ModelSelect2Widget(model=basin,<br>&emsp;&emsp;search_fields=['name__icontains'],<br>&emsp;&emsp;attrs={'data-minimum-input-length': 0,<br>&emsp;&emsp;'data-placeholder': 'Choose a basin'}))                                                                                            |          ![Getting Started](img/basin.png)          |
| breed:<br>&emsp;&emsp;type: CharField<br>&emsp;&emsp;label: "Breed/race"<br>&emsp;&emsp;required: False | breed: <br>&emsp;&emsp;- file_path: "input_files/breed.csv" | breed = <br>&emsp;&emsp;forms.CharField(<br>&emsp;&emsp;label=u"breed",<br>&emsp;&emsp;widget=ModelSelect2Widget(model=breed,<br>&emsp;&emsp;search_fields=['name__icontains'],<br>&emsp;&emsp;dependent_fields={'0-basin': 'basin'},<br>&emsp;&emsp;attrs={'data-minimum-input-length': 0,<br>&emsp;&emsp;'data-placeholder': 'Choose a breed'}))                                      |          ![Getting Started](img/breed.png)          |
| transmission_I:<br>&emsp;&emsp;type: FloatField<br>&emsp;&emsp;label: "Transmission_I"<br>&emsp;&emsp;required: True<br>&emsp;&emsp;widget: NumberInput<br>&emsp;&emsp;widget_attrs: { <br>&emsp;&emsp;'data-min': 0,<br>&emsp;&emsp;'data-step': 0.1,<br>&emsp;&emsp;'data-max': 1 }  | transmission_I: 0.1 | transmission_I=<br>&emsp;&emsp;forms.FloatField(<br>&emsp;&emsp;label="Transmission_I",<br>&emsp;&emsp;required="True",<br>&emsp;&emsp;widget=NumberInput(<br>&emsp;&emsp;attrs={'data-min': 0, 'data-step': 0.1<br>&emsp;&emsp;, 'data-max': 1, 'data-grid': 'true'<br>&emsp;&emsp;, 'data-from': 0.1, 'data-postfix': ' transmission_I', <br>&emsp;&emsp;'class': 'js-range-slider'}))|     ![Getting Started](img/transmission_I.png)      |
| external_files:<br>&emsp;&emsp;type: FileField<br>&emsp;&emsp;label: "Download external files"<br>&emsp;&emsp;required: False<br>&emsp;&emsp;widget: forms.FileInput  | - | 	external_files=<br>&emsp;&emsp;forms.FileField(<br>&emsp;&emsp;label="Download external files",<br>&emsp;&emsp;required="False",<br>&emsp;&emsp;widget=forms.FileInput(<br>&emsp;&emsp;attrs={'class': 'form-control'}))                                                                                                                                                               |       ![Getting Started](img/file_export.png)       |
| test_frequence:<br>&emsp;&emsp;type: IntegerField<br>&emsp;&emsp;label: "Test frequence"<br>&emsp;&emsp;required: False<br>&emsp;&emsp;widget: NumberInput | - | 	test_frequence=<br>&emsp;&emsp;forms.IntegerField(<br>&emsp;&emsp;label="Test frequence",<br>&emsp;&emsp;required="False",widget=NumberInput(<br>&emsp;&emsp;attrs={'class': 'form-control'}))                                                                                                                                                                                         |     ![Getting Started](img/test_frequence.png)      |
| simulation_name:<br>&emsp;&emsp;type: CharField<br>&emsp;&emsp;label: "Simulation name"<br>&emsp;&emsp;required: True<br>&emsp;&emsp;widget: forms.TextInput | - | 	simulation_name=<br>&emsp;&emsp;forms.CharField(<br>&emsp;&emsp;label="Simulation name",<br>&emsp;&emsp;required="True",widget=forms.TextInput(<br>&emsp;&emsp;attrs={'class': 'form-control'}))                                                                                                                                                                                       |     ![Getting Started](img/simulation_name.png)     |
| simulation_description:<br>&emsp;&emsp;type: CharField<br>&emsp;&emsp;label: "Simulation description"<br>&emsp;&emsp;required: False<br>&emsp;&emsp;widget: forms.Textarea | - | simulation_description=<br>&emsp;&emsp;forms.CharField(<br>&emsp;&emsp;label="Simulation description",<br>&emsp;&emsp;required="False",widget=forms.Textarea(<br>&emsp;&emsp;attrs={'class': 'form-control'}))                                                                                                                                                                          | ![Getting Started](img/simulation_description.png)  |
| param_matrix:<br>&emsp;&emsp;type: Matrix<br>&emsp;&emsp;label: "Initial herd composition"<br>&emsp;&emsp;required: False<br>&emsp;&emsp;widget: HiddenInput | -  param_matrix:<br>&emsp;&emsp;nb_initial:<br>&emsp;&emsp;&emsp; rows:<br>&emsp;&emsp;&emsp;help_text: "Le niveau de risque"<br>&emsp;&emsp;&emsp;name: "breeder risk level"<br>&emsp;&emsp;&emsp;values:\["LowRisk", "MediumRisk", "HighRisk" \]<br>&emsp;&emsp;columns:<br>&emsp;&emsp;&emsp;help_text: "Le statut vaccinal"<br>&emsp;&emsp;&emsp;name: "vaccinal status"<br>&emsp;&emsp;&emsp;values:\["NV", "Vacc1", "Vacc2", "Imm2" \]<br>&emsp;&emsp;default_value:0 | <br>&emsp;&emsp; __init__ function in forms.py file (cf see the function below)|         ![Getting Started](img/matrix.png)          |                                                                                                                                                                                                                                                                                                         | ![Getting Started](img/transmission_I.png) |                                                                                                                                                                                                                                                                                                     | ![Getting Started](img/transmission_I.png)
