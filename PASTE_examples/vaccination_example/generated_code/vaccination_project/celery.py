import os
from celery import Celery
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'vaccination_project.settings')
from django.conf import settings
app = Celery('vaccination_project')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()
@app.task()
def debug_task(self):
	print('Request: {0!r}'.format(self.request))