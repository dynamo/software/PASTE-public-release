from django.contrib import admin
from django.urls import path, include, re_path
from django.http import HttpResponseRedirect
urlpatterns = [
	path('admin/', admin.site.urls),
	path('', include('vaccination_app.urls')),
	re_path(r'^$', lambda r: HttpResponseRedirect('form/')),
]