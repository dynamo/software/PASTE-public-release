from django.db import models

class vaccine_characteristics(models.Model): 

	name=models.CharField(max_length=100)
	def get_name(self):
		return self.name
	def set_name(self,name):
		self.name = name


	delay_vacc=models.IntegerField()
	def get_delay_vacc(self):
		return self.delay_vacc
	def set_delay_vacc(self,delay_vacc):
		self.delay_vacc = delay_vacc


	dur_vacc=models.IntegerField()
	def get_dur_vacc(self):
		return self.dur_vacc
	def set_dur_vacc(self,dur_vacc):
		self.dur_vacc = dur_vacc


	vacc_waning=models.CharField(max_length=100)
	def get_vacc_waning(self):
		return self.vacc_waning
	def set_vacc_waning(self,vacc_waning):
		self.vacc_waning = vacc_waning


	V1_mortality_reduction=models.FloatField()
	def get_V1_mortality_reduction(self):
		return self.V1_mortality_reduction
	def set_V1_mortality_reduction(self,V1_mortality_reduction):
		self.V1_mortality_reduction = V1_mortality_reduction


	V2_mortality_reduction=models.FloatField()
	def get_V2_mortality_reduction(self):
		return self.V2_mortality_reduction
	def set_V2_mortality_reduction(self,V2_mortality_reduction):
		self.V2_mortality_reduction = V2_mortality_reduction


	V1_transmission_reduction=models.FloatField()
	def get_V1_transmission_reduction(self):
		return self.V1_transmission_reduction
	def set_V1_transmission_reduction(self,V1_transmission_reduction):
		self.V1_transmission_reduction = V1_transmission_reduction


	V2_transmission_reduction=models.FloatField()
	def get_V2_transmission_reduction(self):
		return self.V2_transmission_reduction
	def set_V2_transmission_reduction(self,V2_transmission_reduction):
		self.V2_transmission_reduction = V2_transmission_reduction


	vaccine_cost=models.IntegerField()
	def get_vaccine_cost(self):
		return self.vaccine_cost
	def set_vaccine_cost(self,vaccine_cost):
		self.vaccine_cost = vaccine_cost

	def __str__(self):
		return ("{}").format(self.name)

class risk_level_breeder(models.Model): 

	name=models.CharField(max_length=100)
	def get_name(self):
		return self.name
	def set_name(self,name):
		self.name = name


	prop_R=models.FloatField()
	def get_prop_R(self):
		return self.prop_R
	def set_prop_R(self,prop_R):
		self.prop_R = prop_R

	def __str__(self):
		return ("{}").format(self.name)
