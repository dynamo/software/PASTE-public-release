import os
from django.shortcuts import render
from django.conf import settings
from django.views import View
from django.http import JsonResponse
from formtools.wizard.forms import ManagementForm
from formtools.wizard.views import SessionWizardView
from .models import *
from .tasks import *
from celery import current_app
from django.core.exceptions import ValidationError
from django.core.files.storage import FileSystemStorage
import requests
import re
import fileinput

FormWizardTemplatesFiles = {"0": "initial_conditions.html", "1": "select_scenario.html","2": "execution.html"}
BASE_DIR=os.getcwd()

class FormWizard(SessionWizardView):
	form_list = ['initial_conditions', 'select_scenario', 'execution']
	file_storage = FileSystemStorage(location=os.path.join(settings.MEDIA_ROOT))

	def get_template_names(self):
		return [FormWizardTemplatesFiles[self.steps.current]]

	def get_form(self, step=None, data=None, files=None):
		if step is None:
			step = self.steps.current
		form_class = self.form_list[step]
		kwargs = self.get_form_kwargs(step)
		kwargs.update({
			'data': data,
			'files': files,
			'prefix': self.get_form_prefix(step, form_class),
			'initial': self.get_form_initial(step),
		})
		return form_class(**kwargs)

	def get_context_data(self, form, **kwargs):
		context = super(FormWizard, self).get_context_data(form=form, **kwargs)
		context.update({
			'all_data': self.get_all_cleaned_data(),
		})
		return context

	def post(self, *args, **kwargs):
		if self.request.method == 'GET':
			self.storage.reset()
			self.storage.current_step = self.steps.first
			self.admin_view.model_form = self.get_step_form()
		else:
			wizard_goto_step = self.request.POST.get('wizard_goto_step', None)
			if wizard_goto_step and wizard_goto_step in self.get_form_list():
				return self.render_goto_step(wizard_goto_step)
		management_form = ManagementForm(self.request.POST, prefix=self.prefix)
		if not management_form.is_valid():
			raise ValidationError('ManagementForm data is missing or has been tampered.')


		form_current_step = management_form.cleaned_data['current_step']
		if (form_current_step != self.steps.current and
				self.storage.current_step is not None):
			self.storage.current_step = form_current_step
		form = self.get_form(data=self.request.POST, files=self.request.FILES)

		input_file_path = BASE_DIR + "/vaccination_tool_specification/input_files/SIR_Vaccination.yaml"
		code_path = BASE_DIR + "vaccination_tool_specification/input_files"
		var = " emulsion run --silent {input_file_path} --code-path {code_path}".format(input_file_path=input_file_path, code_path=code_path)

		dict_non_emulsion_parameters = {}
		dict_non_emulsion_parameters_in_scenario = {}
		if form.is_valid():
			self.storage.set_step_data(self.steps.current, self.process_step(form))
			self.storage.set_step_files(self.steps.current, self.process_step_files(form))

			if self.steps.current == '0':

				risk_level_breeder_id = self.request.POST.get('0-risk_level_breeder')

				risk_level_breeder_name = list(risk_level_breeder.objects.filter(id=int(risk_level_breeder_id)).values_list('name', flat=True))

				prop_R = self.request.POST.get('0-prop_R')

				vaccine_characteristics_id = self.request.POST.get('0-vaccine_characteristics')

				vaccine_characteristics_name = list(vaccine_characteristics.objects.filter(id=int(vaccine_characteristics_id)).values_list('name', flat=True))

				delay_vacc = self.request.POST.get('0-delay_vacc')

				dur_vacc = self.request.POST.get('0-dur_vacc')

				vacc_waning = self.request.POST.get('0-vacc_waning')

				V1_mortality_reduction = self.request.POST.get('0-V1_mortality_reduction')

				V2_mortality_reduction = self.request.POST.get('0-V2_mortality_reduction')

				V1_transmission_reduction = self.request.POST.get('0-V1_transmission_reduction')

				V2_transmission_reduction = self.request.POST.get('0-V2_transmission_reduction')

				vaccine_cost = self.request.POST.get('0-vaccine_cost')

				initial_herd_composition = self.request.POST.get('0-initial_herd_composition')

				cost_purchase_calf = self.request.POST.get('0-cost_purchase_calf')

				selling_price_bull = self.request.POST.get('0-selling_price_bull')

				annual_feeding_cost = self.request.POST.get('0-annual_feeding_cost')

				nb_animals_NV_LowRisk = self.request.POST.get('0-nb_animals_NV_LowRisk')

				nb_animals_NV_MediumRisk = self.request.POST.get('0-nb_animals_NV_MediumRisk')

				nb_animals_NV_HighRisk = self.request.POST.get('0-nb_animals_NV_HighRisk')

				nb_animals_V1_LowRisk = self.request.POST.get('0-nb_animals_V1_LowRisk')

				nb_animals_V1_MediumRisk = self.request.POST.get('0-nb_animals_V1_MediumRisk')

				nb_animals_V1_HighRisk = self.request.POST.get('0-nb_animals_V1_HighRisk')

				nb_animals_V2_LowRisk = self.request.POST.get('0-nb_animals_V2_LowRisk')

				nb_animals_V2_MediumRisk = self.request.POST.get('0-nb_animals_V2_MediumRisk')

				nb_animals_V2_HighRisk = self.request.POST.get('0-nb_animals_V2_HighRisk')

				var_no_disease_no_vaccination = var 
				if nb_animals_NV_LowRisk != '' or nb_animals_NV_LowRisk is not None:
					var_no_disease_no_vaccination = var_no_disease_no_vaccination + (" -p nb_animals_NV_LowRisk={}".format(nb_animals_NV_LowRisk))
				if nb_animals_NV_MediumRisk != '' or nb_animals_NV_MediumRisk is not None:
					var_no_disease_no_vaccination = var_no_disease_no_vaccination + (" -p nb_animals_NV_MediumRisk={}".format(nb_animals_NV_MediumRisk))
				if nb_animals_NV_HighRisk != '' or nb_animals_NV_HighRisk is not None:
					var_no_disease_no_vaccination = var_no_disease_no_vaccination + (" -p nb_animals_NV_HighRisk={}".format(nb_animals_NV_HighRisk))
				if nb_animals_V1_LowRisk != '' or nb_animals_V1_LowRisk is not None:
					var_no_disease_no_vaccination = var_no_disease_no_vaccination + (" -p nb_animals_V1_LowRisk={}".format(nb_animals_V1_LowRisk))
				if nb_animals_V1_MediumRisk != '' or nb_animals_V1_MediumRisk is not None:
					var_no_disease_no_vaccination = var_no_disease_no_vaccination + (" -p nb_animals_V1_MediumRisk={}".format(nb_animals_V1_MediumRisk))
				if nb_animals_V1_HighRisk != '' or nb_animals_V1_HighRisk is not None:
					var_no_disease_no_vaccination = var_no_disease_no_vaccination + (" -p nb_animals_V1_HighRisk={}".format(nb_animals_V1_HighRisk))
				if nb_animals_V2_LowRisk != '' or nb_animals_V2_LowRisk is not None:
					var_no_disease_no_vaccination = var_no_disease_no_vaccination + (" -p nb_animals_V2_LowRisk={}".format(nb_animals_V2_LowRisk))
				if nb_animals_V2_MediumRisk != '' or nb_animals_V2_MediumRisk is not None:
					var_no_disease_no_vaccination = var_no_disease_no_vaccination + (" -p nb_animals_V2_MediumRisk={}".format(nb_animals_V2_MediumRisk))
				if nb_animals_V2_HighRisk != '' or nb_animals_V2_HighRisk is not None:
					var_no_disease_no_vaccination = var_no_disease_no_vaccination + (" -p nb_animals_V2_HighRisk={}".format(nb_animals_V2_HighRisk))

				dict_non_emulsion_parameters["no_disease_no_vaccination"]={"initial_herd_composition" : initial_herd_composition ,"cost_purchase_calf" : cost_purchase_calf ,"selling_price_bull" : selling_price_bull ,"annual_feeding_cost" : annual_feeding_cost ,"vaccine_cost" : vaccine_cost }
				dict_non_emulsion_parameters_in_scenario["no_disease_no_vaccination"]={}
				self.request.session['dict_non_emulsion_parameters_initial_conditions'] = dict_non_emulsion_parameters
				self.request.session['dict_non_emulsion_parameters_in_scenario_initial_conditions'] = dict_non_emulsion_parameters_in_scenario
				if delay_vacc != '' or delay_vacc is not None:
					var_no_disease_no_vaccination = var_no_disease_no_vaccination + (" -p delay_vacc={}".format(delay_vacc))
				if dur_vacc != '' or dur_vacc is not None:
					var_no_disease_no_vaccination = var_no_disease_no_vaccination + (" -p dur_vacc={}".format(dur_vacc))
				if vacc_waning != '' or vacc_waning is not None:
					var_no_disease_no_vaccination = var_no_disease_no_vaccination + (" -p vacc_waning={}".format(vacc_waning))
				if V1_mortality_reduction != '' or V1_mortality_reduction is not None:
					var_no_disease_no_vaccination = var_no_disease_no_vaccination + (" -p V1_mortality_reduction={}".format(V1_mortality_reduction))
				if V2_mortality_reduction != '' or V2_mortality_reduction is not None:
					var_no_disease_no_vaccination = var_no_disease_no_vaccination + (" -p V2_mortality_reduction={}".format(V2_mortality_reduction))
				if V1_transmission_reduction != '' or V1_transmission_reduction is not None:
					var_no_disease_no_vaccination = var_no_disease_no_vaccination + (" -p V1_transmission_reduction={}".format(V1_transmission_reduction))
				if V2_transmission_reduction != '' or V2_transmission_reduction is not None:
					var_no_disease_no_vaccination = var_no_disease_no_vaccination + (" -p V2_transmission_reduction={}".format(V2_transmission_reduction))
				if prop_R != '' or prop_R is not None:
					var_no_disease_no_vaccination = var_no_disease_no_vaccination + (" -p prop_R={}".format(prop_R))


				var_no_disease_no_vaccination = var_no_disease_no_vaccination + " -r 50"
				var_no_disease_no_vaccination = var_no_disease_no_vaccination + " -p initial_prevalence_LR=0"
				var_no_disease_no_vaccination = var_no_disease_no_vaccination + " -p initial_prevalence_MR=0"
				var_no_disease_no_vaccination = var_no_disease_no_vaccination + " -p initial_prevalence_HR=0"
				var_no_disease_no_vaccination = var_no_disease_no_vaccination + " -p vaccinate_on_farm=0"
				var_no_disease_no_vaccination = var_no_disease_no_vaccination + " -p revaccinate_on_farm=0"

				self.request.session['var_no_disease_no_vaccination'] = var_no_disease_no_vaccination


				var_no_disease_initial_vaccination = var 
				if nb_animals_NV_LowRisk != '' or nb_animals_NV_LowRisk is not None:
					var_no_disease_initial_vaccination = var_no_disease_initial_vaccination + (" -p nb_animals_NV_LowRisk={}".format(nb_animals_NV_LowRisk))
				if nb_animals_NV_MediumRisk != '' or nb_animals_NV_MediumRisk is not None:
					var_no_disease_initial_vaccination = var_no_disease_initial_vaccination + (" -p nb_animals_NV_MediumRisk={}".format(nb_animals_NV_MediumRisk))
				if nb_animals_NV_HighRisk != '' or nb_animals_NV_HighRisk is not None:
					var_no_disease_initial_vaccination = var_no_disease_initial_vaccination + (" -p nb_animals_NV_HighRisk={}".format(nb_animals_NV_HighRisk))
				if nb_animals_V1_LowRisk != '' or nb_animals_V1_LowRisk is not None:
					var_no_disease_initial_vaccination = var_no_disease_initial_vaccination + (" -p nb_animals_V1_LowRisk={}".format(nb_animals_V1_LowRisk))
				if nb_animals_V1_MediumRisk != '' or nb_animals_V1_MediumRisk is not None:
					var_no_disease_initial_vaccination = var_no_disease_initial_vaccination + (" -p nb_animals_V1_MediumRisk={}".format(nb_animals_V1_MediumRisk))
				if nb_animals_V1_HighRisk != '' or nb_animals_V1_HighRisk is not None:
					var_no_disease_initial_vaccination = var_no_disease_initial_vaccination + (" -p nb_animals_V1_HighRisk={}".format(nb_animals_V1_HighRisk))
				if nb_animals_V2_LowRisk != '' or nb_animals_V2_LowRisk is not None:
					var_no_disease_initial_vaccination = var_no_disease_initial_vaccination + (" -p nb_animals_V2_LowRisk={}".format(nb_animals_V2_LowRisk))
				if nb_animals_V2_MediumRisk != '' or nb_animals_V2_MediumRisk is not None:
					var_no_disease_initial_vaccination = var_no_disease_initial_vaccination + (" -p nb_animals_V2_MediumRisk={}".format(nb_animals_V2_MediumRisk))
				if nb_animals_V2_HighRisk != '' or nb_animals_V2_HighRisk is not None:
					var_no_disease_initial_vaccination = var_no_disease_initial_vaccination + (" -p nb_animals_V2_HighRisk={}".format(nb_animals_V2_HighRisk))

				dict_non_emulsion_parameters["no_disease_initial_vaccination"]={"initial_herd_composition" : initial_herd_composition ,"cost_purchase_calf" : cost_purchase_calf ,"selling_price_bull" : selling_price_bull ,"annual_feeding_cost" : annual_feeding_cost ,"vaccine_cost" : vaccine_cost }
				dict_non_emulsion_parameters_in_scenario["no_disease_initial_vaccination"]={}
				self.request.session['dict_non_emulsion_parameters_initial_conditions'] = dict_non_emulsion_parameters
				self.request.session['dict_non_emulsion_parameters_in_scenario_initial_conditions'] = dict_non_emulsion_parameters_in_scenario
				if delay_vacc != '' or delay_vacc is not None:
					var_no_disease_initial_vaccination = var_no_disease_initial_vaccination + (" -p delay_vacc={}".format(delay_vacc))
				if dur_vacc != '' or dur_vacc is not None:
					var_no_disease_initial_vaccination = var_no_disease_initial_vaccination + (" -p dur_vacc={}".format(dur_vacc))
				if vacc_waning != '' or vacc_waning is not None:
					var_no_disease_initial_vaccination = var_no_disease_initial_vaccination + (" -p vacc_waning={}".format(vacc_waning))
				if V1_mortality_reduction != '' or V1_mortality_reduction is not None:
					var_no_disease_initial_vaccination = var_no_disease_initial_vaccination + (" -p V1_mortality_reduction={}".format(V1_mortality_reduction))
				if V2_mortality_reduction != '' or V2_mortality_reduction is not None:
					var_no_disease_initial_vaccination = var_no_disease_initial_vaccination + (" -p V2_mortality_reduction={}".format(V2_mortality_reduction))
				if V1_transmission_reduction != '' or V1_transmission_reduction is not None:
					var_no_disease_initial_vaccination = var_no_disease_initial_vaccination + (" -p V1_transmission_reduction={}".format(V1_transmission_reduction))
				if V2_transmission_reduction != '' or V2_transmission_reduction is not None:
					var_no_disease_initial_vaccination = var_no_disease_initial_vaccination + (" -p V2_transmission_reduction={}".format(V2_transmission_reduction))
				if prop_R != '' or prop_R is not None:
					var_no_disease_initial_vaccination = var_no_disease_initial_vaccination + (" -p prop_R={}".format(prop_R))


				var_no_disease_initial_vaccination = var_no_disease_initial_vaccination + " -r 50"
				var_no_disease_initial_vaccination = var_no_disease_initial_vaccination + " -p initial_prevalence_LR=0"
				var_no_disease_initial_vaccination = var_no_disease_initial_vaccination + " -p initial_prevalence_MR=0"
				var_no_disease_initial_vaccination = var_no_disease_initial_vaccination + " -p initial_prevalence_HR=0"
				var_no_disease_initial_vaccination = var_no_disease_initial_vaccination + " -p vaccinate_on_farm=1"
				var_no_disease_initial_vaccination = var_no_disease_initial_vaccination + " -p revaccinate_on_farm=0"

				self.request.session['var_no_disease_initial_vaccination'] = var_no_disease_initial_vaccination


				var_no_disease_periodic_vaccination = var 
				if nb_animals_NV_LowRisk != '' or nb_animals_NV_LowRisk is not None:
					var_no_disease_periodic_vaccination = var_no_disease_periodic_vaccination + (" -p nb_animals_NV_LowRisk={}".format(nb_animals_NV_LowRisk))
				if nb_animals_NV_MediumRisk != '' or nb_animals_NV_MediumRisk is not None:
					var_no_disease_periodic_vaccination = var_no_disease_periodic_vaccination + (" -p nb_animals_NV_MediumRisk={}".format(nb_animals_NV_MediumRisk))
				if nb_animals_NV_HighRisk != '' or nb_animals_NV_HighRisk is not None:
					var_no_disease_periodic_vaccination = var_no_disease_periodic_vaccination + (" -p nb_animals_NV_HighRisk={}".format(nb_animals_NV_HighRisk))
				if nb_animals_V1_LowRisk != '' or nb_animals_V1_LowRisk is not None:
					var_no_disease_periodic_vaccination = var_no_disease_periodic_vaccination + (" -p nb_animals_V1_LowRisk={}".format(nb_animals_V1_LowRisk))
				if nb_animals_V1_MediumRisk != '' or nb_animals_V1_MediumRisk is not None:
					var_no_disease_periodic_vaccination = var_no_disease_periodic_vaccination + (" -p nb_animals_V1_MediumRisk={}".format(nb_animals_V1_MediumRisk))
				if nb_animals_V1_HighRisk != '' or nb_animals_V1_HighRisk is not None:
					var_no_disease_periodic_vaccination = var_no_disease_periodic_vaccination + (" -p nb_animals_V1_HighRisk={}".format(nb_animals_V1_HighRisk))
				if nb_animals_V2_LowRisk != '' or nb_animals_V2_LowRisk is not None:
					var_no_disease_periodic_vaccination = var_no_disease_periodic_vaccination + (" -p nb_animals_V2_LowRisk={}".format(nb_animals_V2_LowRisk))
				if nb_animals_V2_MediumRisk != '' or nb_animals_V2_MediumRisk is not None:
					var_no_disease_periodic_vaccination = var_no_disease_periodic_vaccination + (" -p nb_animals_V2_MediumRisk={}".format(nb_animals_V2_MediumRisk))
				if nb_animals_V2_HighRisk != '' or nb_animals_V2_HighRisk is not None:
					var_no_disease_periodic_vaccination = var_no_disease_periodic_vaccination + (" -p nb_animals_V2_HighRisk={}".format(nb_animals_V2_HighRisk))

				dict_non_emulsion_parameters["no_disease_periodic_vaccination"]={"initial_herd_composition" : initial_herd_composition ,"cost_purchase_calf" : cost_purchase_calf ,"selling_price_bull" : selling_price_bull ,"annual_feeding_cost" : annual_feeding_cost ,"vaccine_cost" : vaccine_cost }
				dict_non_emulsion_parameters_in_scenario["no_disease_periodic_vaccination"]={}
				self.request.session['dict_non_emulsion_parameters_initial_conditions'] = dict_non_emulsion_parameters
				self.request.session['dict_non_emulsion_parameters_in_scenario_initial_conditions'] = dict_non_emulsion_parameters_in_scenario
				if delay_vacc != '' or delay_vacc is not None:
					var_no_disease_periodic_vaccination = var_no_disease_periodic_vaccination + (" -p delay_vacc={}".format(delay_vacc))
				if dur_vacc != '' or dur_vacc is not None:
					var_no_disease_periodic_vaccination = var_no_disease_periodic_vaccination + (" -p dur_vacc={}".format(dur_vacc))
				if vacc_waning != '' or vacc_waning is not None:
					var_no_disease_periodic_vaccination = var_no_disease_periodic_vaccination + (" -p vacc_waning={}".format(vacc_waning))
				if V1_mortality_reduction != '' or V1_mortality_reduction is not None:
					var_no_disease_periodic_vaccination = var_no_disease_periodic_vaccination + (" -p V1_mortality_reduction={}".format(V1_mortality_reduction))
				if V2_mortality_reduction != '' or V2_mortality_reduction is not None:
					var_no_disease_periodic_vaccination = var_no_disease_periodic_vaccination + (" -p V2_mortality_reduction={}".format(V2_mortality_reduction))
				if V1_transmission_reduction != '' or V1_transmission_reduction is not None:
					var_no_disease_periodic_vaccination = var_no_disease_periodic_vaccination + (" -p V1_transmission_reduction={}".format(V1_transmission_reduction))
				if V2_transmission_reduction != '' or V2_transmission_reduction is not None:
					var_no_disease_periodic_vaccination = var_no_disease_periodic_vaccination + (" -p V2_transmission_reduction={}".format(V2_transmission_reduction))
				if prop_R != '' or prop_R is not None:
					var_no_disease_periodic_vaccination = var_no_disease_periodic_vaccination + (" -p prop_R={}".format(prop_R))


				var_no_disease_periodic_vaccination = var_no_disease_periodic_vaccination + " -r 50"
				var_no_disease_periodic_vaccination = var_no_disease_periodic_vaccination + " -p initial_prevalence_LR=0"
				var_no_disease_periodic_vaccination = var_no_disease_periodic_vaccination + " -p initial_prevalence_MR=0"
				var_no_disease_periodic_vaccination = var_no_disease_periodic_vaccination + " -p initial_prevalence_HR=0"
				var_no_disease_periodic_vaccination = var_no_disease_periodic_vaccination + " -p vaccinate_on_farm=1"
				var_no_disease_periodic_vaccination = var_no_disease_periodic_vaccination + " -p revaccinate_on_farm=1"

				self.request.session['var_no_disease_periodic_vaccination'] = var_no_disease_periodic_vaccination


				var_disease_no_vaccination = var 
				if nb_animals_NV_LowRisk != '' or nb_animals_NV_LowRisk is not None:
					var_disease_no_vaccination = var_disease_no_vaccination + (" -p nb_animals_NV_LowRisk={}".format(nb_animals_NV_LowRisk))
				if nb_animals_NV_MediumRisk != '' or nb_animals_NV_MediumRisk is not None:
					var_disease_no_vaccination = var_disease_no_vaccination + (" -p nb_animals_NV_MediumRisk={}".format(nb_animals_NV_MediumRisk))
				if nb_animals_NV_HighRisk != '' or nb_animals_NV_HighRisk is not None:
					var_disease_no_vaccination = var_disease_no_vaccination + (" -p nb_animals_NV_HighRisk={}".format(nb_animals_NV_HighRisk))
				if nb_animals_V1_LowRisk != '' or nb_animals_V1_LowRisk is not None:
					var_disease_no_vaccination = var_disease_no_vaccination + (" -p nb_animals_V1_LowRisk={}".format(nb_animals_V1_LowRisk))
				if nb_animals_V1_MediumRisk != '' or nb_animals_V1_MediumRisk is not None:
					var_disease_no_vaccination = var_disease_no_vaccination + (" -p nb_animals_V1_MediumRisk={}".format(nb_animals_V1_MediumRisk))
				if nb_animals_V1_HighRisk != '' or nb_animals_V1_HighRisk is not None:
					var_disease_no_vaccination = var_disease_no_vaccination + (" -p nb_animals_V1_HighRisk={}".format(nb_animals_V1_HighRisk))
				if nb_animals_V2_LowRisk != '' or nb_animals_V2_LowRisk is not None:
					var_disease_no_vaccination = var_disease_no_vaccination + (" -p nb_animals_V2_LowRisk={}".format(nb_animals_V2_LowRisk))
				if nb_animals_V2_MediumRisk != '' or nb_animals_V2_MediumRisk is not None:
					var_disease_no_vaccination = var_disease_no_vaccination + (" -p nb_animals_V2_MediumRisk={}".format(nb_animals_V2_MediumRisk))
				if nb_animals_V2_HighRisk != '' or nb_animals_V2_HighRisk is not None:
					var_disease_no_vaccination = var_disease_no_vaccination + (" -p nb_animals_V2_HighRisk={}".format(nb_animals_V2_HighRisk))

				dict_non_emulsion_parameters["disease_no_vaccination"]={"initial_herd_composition" : initial_herd_composition ,"cost_purchase_calf" : cost_purchase_calf ,"selling_price_bull" : selling_price_bull ,"annual_feeding_cost" : annual_feeding_cost ,"vaccine_cost" : vaccine_cost }
				dict_non_emulsion_parameters_in_scenario["disease_no_vaccination"]={}
				self.request.session['dict_non_emulsion_parameters_initial_conditions'] = dict_non_emulsion_parameters
				self.request.session['dict_non_emulsion_parameters_in_scenario_initial_conditions'] = dict_non_emulsion_parameters_in_scenario
				if delay_vacc != '' or delay_vacc is not None:
					var_disease_no_vaccination = var_disease_no_vaccination + (" -p delay_vacc={}".format(delay_vacc))
				if dur_vacc != '' or dur_vacc is not None:
					var_disease_no_vaccination = var_disease_no_vaccination + (" -p dur_vacc={}".format(dur_vacc))
				if vacc_waning != '' or vacc_waning is not None:
					var_disease_no_vaccination = var_disease_no_vaccination + (" -p vacc_waning={}".format(vacc_waning))
				if V1_mortality_reduction != '' or V1_mortality_reduction is not None:
					var_disease_no_vaccination = var_disease_no_vaccination + (" -p V1_mortality_reduction={}".format(V1_mortality_reduction))
				if V2_mortality_reduction != '' or V2_mortality_reduction is not None:
					var_disease_no_vaccination = var_disease_no_vaccination + (" -p V2_mortality_reduction={}".format(V2_mortality_reduction))
				if V1_transmission_reduction != '' or V1_transmission_reduction is not None:
					var_disease_no_vaccination = var_disease_no_vaccination + (" -p V1_transmission_reduction={}".format(V1_transmission_reduction))
				if V2_transmission_reduction != '' or V2_transmission_reduction is not None:
					var_disease_no_vaccination = var_disease_no_vaccination + (" -p V2_transmission_reduction={}".format(V2_transmission_reduction))
				if prop_R != '' or prop_R is not None:
					var_disease_no_vaccination = var_disease_no_vaccination + (" -p prop_R={}".format(prop_R))


				var_disease_no_vaccination = var_disease_no_vaccination + " -r 50"
				var_disease_no_vaccination = var_disease_no_vaccination + " -p vaccinate_on_farm=0"
				var_disease_no_vaccination = var_disease_no_vaccination + " -p revaccinate_on_farm=0"

				self.request.session['var_disease_no_vaccination'] = var_disease_no_vaccination


				var_disease_initial_vaccination = var 
				if nb_animals_NV_LowRisk != '' or nb_animals_NV_LowRisk is not None:
					var_disease_initial_vaccination = var_disease_initial_vaccination + (" -p nb_animals_NV_LowRisk={}".format(nb_animals_NV_LowRisk))
				if nb_animals_NV_MediumRisk != '' or nb_animals_NV_MediumRisk is not None:
					var_disease_initial_vaccination = var_disease_initial_vaccination + (" -p nb_animals_NV_MediumRisk={}".format(nb_animals_NV_MediumRisk))
				if nb_animals_NV_HighRisk != '' or nb_animals_NV_HighRisk is not None:
					var_disease_initial_vaccination = var_disease_initial_vaccination + (" -p nb_animals_NV_HighRisk={}".format(nb_animals_NV_HighRisk))
				if nb_animals_V1_LowRisk != '' or nb_animals_V1_LowRisk is not None:
					var_disease_initial_vaccination = var_disease_initial_vaccination + (" -p nb_animals_V1_LowRisk={}".format(nb_animals_V1_LowRisk))
				if nb_animals_V1_MediumRisk != '' or nb_animals_V1_MediumRisk is not None:
					var_disease_initial_vaccination = var_disease_initial_vaccination + (" -p nb_animals_V1_MediumRisk={}".format(nb_animals_V1_MediumRisk))
				if nb_animals_V1_HighRisk != '' or nb_animals_V1_HighRisk is not None:
					var_disease_initial_vaccination = var_disease_initial_vaccination + (" -p nb_animals_V1_HighRisk={}".format(nb_animals_V1_HighRisk))
				if nb_animals_V2_LowRisk != '' or nb_animals_V2_LowRisk is not None:
					var_disease_initial_vaccination = var_disease_initial_vaccination + (" -p nb_animals_V2_LowRisk={}".format(nb_animals_V2_LowRisk))
				if nb_animals_V2_MediumRisk != '' or nb_animals_V2_MediumRisk is not None:
					var_disease_initial_vaccination = var_disease_initial_vaccination + (" -p nb_animals_V2_MediumRisk={}".format(nb_animals_V2_MediumRisk))
				if nb_animals_V2_HighRisk != '' or nb_animals_V2_HighRisk is not None:
					var_disease_initial_vaccination = var_disease_initial_vaccination + (" -p nb_animals_V2_HighRisk={}".format(nb_animals_V2_HighRisk))

				dict_non_emulsion_parameters["disease_initial_vaccination"]={"initial_herd_composition" : initial_herd_composition ,"cost_purchase_calf" : cost_purchase_calf ,"selling_price_bull" : selling_price_bull ,"annual_feeding_cost" : annual_feeding_cost ,"vaccine_cost" : vaccine_cost }
				dict_non_emulsion_parameters_in_scenario["disease_initial_vaccination"]={}
				self.request.session['dict_non_emulsion_parameters_initial_conditions'] = dict_non_emulsion_parameters
				self.request.session['dict_non_emulsion_parameters_in_scenario_initial_conditions'] = dict_non_emulsion_parameters_in_scenario
				if delay_vacc != '' or delay_vacc is not None:
					var_disease_initial_vaccination = var_disease_initial_vaccination + (" -p delay_vacc={}".format(delay_vacc))
				if dur_vacc != '' or dur_vacc is not None:
					var_disease_initial_vaccination = var_disease_initial_vaccination + (" -p dur_vacc={}".format(dur_vacc))
				if vacc_waning != '' or vacc_waning is not None:
					var_disease_initial_vaccination = var_disease_initial_vaccination + (" -p vacc_waning={}".format(vacc_waning))
				if V1_mortality_reduction != '' or V1_mortality_reduction is not None:
					var_disease_initial_vaccination = var_disease_initial_vaccination + (" -p V1_mortality_reduction={}".format(V1_mortality_reduction))
				if V2_mortality_reduction != '' or V2_mortality_reduction is not None:
					var_disease_initial_vaccination = var_disease_initial_vaccination + (" -p V2_mortality_reduction={}".format(V2_mortality_reduction))
				if V1_transmission_reduction != '' or V1_transmission_reduction is not None:
					var_disease_initial_vaccination = var_disease_initial_vaccination + (" -p V1_transmission_reduction={}".format(V1_transmission_reduction))
				if V2_transmission_reduction != '' or V2_transmission_reduction is not None:
					var_disease_initial_vaccination = var_disease_initial_vaccination + (" -p V2_transmission_reduction={}".format(V2_transmission_reduction))
				if prop_R != '' or prop_R is not None:
					var_disease_initial_vaccination = var_disease_initial_vaccination + (" -p prop_R={}".format(prop_R))


				var_disease_initial_vaccination = var_disease_initial_vaccination + " -r 50"
				var_disease_initial_vaccination = var_disease_initial_vaccination + " -p vaccinate_on_farm=1"
				var_disease_initial_vaccination = var_disease_initial_vaccination + " -p revaccinate_on_farm=0"

				self.request.session['var_disease_initial_vaccination'] = var_disease_initial_vaccination


				var_disease_periodic_vaccination = var 
				if nb_animals_NV_LowRisk != '' or nb_animals_NV_LowRisk is not None:
					var_disease_periodic_vaccination = var_disease_periodic_vaccination + (" -p nb_animals_NV_LowRisk={}".format(nb_animals_NV_LowRisk))
				if nb_animals_NV_MediumRisk != '' or nb_animals_NV_MediumRisk is not None:
					var_disease_periodic_vaccination = var_disease_periodic_vaccination + (" -p nb_animals_NV_MediumRisk={}".format(nb_animals_NV_MediumRisk))
				if nb_animals_NV_HighRisk != '' or nb_animals_NV_HighRisk is not None:
					var_disease_periodic_vaccination = var_disease_periodic_vaccination + (" -p nb_animals_NV_HighRisk={}".format(nb_animals_NV_HighRisk))
				if nb_animals_V1_LowRisk != '' or nb_animals_V1_LowRisk is not None:
					var_disease_periodic_vaccination = var_disease_periodic_vaccination + (" -p nb_animals_V1_LowRisk={}".format(nb_animals_V1_LowRisk))
				if nb_animals_V1_MediumRisk != '' or nb_animals_V1_MediumRisk is not None:
					var_disease_periodic_vaccination = var_disease_periodic_vaccination + (" -p nb_animals_V1_MediumRisk={}".format(nb_animals_V1_MediumRisk))
				if nb_animals_V1_HighRisk != '' or nb_animals_V1_HighRisk is not None:
					var_disease_periodic_vaccination = var_disease_periodic_vaccination + (" -p nb_animals_V1_HighRisk={}".format(nb_animals_V1_HighRisk))
				if nb_animals_V2_LowRisk != '' or nb_animals_V2_LowRisk is not None:
					var_disease_periodic_vaccination = var_disease_periodic_vaccination + (" -p nb_animals_V2_LowRisk={}".format(nb_animals_V2_LowRisk))
				if nb_animals_V2_MediumRisk != '' or nb_animals_V2_MediumRisk is not None:
					var_disease_periodic_vaccination = var_disease_periodic_vaccination + (" -p nb_animals_V2_MediumRisk={}".format(nb_animals_V2_MediumRisk))
				if nb_animals_V2_HighRisk != '' or nb_animals_V2_HighRisk is not None:
					var_disease_periodic_vaccination = var_disease_periodic_vaccination + (" -p nb_animals_V2_HighRisk={}".format(nb_animals_V2_HighRisk))

				dict_non_emulsion_parameters["disease_periodic_vaccination"]={"initial_herd_composition" : initial_herd_composition ,"cost_purchase_calf" : cost_purchase_calf ,"selling_price_bull" : selling_price_bull ,"annual_feeding_cost" : annual_feeding_cost ,"vaccine_cost" : vaccine_cost }
				dict_non_emulsion_parameters_in_scenario["disease_periodic_vaccination"]={}
				self.request.session['dict_non_emulsion_parameters_initial_conditions'] = dict_non_emulsion_parameters
				self.request.session['dict_non_emulsion_parameters_in_scenario_initial_conditions'] = dict_non_emulsion_parameters_in_scenario
				if delay_vacc != '' or delay_vacc is not None:
					var_disease_periodic_vaccination = var_disease_periodic_vaccination + (" -p delay_vacc={}".format(delay_vacc))
				if dur_vacc != '' or dur_vacc is not None:
					var_disease_periodic_vaccination = var_disease_periodic_vaccination + (" -p dur_vacc={}".format(dur_vacc))
				if vacc_waning != '' or vacc_waning is not None:
					var_disease_periodic_vaccination = var_disease_periodic_vaccination + (" -p vacc_waning={}".format(vacc_waning))
				if V1_mortality_reduction != '' or V1_mortality_reduction is not None:
					var_disease_periodic_vaccination = var_disease_periodic_vaccination + (" -p V1_mortality_reduction={}".format(V1_mortality_reduction))
				if V2_mortality_reduction != '' or V2_mortality_reduction is not None:
					var_disease_periodic_vaccination = var_disease_periodic_vaccination + (" -p V2_mortality_reduction={}".format(V2_mortality_reduction))
				if V1_transmission_reduction != '' or V1_transmission_reduction is not None:
					var_disease_periodic_vaccination = var_disease_periodic_vaccination + (" -p V1_transmission_reduction={}".format(V1_transmission_reduction))
				if V2_transmission_reduction != '' or V2_transmission_reduction is not None:
					var_disease_periodic_vaccination = var_disease_periodic_vaccination + (" -p V2_transmission_reduction={}".format(V2_transmission_reduction))
				if prop_R != '' or prop_R is not None:
					var_disease_periodic_vaccination = var_disease_periodic_vaccination + (" -p prop_R={}".format(prop_R))


				var_disease_periodic_vaccination = var_disease_periodic_vaccination + " -r 50"
				var_disease_periodic_vaccination = var_disease_periodic_vaccination + " -p vaccinate_on_farm=1"
				var_disease_periodic_vaccination = var_disease_periodic_vaccination + " -p revaccinate_on_farm=1"

				self.request.session['var_disease_periodic_vaccination'] = var_disease_periodic_vaccination



			if self.steps.current == '1':

				selecting_scenarios_to_run = self.request.POST.getlist('1-selecting_scenarios_to_run')

				self.request.session['selecting_scenarios_to_run'] = selecting_scenarios_to_run

			if self.steps.current == '2':

				total_duration = self.request.POST.get('2-total_duration')

				simulation_name = self.request.POST.get('2-simulation_name')

				simulation_description = self.request.POST.get('2-simulation_description')


				total_duration = int(total_duration)

				self.request.session['total_duration'] = total_duration

				self.request.session['simulation_name'] = simulation_name
				shinyapp_file = BASE_DIR + "/shinyapp/" + "app.R"
				for line in fileinput.input(shinyapp_file, inplace=True):
					print(re.sub(r'simulation_name <- ".+"', 'simulation_name <- \"{}\"'.format(simulation_name), line.rstrip()))
				
				scenario_dict={'no_disease_no_vaccination': [{'initial_prevalence_LR': 0}, {'initial_prevalence_MR': 0}, {'initial_prevalence_HR': 0}, {'vaccinate_on_farm': 0}, {'revaccinate_on_farm': 0}], 'no_disease_initial_vaccination': [{'initial_prevalence_LR': 0}, {'initial_prevalence_MR': 0}, {'initial_prevalence_HR': 0}, {'vaccinate_on_farm': 1}, {'revaccinate_on_farm': 0}], 'no_disease_periodic_vaccination': [{'initial_prevalence_LR': 0}, {'initial_prevalence_MR': 0}, {'initial_prevalence_HR': 0}, {'vaccinate_on_farm': 1}, {'revaccinate_on_farm': 1}], 'disease_no_vaccination': [{'vaccinate_on_farm': 0}, {'revaccinate_on_farm': 0}], 'disease_initial_vaccination': [{'vaccinate_on_farm': 1}, {'revaccinate_on_farm': 0}], 'disease_periodic_vaccination': [{'vaccinate_on_farm': 1}, {'revaccinate_on_farm': 1}]}


			if self.steps.current == self.steps.last:
				return self.render_done(form, **kwargs)
			else:
				return self.render_next_step(form)

		return self.render(form)

	def done(self, form_list, form_dict, **kwargs):
		var_no_disease_no_vaccination = self.request.session['var_no_disease_no_vaccination']
		var_no_disease_initial_vaccination = self.request.session['var_no_disease_initial_vaccination']
		var_no_disease_periodic_vaccination = self.request.session['var_no_disease_periodic_vaccination']
		var_disease_no_vaccination = self.request.session['var_disease_no_vaccination']
		var_disease_initial_vaccination = self.request.session['var_disease_initial_vaccination']
		var_disease_periodic_vaccination = self.request.session['var_disease_periodic_vaccination']

		output_directory_path = BASE_DIR + "/emulsion_outputs/"
		simulation_name = str(self.request.session['simulation_name'])

		var_no_disease_no_vaccination = var_no_disease_no_vaccination + (" -t ") + str(int(self.request.session['total_duration'])) + " --output-dir " + output_directory_path + "output_" + simulation_name + "/output_" + "no_disease_no_vaccination"
		var_no_disease_initial_vaccination = var_no_disease_initial_vaccination + (" -t ") + str(int(self.request.session['total_duration'])) + " --output-dir " + output_directory_path + "output_" + simulation_name + "/output_" + "no_disease_initial_vaccination"
		var_no_disease_periodic_vaccination = var_no_disease_periodic_vaccination + (" -t ") + str(int(self.request.session['total_duration'])) + " --output-dir " + output_directory_path + "output_" + simulation_name + "/output_" + "no_disease_periodic_vaccination"
		var_disease_no_vaccination = var_disease_no_vaccination + (" -t ") + str(int(self.request.session['total_duration'])) + " --output-dir " + output_directory_path + "output_" + simulation_name + "/output_" + "disease_no_vaccination"
		var_disease_initial_vaccination = var_disease_initial_vaccination + (" -t ") + str(int(self.request.session['total_duration'])) + " --output-dir " + output_directory_path + "output_" + simulation_name + "/output_" + "disease_initial_vaccination"
		var_disease_periodic_vaccination = var_disease_periodic_vaccination + (" -t ") + str(int(self.request.session['total_duration'])) + " --output-dir " + output_directory_path + "output_" + simulation_name + "/output_" + "disease_periodic_vaccination"

		list_scenarios_name = ['no_disease_no_vaccination', 'no_disease_initial_vaccination', 'no_disease_periodic_vaccination', 'disease_no_vaccination', 'disease_initial_vaccination', 'disease_periodic_vaccination']
		scenario_output_directory = {}
		for item in list_scenarios_name:
			scenario_output_directory[item] = output_directory_path + "output_" + simulation_name + "/output_" + item + "/counts.csv"

		cmd_dict = {'var_no_disease_no_vaccination' : var_no_disease_no_vaccination,'var_no_disease_initial_vaccination' : var_no_disease_initial_vaccination,'var_no_disease_periodic_vaccination' : var_no_disease_periodic_vaccination,'var_disease_no_vaccination' : var_disease_no_vaccination,'var_disease_initial_vaccination' : var_disease_initial_vaccination,'var_disease_periodic_vaccination' : var_disease_periodic_vaccination}
		scenario_not_optional = ['var_no_disease_no_vaccination', 'var_no_disease_initial_vaccination', 'var_no_disease_periodic_vaccination']
		list_scenarios_name_to_run = []

		for scenario in scenario_not_optional:
			for key, value in cmd_dict.items():
				if scenario == key:
					list_scenarios_name_to_run.append(key)

		scenario_optional_list = self.request.session['selecting_scenarios_to_run']
		for scenario_optional in scenario_optional_list:
			var_scenario_optional = "var_" + scenario_optional
			for keys, values in cmd_dict.items():
				if var_scenario_optional == keys:
					list_scenarios_name_to_run.append(var_scenario_optional)

		self.request.session['dict_non_emulsion_parameters'] ={**self.request.session['dict_non_emulsion_parameters_initial_conditions']}
		self.request.session['dict_non_emulsion_parameters_in_scenario'] ={**self.request.session['dict_non_emulsion_parameters_in_scenario_initial_conditions']}
		for dict_non_emulsion_parameters_key, dict_non_emulsion_parameters_value in self.request.session['dict_non_emulsion_parameters'].items():
			for dict_non_emulsion_parameters_in_scenario_key, dict_non_emulsion_parameters_in_scenario_value in self.request.session['dict_non_emulsion_parameters_in_scenario'].items():
				if dict_non_emulsion_parameters_in_scenario_key == dict_non_emulsion_parameters_key:
					for key, value in dict_non_emulsion_parameters_value.items():
						for key1, value1 in dict_non_emulsion_parameters_in_scenario_value.items():
							if key == key1:
								self.request.session['dict_non_emulsion_parameters'][dict_non_emulsion_parameters_key].update(dict_non_emulsion_parameters_in_scenario_value)

		dict_eco= {}
		for key, values in self.request.session['dict_non_emulsion_parameters'].items():
			new_list = []
			for key_value, value_value in values.items():
				if value_value is not None:
					new_list.append(str(value_value))
			dict_eco[key] = new_list

		dict_each_scenario = {}
		for item in list_scenarios_name_to_run:
			list_each_scenario = []
			list_each_scenario.append(scenario_output_directory[item.replace('var_', '')])
			list_each_scenario.append(item.replace('var_',''))
			list_each_scenario.append(simulation_name)
			for number in dict_eco[item.replace('var_', '')]:
				list_each_scenario.append(number)
			dict_each_scenario[item.replace('var_', '')] = list_each_scenario

		app_name = "vaccination_app"
		list_scenarios_name_comp = ' '.join(list_scenarios_name_to_run).replace('var_','').split()
		list_scenario_comparison =[BASE_DIR + "/" + app_name + "/" + "Rscripts/output_Rdata_" + simulation_name + "/" + item + ".Rdata" for item in list_scenarios_name_comp]
		list_scenario_comparison.append(simulation_name)

		task = chord([chain(run_scenario.si(cmd_dict[item]), run_statistics_for_each_scenario.si(dict_each_scenario[item.replace('var_', '')])) for item in list_scenarios_name_to_run],run_scenarios_comparison.si(list_scenario_comparison))()
		context={}
		context['task_id'] = task.id

		return render(self.request, 'done.html', context)

class TaskView(View):

	def get(self, request, task_id):
		task = current_app.AsyncResult(task_id)
		response_data = {'task_status': task.status, 'task_id': task.id}
		if task.status == 'SUCCESS':
			response_data['results'] = task.get()
		return JsonResponse(response_data)

def dashapp(request):
	return render(request, 'result.html')

def shiny(request):
	return render(request, 'shiny.html')

def shiny_contents(request):
	response = requests.get('http://localhost:8100')
	soup = BeautifulSoup(response.content, 'html.parser')
	return JsonResponse({'html_contents': str(soup)})

def load_vaccine_characteristics_details(request):
	vaccine_characteristics_id = request.GET.get('vaccine_characteristics', None)
	data = {
		'is_taken': list(vaccine_characteristics.objects.filter(id=vaccine_characteristics_id).values())
	}
	return JsonResponse(data, safe=False)


def load_risk_level_breeder_details(request):
	risk_level_breeder_id = request.GET.get('risk_level_breeder', None)
	data = {
		'is_taken': list(risk_level_breeder.objects.filter(id=risk_level_breeder_id).values())
	}
	return JsonResponse(data, safe=False)
