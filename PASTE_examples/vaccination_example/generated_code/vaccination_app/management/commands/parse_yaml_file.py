from django.core.management.base import BaseCommand
import yaml
import os
import re
import sys
import pandas as pd
import networkx as nx
from django.conf import settings
from django.core import management
import operator
import warnings
from pathlib import Path

class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('--filename', type=str)
        parser.add_argument('--inputdir', type=str)

    def handle(self, *args, **options):

        input_dir_path = options['inputdir']

        with open(options['filename']) as file:
            # The FullLoader parameter handles the conversion from YAML
            # scalar values to Python the dictionary format
            documents = yaml.load(file, Loader=yaml.FullLoader)
            emulsion_setup = documents["emulsion_setup"]
            show_hidden_params = documents['show_hidden_parameters'] if 'show_hidden_parameters' in documents else False
            calcul_uri = documents['calcul_URI']
            input_parameters = documents["input_parameters"]
            scenarios = documents["scenarios"]
            scenarios_components = documents["scenarios_components"]

            # external_files = documents["external_files"]
            input_forms = documents["input_forms"]
            statistics = documents["statistics"]
            app_name = documents["django_app_name"]
            form_wizard_name = "FormWizard"

            BASE_DIR = os.path.join(settings.BASE_DIR)

            print("--------- Modifying configuration files in django project ---------")

            print("\nAdding celery in the project __init__.py file")
            init_file = open(BASE_DIR + "/" + os.path.basename(BASE_DIR) + "/__init__.py", "w+")
            init_file.write("from __future__ import absolute_import, unicode_literals"
                            "\nfrom .celery import app as celery_app"
                            "\n__all__ = ['celery_app']")

            print("\nCreating celery configuration file")
            celery_file = open(BASE_DIR + "/" + os.path.basename(BASE_DIR) + "/celery.py", "w+")
            celery_file.write("import os\nfrom celery import Celery"
                              "\nos.environ.setdefault(\'DJANGO_SETTINGS_MODULE\', \'{}.settings\')"
                              "\nfrom django.conf import settings"
                              "\napp = Celery(\'{}\')"
                              "\napp.config_from_object(\'django.conf:settings\', namespace=\'CELERY\')"
                              "\napp.autodiscover_tasks()"
                              "\n@app.task()"
                              "\ndef debug_task(self):"
                              "\n\tprint(\'Request: {{0!r}}\'.format(self.request))".format(os.path.basename(BASE_DIR),
                                                                                            os.path.basename(BASE_DIR)))

            print("\nCreating json file configuration for celery result encoder and decoder")
            json_file = open(BASE_DIR + "/" + os.path.basename(BASE_DIR) + "/myjson.py", "w+")
            json_file.write("import json"
                            "\nfrom datetime import datetime"
                            "\nfrom time import mktime")

            json_file.write("\nclass MyEncoder(json.JSONEncoder):"
                            "\n\tdef default(self, obj):"
                            "\n\t\tif isinstance(obj, datetime):"
                            "\n\t\t\treturn {"
                            "\n\t\t\t\t'__type__': '__datetime__',"
                            "\n\t\t\t\t'epoch': int(mktime(obj.timetuple()))"
                            "\n\t\t\t}"
                            "\n\t\telse:"
                            "\n\t\t\treturn json.JSONEncoder.default(self, obj)")

            json_file.write("\ndef my_decoder(obj):"
                            "\n\tif '__type__' in obj:"
                            "\n\t\tif obj['__type__'] == '__datetime__':"
                            "\n\t\t\treturn datetime.fromtimestamp(obj['epoch'])"
                            "\n\treturn obj")

            json_file.write("\n# Encoder function"
                            "\ndef my_dumps(obj):"
                            "\n\treturn json.dumps(obj, cls=MyEncoder)")

            json_file.write("\n# Decoder function"
                            "\ndef my_loads(obj):"
                            "\n\treturn json.loads(obj, object_hook=my_decoder)")

            print("\nCreating routing configuration file for channels")
            routing_file = open(BASE_DIR + "/" + os.path.basename(BASE_DIR) + "/routing.py", "w+")

            routing_file.write("from channels.routing import ProtocolTypeRouter"
                               "\napplication = ProtocolTypeRouter({"
                               "\n})")

            print("\nAdding into project settings file links to static, celery configuration files, "
                  "installed modules")
            settings_file = open(BASE_DIR + "/" + os.path.basename(BASE_DIR) + "/settings_paste.py", "w+")
            settings_file.write("from .settings import *")
            settings_file.write("\nINSTALLED_APPS.extend(['channels','formtools','django_select2', 'mathfilters'])")

            settings_file.write("\nASGI_APPLICATION = '{}.routing.application'"
                                "\nCHANNEL_LAYERS = {{"
                                "\n\t\t'default': {{"
                                "\n\t\t\t'BACKEND': 'channels_redis.core.RedisChannelLayer',"
                                "\n\t\t\t'CONFIG': {{"
                                "\n\t\t\t\t'hosts': [('127.0.0.1', 6379),],"
                                "\n\t\t}}\n\t}}\n}}".format(os.path.basename(BASE_DIR)))
            settings_file.write("\n#Register your new serializer methods into kombu"
                                "\nfrom kombu.serialization import register"
                                "\nfrom .myjson import my_dumps, my_loads"
                                "\nregister('myjson', my_dumps, my_loads,"
                                "\n\t\tcontent_type='application/x-myjson',"
                                "\n\t\tcontent_encoding='utf-8')")

            settings_file.write("\n#Tell celery to use your new serializer:"
                                "\nCELERY_ACCEPT_CONTENT = ['myjson']"
                                "\nCELERY_TASK_SERIALIZER = 'myjson'"
                                "\nCELERY_RESULT_SERIALIZER = 'myjson'")

            settings_file.write("\n#celery"
                                "\nCELERY_BROKER_URL = 'redis://localhost:6379'"
                                "\nCELERY_RESULT_BACKEND = 'redis://localhost:6379'")
            settings_file.write("\nSESSION_SERIALIZER = 'django.contrib.sessions.serializers.PickleSerializer'")
            settings_file.write("\nimport os")
            settings_file.write("\nSTATICFILES_DIRS = ["
                                "\n\tos.path.join(BASE_DIR, '{}/static'),"
                                "\n]"
                                "\nSTATIC_ROOT  = os.path.join(BASE_DIR, 'staticfiles')".format(app_name))

            print("\n\n--------- Creating/Modifying django application files ---------")
            input_file_path = BASE_DIR + "/" + str(documents["input_files_path"]['path']) + "/" + str(documents["model_emulsion_file"]['file_name'] )

            print("\nCreating models files according to block_values in the description tool yaml file")
            model_file = open(BASE_DIR + "/" + app_name + "/models.py", "w+")
            model_file.write("from django.db import models\n")

            f = open(BASE_DIR + "/" + app_name + "/forms.py", "w+")
            f.write("from django import forms \n")
            f.write("from django.forms.widgets import Select, HiddenInput, CheckboxInput, NumberInput, TextInput \n")
            f.write("from .models import *\n")
            f.write("from django_select2.forms import ModelSelect2Widget\n")

            ## creating dictionnary of block values
            block_values = {}
            cmd_import_data_files = {}
            block_values_graph = nx.DiGraph()
            for list_element in input_parameters[0]["block_values"]:
                for list_object in input_parameters[0]["block_values"][list_element]:
                    cmd_import_data_files["filename_" + list_element] = str(Path(input_dir_path).joinpath(list_object["file_path"]))
                    file = open(Path(input_dir_path).joinpath(list_object["file_path"]), 'r')
                    data = pd.read_csv(file, sep=',')
                    block_values[list_element] = data.dtypes.to_dict()  ## create dictionnary that contains headers and type of there values
                    for block_values_key in block_values.keys():  ## loop over the dictionnary
                        ## Creating networkx graphs for dependent fields in block values. adding block values element as nodes to the graph
                        block_values_graph.add_nodes_from([block_values_key])
                        for each, value in block_values.items():

                            if block_values_key in value:  ## to find relations between models, if a key is equals to a value of another key, then there is a relation
                                block_values_graph.add_edge(each,
                                                            block_values_key)  ## Linking nodes that are dependant with edges function
                                block_values[each][block_values_key] = "manytomany"

                            for key2, value in value.items():  ## to take account the relations in dtypes pandas, convert all dtypes to string
                                if value == 'object':
                                    block_values[each][key2] = "str"
                                if value == 'int64':
                                    block_values[each][key2] = "int"
                                if value == 'float64':
                                    block_values[each][key2] = "float"

            # write into models.py file the block_values model classes, taking account the many to many relations
            dict_ancestors = {}
            for each, value in block_values.items():
                dict_ancestors[each] = [len(list(nx.dfs_preorder_nodes(block_values_graph, each))),
                                        list(nx.dfs_preorder_nodes(block_values_graph, each))]

            sorted_d = dict(sorted(dict_ancestors.items(), key=operator.itemgetter(1), reverse=True))

            list_created = []
            for key, values in sorted_d.items():
                for element in values:
                    if type(element) is list:
                        for i in reversed(element):
                            if i not in list_created:
                                model_file.write("\nclass {}(models.Model): \n".format(i))
                                list_created.append(i)
                                for field, field_type in block_values[i].items():
                                    model_file.write("\n")
                                    if field_type == 'str':
                                        model_file.write("\t{}=models.CharField(max_length=100)\n".format(field))

                                    if field_type == 'int':
                                        model_file.write("\t{}=models.IntegerField()\n".format(field))

                                    if field_type == 'float':
                                        model_file.write("\t{}=models.FloatField()\n".format(field))

                                    if field_type == 'manytomany':
                                        model_file.write(
                                            "\t{} = models.ManyToManyField({}, blank=True)\n".format(field, field))

                                    model_file.write("\tdef get_{}(self):\n\t\treturn self.{}\n".format(field, field))
                                    model_file.write(
                                        "\tdef set_{}(self,{}):\n\t\tself.{} = {}\n\n".format(field, field, field,
                                                                                              field))

                                model_file.write("\tdef __str__(self):\n\t\treturn (\"{}\").format(self.name)")
                                model_file.write("\n")

            ## Creating dict of user_parameters_values
            dict_user_parameters_values = {}
            if input_parameters[1]["user_parameters_values"] is not None:
                for emulsion_parameters in input_parameters[1]["user_parameters_values"]:
                    dict_user_parameters_values.update(emulsion_parameters)

            ## Creating dict of user_parameters_matrices
            dict_user_parameters_matrices = {}
            if input_parameters[2]["user_parameters_matrices"] is not None:
                for matrice in input_parameters[2]["user_parameters_matrices"]:
                    dict_user_parameters_matrices.update(matrice)

            n = []
            input_form_template_name_list = []
            input_form_step_number = {}
            step_number = 0

            print("\nCreating forms and their html templates according to yaml file")
            if not os.path.exists(BASE_DIR + "/" + app_name + "/templates/"):
                os.makedirs(BASE_DIR + "/" + app_name + "/templates/")


            ## Reading scenario_components and scenario
            list_scenarios_name = []  ## variable to create R files for statistics
            scenarios_template = {}
            dict_optional_no = {}
            dict_optional_yes = {}
            list_keys_no = []
            list_keys_yes = []
            param_all = {}
            scenarios_form_list={}


            for scenario_list in scenarios:

                list_scenarios = []
                for component_key in scenario_list:
                    for component in scenarios_components:

                        for keys, values in component.items():
                            if component_key == keys:
                                list_scenarios.append(values)

                            for item in values:
                                for item_key, item_value in item.items():
                                    if component_key == item_key:
                                        list_scenarios.append([item])

                if len(list_scenarios) > 1: ## if scenarios list is more than on component

                    for component_values in list_scenarios[0]:

                        for key, value in component_values.items():
                            param_global = []

                            for value_item_key, value_item_value in value.items():
                                if value_item_key == "optional":
                                    component_option = value_item_value

                                else:
                                    dict_param = {value_item_key: value_item_value}
                                    param_global.append(dict_param)

                            for scenario_component in list_scenarios[1]:

                                for scenario_component_key, scenario_component_value in scenario_component.items():
                                    scenario_key = key + "_" + scenario_component_key
                                    list_scenarios_name.append(scenario_key)

                                    for scenario_component_value_key, scenario_component_value_value in scenario_component_value.items():
                                        if scenario_component_value_key == "optional":
                                            scenario_component_option = scenario_component_value_value

                                        form_name = scenario_component_value["forms"]["name"]
                                        scenarios_form_list[scenario_key]=form_name ### create dict with form name and scenario name as key

                                        param_specific = {}
                                        if "specific_parameters" in scenario_component_value["forms"].keys():
                                            param_specific[scenario_key] = scenario_component_value['forms'][
                                                'specific_parameters']
                                        param_all[scenario_key] = param_global + param_specific[scenario_key]

                                    scenarios_template[scenario_key] = scenario_component_value["forms"]["name"]

                                    if component_option == "no" and scenario_component_option == "no":
                                        list_keys_no.append(scenario_key)
                                        dict_optional_no["no"] = list_keys_no
                                    if component_option == "no" and scenario_component_option == "yes":
                                        list_keys_no.append(scenario_key)
                                        dict_optional_no["no"] = list_keys_no
                                    if component_option == "yes" and scenario_component_option == "no":
                                        list_keys_yes.append(scenario_key)
                                        dict_optional_yes["yes"] = list_keys_yes
                                    if component_option == "yes" and scenario_component_option == "yes":
                                        list_keys_yes.append(scenario_key)
                                        dict_optional_yes["yes"] = list_keys_yes
                else: ## if there is only on component in scenario list
                    for scenario_component_list in list_scenarios:
                        for scenario_component in scenario_component_list:

                            for scenario_component_key, scenario_component_value in scenario_component.items():
                                scenario_key = scenario_component_key
                                list_scenarios_name.append(scenario_key)

                                for scenario_component_value_key, scenario_component_value_value in scenario_component_value.items():
                                    if scenario_component_value_key == "optional":
                                        scenario_component_option = scenario_component_value_value

                                        if scenario_component_value_value == "yes":
                                            list_keys_yes.append(scenario_key)
                                            dict_optional_yes["yes"] = list_keys_yes

                                        if scenario_component_value_value == "no":
                                            list_keys_no.append(scenario_key)
                                            dict_optional_no["no"] = list_keys_no

                                    form_name = scenario_component_value["forms"]["name"]
                                    scenarios_form_list[scenario_key] = form_name
                                    param_specific = {}
                                    if "specific_parameters" in scenario_component_value["forms"].keys():
                                        param_specific[scenario_key] = scenario_component_value['forms'][
                                            'specific_parameters']
                                    param_all[scenario_key] = param_specific[scenario_key]

                                scenarios_template[scenario_key] = scenario_component_value["forms"]["name"]

            ## using input_forms to create both forms and templates files using Networkx. Networkx graph is used to identify dependant fields in the form
            field_created = []
            list_validate_k2 = []
            list_matrices_col_row = []
            validate_input_form_name = []
            matrice_cells_name_dict = {}
            input_form_name_list = []
            last_input_form = list(input_forms[-1].keys())

            ### List of django forms fields allowed
            from django.forms import fields
            django_list_fields = fields.__all__

            for step in input_forms:
                for input_form_name in step.keys():
                    ### Creating input_form name list with scenarios that are optional
                    if input_form_name in last_input_form:
                        if len(dict_optional_yes) > 0:
                            input_form_name_list.extend(["select_scenario", input_form_name])
                        else:
                            input_form_name_list.append(input_form_name)
                    else:
                        input_form_name_list.append(input_form_name)

                    ## Create the hmtl templates files
                    filename = input_form_name + ".html"
                    if input_form_name in last_input_form:
                        if len(dict_optional_yes) > 0:
                            template_file = open(BASE_DIR + "/" + app_name + "/templates/" + "select_scenario.html", "w+")
                            template_file.write("{% extends \"base.html\" %}\n{% load i18n %}"
                                                "\n{% load static %}}\n{% load mathfilters %}\n{% block head %}")

                            template_file.write("\n<h2>Scenario selection")
                            template_file.write("</h2><br>")

                            template_file.write("\n{{wizard.form.media}}"
                                                "\n{% endblock %}\n{% block content %}"
                                                "\n{{wizard.form.media.css}}\n")

                            template_file.write(
                                "\n<form action=\"\" method=\"post\" enctype=\"multipart/form-data\">{% csrf_token %}"
                                "\n{{ wizard.management_form }}"
                                "\n{% if wizard.form.forms %}"
                                "\n\t{{ wizard.form.management_form }}"
                                "\n\t{% for form in wizard.form.forms %}"
                                "\n\t\t{{ form }}"
                                "\n\t{% endfor %}"
                                "\n{% else %}")

                            template_file.write(" \n\t<div id=\"visible\" class=\"row\">"
                                                "\n\t\t{% for field in wizard.form.visible_fields %}"
                                                "\n\t\t\t<div class=\"form-group col-md-12\">"
                                                "\n\t\t\t\t{% if field.field.widget.attrs.class == \"js-range-slider\" %}"
                                                "\n\t\t\t\t{% if field.help_text %}"
                                                "\n\t\t\t\t\t<a href=\"#\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"{{ field.help_text|safe }}\">"
                                                "\n\t\t\t\t\t<label for=\"{{ field.id_for_label }}\">{{ field.label }}: <b><span id=\"demo1\"></span></b></label></a>"
                                                "\n\t\t\t\t{% else %}"
                                                "\n\t\t\t\t\t<label for=\"{{ field.id_for_label }}\">{{ field.label }}: <b><span id=\"demo1\"></span></b></label>"
                                                "\n\t\t\t\t{% endif %}"
                                                "\n\t\t\t\t\t{{ field  }}"
                                                "\n\t\t\t\t{% else %}"
                                                "\n\t\t\t\t{% if field.help_text %}"
                                                "\n\t\t\t\t\t<a href=\"#\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"{{ field.help_text|safe }}\">"
                                                "\n\t\t\t\t\t<label for=\"{{ field.id_for_label }}\">{{ field.label }}</label></a>"
                                                "\n\t\t\t\t{% else %}"
                                                "\n\t\t\t\t\t<label for=\"{{ field.id_for_label }}\">{{ field.label }}</label>"
                                                "\n\t\t\t\t{% endif %}"
                                                "\n\t\t\t\t\t{{ field }}"
                                                "\n\t\t\t\t{% endif %}"
                                                "\n\t\t\t</div>\n\t\t{% endfor %}\n\t</div>")

                            template_file.write("\n\t\t<div id=\"hidden\" class=\"row\">")
                            template_file.write("\n\t\t{% for hidden in wizard.form.hidden_fields %}"
                                                "\n\t\t\t{% if forloop.counter0|divisibleby:2 %} <div class=\"row text-center\"> {%  endif %}"
                                                "\n\t\t\t\t<div class=\"col-md-6\" style=\"cursor: pointer;\">"
                                                "\n\t\t\t<label for=\"{{ hidden.id_for_label }}\" style=\"display:none;\">{{hidden.label}} {{ hidden }}</label>"
                                                "\n\t\t\t</div>"
                                                "\n\t\t\t{%  if forloop.counter|divisibleby:2 or forloop.last %}</div><!-- row closing -->{%  endif %}"
                                                "\n\t\t\t{% endfor %}")
                            template_file.write("\n\t</div>")
                            template_file.write("\n{% endif %}"
                                                "\n{% endblock %}")
                            template_file.write("\n{% block button %}"
                                                "\n\t{% if wizard.steps.prev %}"
                                                "\n\t\t<button name=\"wizard_goto_step\" type=\"submit\" value=\"{{ wizard.steps.first }}\">{% trans \"first step\" %}</button>"
                                                "\n\t\t<button name=\"wizard_goto_step\" type=\"submit\" value=\"{{ wizard.steps.prev }}\">{% trans \"prev step\" %}</button>"
                                                "\n\t{% endif %}"
                                                "\n{% endblock %}")
                            template_file.write("\n{% block input %}"
                                                "\n\t<div style=\"float: left;margin-right:10px\"> Step {{wizard.steps.step1}} of {{wizard.steps.count}} </div>")
                            template_file.write("\n\t<div><input type=\"submit\" value=\"{% trans \"next\" %}\"/></div>")
                            template_file.write("\n</form>")
                            template_file.write("\n{% endblock %}")

                    template_file = open(BASE_DIR + "/" + app_name + "/templates/" + filename, "w+")
                    template_file.write("{% extends \"base.html\" %}\n{% load i18n %}"
                                        "\n{% load static %}}\n{% load mathfilters %}\n{% block head %}")

                    template_file.write("\n<h2>{}</h2><br>".format(input_form_name.replace('_', ' ').capitalize()))

                    template_file.write("\n{{wizard.form.media}}"
                                        "\n{% endblock %}\n{% block content %}"
                                        "\n{{wizard.form.media.css}}\n")

                    template_file.write(
                        "\n<form action=\"\" method=\"post\" enctype=\"multipart/form-data\">{% csrf_token %}"
                        "\n{{ wizard.management_form }}"
                        "\n{% if wizard.form.forms %}"
                        "\n\t{{ wizard.form.management_form }}"
                        "\n\t{% for form in wizard.form.forms %}"
                        "\n\t\t{{ form }}"
                        "\n\t{% endfor %}"
                        "\n{% else %}")

                    template_file.write(" \n\t<div id=\"visible\" class=\"row\">"
                                        "\n\t\t{% for field in wizard.form.visible_fields %}"
                                        "\n\t\t\t<div class=\"form-group col-md-12\">"
                                        "\n\t\t\t\t{% if field.field.widget.attrs.class == \"js-range-slider\" %}"
                                        "\n\t\t\t\t{% if field.help_text %}"
                                        "\n\t\t\t\t\t<a href=\"#\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"{{ field.help_text|safe }}\">"
                                        "\n\t\t\t\t\t<label for=\"{{ field.id_for_label }}\">{{ field.label }}: <b><span id=\"demo1\"></span></b></label></a>"
                                        "\n\t\t\t\t{% else %}"
                                        "\n\t\t\t\t\t<label for=\"{{ field.id_for_label }}\">{{ field.label }}: <b><span id=\"demo1\"></span></b></label>"
                                        "\n\t\t\t\t{% endif %}"
                                        "\n\t\t\t\t\t{{ field  }}"
                                        "\n\t\t\t\t{% else %}"
                                        "\n\t\t\t\t{% if field.help_text %}"
                                        "\n\t\t\t\t\t<a href=\"#\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"{{ field.help_text|safe }}\">"
                                        "\n\t\t\t\t\t<label for=\"{{ field.id_for_label }}\">{{ field.label }}</label></a>"
                                        "\n\t\t\t\t{% else %}"
                                        "\n\t\t\t\t\t<label for=\"{{ field.id_for_label }}\">{{ field.label }}</label>"
                                         "\n\t\t\t\t{% endif %}"
                                        "\n\t\t\t\t\t{{ field }}"
                                        "\n\t\t\t\t{% endif %}"
                                        "\n\t\t\t</div>\n\t\t{% endfor %}\n\t</div>")

                    template_file.write("\n\t\t<div id=\"hidden\" class=\"row\">")
                    template_file.write("\n\t\t{% for hidden in wizard.form.hidden_fields %}"
                                        "\n\t\t\t{% if forloop.counter0|divisibleby:4 %} <div class=\"row text-center\"> {%  endif %}"
                                        "\n\t\t\t\t<div class=\"col-md-6\" style=\"cursor: pointer;\">"
                                        "\n\t\t\t<label for=\"{{ hidden.id_for_label }}\" style=\"display:none;\">{{hidden.label}} <br>{{ hidden }}</label>"
                                        "\n\t\t\t</div>"
                                        "\n\t\t\t{%  if forloop.counter|divisibleby:4 or forloop.last %}</div><!-- row closing -->{%  endif %}"
                                        "\n\t\t\t{% endfor %}")
                    template_file.write("\n\t</div>")

                    ### Add matrice display in the corresponding template file
                    list_matrices_name = []
                    label_dict = {}
                    if dict_user_parameters_matrices:
                        for matrice_name, matrice_value in dict_user_parameters_matrices.items():
                            for matrice_fields_name, matrice_fields_value in matrice_value.items():
                                for attributes in step[input_form_name]:
                                    for k2, v2 in attributes.items():
                                        if matrice_name == k2:
                                            if "label" in v2.keys() and matrice_name not in label_dict:
                                                label_dict[matrice_name] = v2["label"]

                                            if k2 not in list_matrices_name:
                                                list_matrices_name.append(k2)

                                            if k2 not in list_validate_k2:
                                                list_validate_k2.append("True")

                                            if k2 not in list_matrices_col_row:
                                                list_matrices_col_row.append(k2)

                                            if input_form_name not in validate_input_form_name:
                                                validate_input_form_name.append(input_form_name)

                    for matrice_name, matrice_value in dict_user_parameters_matrices.items():
                        if input_form_name in validate_input_form_name and "True" in list_validate_k2 and matrice_name in list_matrices_name:

                            template_file.write("\n\t<div class=\"row\" >")
                            template_file.write("\n\t<div class=\"col-md-12\" >")
                            template_file.write("\n\t<label>{}</label>".format(label_dict[matrice_name]))
                            template_file.write("\n\t\t{{% if wizard.form.{}_cells %}}".format(matrice_name))
                            template_file.write("\n\t\t\t<table class=\"table .table-striped\">")
                            template_file.write("\n\t\t\t\t<tr>")

                            for matrice_fields_name, matrice_fields_value in matrice_value.items():
                                for key, value in matrice_fields_value.items():
                                    if "rows" in key:
                                        template_file.write("\n\t\t\t\t\t<th class=\"CellWithComment\" style=\"color:#337ab7;\" rowspan=\"2\">{}<span class=\"CellComment\">{}</span> </th>".format(matrice_fields_value[key]["name"],matrice_fields_value[key]["help_text"]))
                                        row_values=[matrice_fields_name + "_" + x for x in matrice_fields_value[key]["values"]]

                                for key, value in matrice_fields_value.items():
                                    if "columns" in key:
                                        len_colspan = len(matrice_fields_value[key]["values"])
                                        template_file.write("\n\t\t\t\t\t<th class=\"CellWithComment\" style=\"text-align:center; color:#337ab7\" colspan={}>{} <span class=\"CellComment\">{}</span></th>".format(len_colspan, matrice_fields_value[key]["name"],matrice_fields_value[key]["help_text"]))
                                        columns_values=matrice_fields_value[key]["values"]

                                template_file.write("\n\t\t\t\t</tr>")
                                template_file.write("\n\t\t\t\t<tr>")
                                for key, value in matrice_fields_value.items():
                                     if "columns" in key:
                                         template_file.write("\n\t\t\t\t\t{{% for colname in wizard.form.{}_{}_names %}}".format(matrice_name,key))

                                template_file.write("\n\t\t\t\t\t\t<th> {{colname}} </th>")
                                template_file.write("\n\t\t\t\t\t{% endfor %}")
                                template_file.write("\n\t\t\t\t</tr>")

                                template_file.write("\n\t\t\t{{%for row in wizard.form.{}_cells %}}".format(matrice_name))
                                template_file.write("\n\t\t\t<tr>")
                                template_file.write("\n\t\t\t\t<td> {{row.row_label}}</td>")
                                template_file.write("\n\t\t\t\t{%for col in row.data %}")

                                ### If defau value is list (Dropdown Field) or a number (number Field), display the approriate html.
                                if type(matrice_fields_value["default_value"]) is int:
                                    template_file.write("\n\t\t\t\t\t<td><input name =\"{{wizard.steps.step1 |sub:1}}-{{col.name}}\" value=\"{{col.value}}\"></td>")
                                elif type(matrice_fields_value["default_value"]) is list:
                                    template_file.write("\n\t\t\t\t<td>")
                                    template_file.write(
                                        "\n\t\t\t\t\t<select name = \"{{wizard.steps.step1 |sub:1}}-{{col.name}}\"> <option value = \"\"> --Please choose an option - - </option>")
                                    template_file.write("\n\t\t\t\t\t\t{% for val in col.value %}")
                                    template_file.write("\n\t\t\t\t\t\t\t<option value = \"{{val}}\" > {{val}} </option>")
                                    template_file.write("\n\t\t\t\t\t\t{% endfor %}")
                                    template_file.write("\n\t\t\t\t\t</select>")
                                    template_file.write("\n\t\t\t\t</td>")

                                template_file.write("\n\t\t\t\t{% endfor %}")
                                template_file.write("\n\t\t\t</tr>")
                                template_file.write("\n\t\t\t{% endfor %}")
                                template_file.write("\n\t\t</table>")
                                template_file.write("\n\t\t\n{% endif %}")
                                template_file.write("\n\t</div>")
                                template_file.write("\n\t</div>")

                            matrices_cells_list = [x + '_' + y for x in row_values for y in columns_values]
                            matrice_cells_name_dict[matrice_name] = matrices_cells_list

                    template_file.write("\n{% endif %}"
                                        "\n{% endblock %}")
                    template_file.write("\n{% block button %}"
                                        "\n\t{% if wizard.steps.prev %}"
                                        "\n\t\t<button name=\"wizard_goto_step\" type=\"submit\" value=\"{{ wizard.steps.first }}\">{% trans \"first step\" %}</button>"
                                        "\n\t\t<button name=\"wizard_goto_step\" type=\"submit\" value=\"{{ wizard.steps.prev }}\">{% trans \"prev step\" %}</button>"
                                        "\n\t{% endif %}"
                                        "\n{% endblock %}")
                    template_file.write("\n{% block input %}"
                                        "\n\t<div style=\"float: left;margin-right:10px\"> Step {{wizard.steps.step1}} of {{wizard.steps.count}} </div>")

                    if input_form_name in last_input_form:
                        template_file.write("\n\t<div><input type=\"submit\" value=\"{% trans \"submit\" %}\"/></div>")
                    else:
                        template_file.write("\n\t<div><input type=\"submit\" value=\"{% trans \"next\" %}\"/></div>")
                    template_file.write("\n</form>")

                    ### Creating choose scenario form: some scenario is optional yes.
                    if input_form_name in last_input_form:
                        if len(dict_optional_yes) > 0:
                            f.write("\nclass select_scenario(forms.Form):")
                            f.write("\n\tselecting_scenarios_to_run = forms.MultipleChoiceField(")
                            f.write("\n\trequired=False,"
                                    "\n\thelp_text=\"When a box is selected, this scenario is added to the reference scenarios and run in Emulsion\",")
                            f.write("\n\twidget=forms.CheckboxSelectMultiple(attrs={'class': 'inline'}),")
                            if type(dict_optional_yes["yes"]) is list:
                                f.write("\n\tchoices={})".format(
                                    tuple([(name, name) for name in dict_optional_yes["yes"]])))

                    ## name of the form class
                    f.write("\nclass {}(forms.Form):\n".format(
                        input_form_name))  ## write the form step name as classes in the form file

                    if input_form_name in last_input_form:
                        if len(dict_optional_yes) > 0:
                            input_form_template_name_scenario = "\"" + str(
                                step_number) + "\"" + ":" + " " + "\"" + "select_scenario.html" + "\""

                            input_form_template_name = input_form_template_name_scenario + "," + "\"" + str(
                            step_number + 1) + "\"" + ":" + " " + "\"" + input_form_name + ".html" + "\""
                        else:
                            input_form_template_name = "\"" + str(
                                step_number) + "\"" + ":" + " " + "\"" + input_form_name + ".html" + "\""

                    else:
                        input_form_template_name = "\"" + str(
                            step_number) + "\"" + ":" + " " + "\"" + input_form_name + ".html" + "\""

                    input_form_template_name_list.append(
                        input_form_template_name)  ## Create list for form templates names

                    input_form_step_number[input_form_name] = step_number
                    template_file.write("\n{{ wizard.form.media.js }}")
                    for attributes in step[input_form_name]:  ## loop over dictionnary of step fields
                        ## Creating javascript for dependent fields in form using networkx
                        for field in attributes.keys():
                            if field in list(block_values_graph):
                                template_file.write(
                                    "\n<script>\n$(\"#id_{}-{}\").change(function(){{".format(
                                        str(input_form_step_number[input_form_name]), field))
                                template_file.write("\nvar {}ID = $(this).val();".format(field))
                                template_file.write("\n$.ajax({")
                                template_file.write(
                                    "\n\turl: '{{% url 'load_{}_details' %}}',".format(field))
                                template_file.write("\n\tdata: {{'{}': {}ID}},".format(field, field))
                                template_file.write("\n\tdataType: 'json',")
                                template_file.write("\n\tsuccess: function(data) {")
                                template_file.write("\n\t\tid = data.is_taken[0].id")
                                template_file.write("\n\t\tif ({}ID == id) {{".format(field))

                                for sub_field in block_values[field].keys():
                                    if sub_field != "name":
                                        template_file.write(
                                            "\n\t\t\t$('#id_{}-{}').attr('value', data.is_taken[0].{});".format(
                                                str(input_form_step_number[input_form_name]), sub_field, sub_field))

                                for sub_field in block_values[field].keys():
                                    if sub_field != "name":
                                        if show_hidden_params is False:
                                            break

                                        template_file.write("\n\t\t\t$(\'#hidden\').show();")
                                        template_file.write(
                                            "\n\n\t\t\t$('label[for=\"id_{}-{}\"]').show();".format(
                                                str(input_form_step_number[input_form_name]), sub_field))
                                        template_file.write(
                                            "\n\t\t\t$('#id_{}-{}').attr('type', \"text\");".format(
                                                str(input_form_step_number[input_form_name]), sub_field))
                                        template_file.write(
                                            "\n\t\t\t$('#id_{}-{}').attr('readonly', \"true\");".format(
                                                str(input_form_step_number[input_form_name]), sub_field))
                                        template_file.write(
                                            "\n\t\t\t$('#id_{}-{}').attr('readonly', \"true\");".format(
                                                str(input_form_step_number[input_form_name]), sub_field))
                                        template_file.write(
                                            "$('#id_{}-{}').attr('style', 'text-align:center;color:grey;');".format(
                                                str(input_form_step_number[input_form_name]), sub_field))

                                if list(block_values_graph.predecessors(field)):
                                    for sub_field in block_values[field].keys():
                                        if sub_field != "name":

                                            template_file.write(
                                                "\n\t\t\t$('#id_{}-{}').attr('value', data.is_taken[0].{});".format(
                                                    str(input_form_step_number[input_form_name]), sub_field, sub_field))
                                    for sub_field in block_values[field].keys():
                                        if sub_field != "name":
                                            if show_hidden_params is False:
                                                break

                                            template_file.write("\n\t\t\t$(\'#hidden\').show();")
                                            template_file.write(
                                                "\n\n\t\t\t$('label[for=\"id_{}-{}\"]').show();".format(
                                                    str(input_form_step_number[input_form_name]), sub_field))
                                            template_file.write(
                                                "\n\t\t\t$('#id_{}-{}').attr('type', \"text\");".format(
                                                    str(input_form_step_number[input_form_name]), sub_field))

                                            template_file.write(
                                                "\n\t\t\t$('#id_{}-{}').attr('readonly', \"true\");".format(
                                                    str(input_form_step_number[input_form_name]), sub_field))
                                            template_file.write(
                                                "\n\t\t\t$('#id_{}-{}').attr('readonly', \"true\");".format(
                                                    str(input_form_step_number[input_form_name]), sub_field))

                                template_file.write("\n\t\t\t} else {"
                                                    "\n\t\t\t\t$('#hidden').hide();"
                                                    "\n\t\t\t}")

                                template_file.write("\n\t},\n\terror: function(data){"
                                                    "\n\tconsole.log('Data could not be retrieved: ' + data)"
                                                    "\n\t}\n\t});\n});\n</script>")

                        ## Creating input field description in forms templates
                        for k2, v2 in attributes.items():  ## loop over field name and attributes. keys correspond to name and values to fields arguments
                            if k2 in sorted_d.keys():
                                if list(block_values_graph.predecessors(k2)):
                                    predecessors = list(block_values_graph.predecessors(k2))

                                    dependant_fields = []
                                    for item in predecessors:
                                        string_item = "'" + str(input_form_step_number[
                                                                    input_form_name]) + "-" + item + "'" + ":" + "'" + item + "'"
                                        dependant_fields.append(string_item)
                                    dependant_fields = ', '.join(dependant_fields)
                                    for item in predecessors:

                                        if item not in field_created:
                                            field_created.append(item)
                                            for dict_fields in step[input_form_name]:
                                                if item in dict_fields:
                                                    for dict_fields_key, dict_fields_value in dict_fields.items():
                                                        f.write(
                                                            "\t{}=forms.{}(label=u\"{}\",required=False,help_text=\"{}\", widget=ModelSelect2Widget(model={},"
                                                            "search_fields=['name__icontains'],attrs={{'data-minimum-input-length': 0,"
                                                            "'data-placeholder': 'Choose a {}','class': 'form-control'}}))\n".format(item, dict_fields_value["type"], dict_fields_value["label"], dict_fields_value["help_text"], item,
                                                                                                             item))
                                            for key, value in block_values[item].items():
                                                if value == 'str' and key != "name":
                                                    f.write("\t{}=forms.CharField(widget=forms.HiddenInput(),required=False)\n".format(
                                                        key))

                                                if value == 'int' and key != "name":
                                                    f.write(
                                                        "\t{}=forms.IntegerField(widget=forms.HiddenInput(),required=False)\n".format(
                                                            key))

                                                if value == 'float' and key != "name":
                                                    f.write(
                                                        "\t{}=forms.FloatField(localize=False, widget=forms.HiddenInput(),required=False)\n".format(
                                                            key))

                                    if k2 not in field_created:
                                        field_created.append(k2)
                                        f.write(
                                            "\t{}=forms.{}(label=u\"{}\",required=False,help_text=\"{}\",widget=ModelSelect2Widget(model={},"
                                            "search_fields=['name__icontains'],dependent_fields={{{}}},"
                                            "attrs={{'data-minimum-input-length': 0,'data-placeholder': 'Choose a {}','class': 'form-control'}}))\n".format(
                                                k2, attributes[k2]["type"],attributes[k2]["label"], attributes[k2]["help_text"], k2, dependant_fields, k2))

                                    for key, value in block_values[k2].items():
                                        if value == 'str' and key != "name":
                                            f.write("\t{}=forms.CharField(widget=forms.HiddenInput(),required=False)\n".format(key))

                                        if value == 'int' and key != "name":
                                            f.write("\t{}=forms.IntegerField(widget=forms.HiddenInput(),required=False)\n".format(key))

                                        if value == 'float' and key != "name":
                                            f.write("\t{}=forms.FloatField(widget=forms.HiddenInput(),required=False)\n".format(key))

                                if k2 in list(nx.isolates(block_values_graph)):
                                    if k2 not in field_created:
                                        field_created.append(k2)

                                    f.write(
                                        "\t{}= forms.ModelChoiceField(queryset={}.objects.all(),".format(
                                            k2, k2))
                                    for item_key, item_value in v2.items():
                                        if item_key == "label":
                                            f.write("label=\"{}\",".format(item_value))
                                        if item_key == "required":
                                            f.write("required={},".format(item_value))
                                        if item_key == "help_text":
                                            f.write("help_text=\"{}\",".format(item_value))
                                        if item_key == "widget":
                                            f.write(
                                                "widget={}(attrs={{'class': 'form-control'}}))\n".format(item_value))


                                    for key, value in block_values[k2].items():
                                        if value == 'str' and key != "name":
                                            f.write("\t{}=forms.CharField(widget=forms.HiddenInput(),required=False)\n".format(key))

                                        if value == 'int' and key != "name":
                                            f.write("\t{}=forms.IntegerField(widget=forms.HiddenInput(),required=False)\n".format(key))

                                        if value == 'float' and key != "name":
                                            f.write("\t{}=forms.FloatField(widget=forms.HiddenInput(),required=False)\n".format(key))
                            else:
                                for k3, v3 in v2.items():
                                    if k3 == "type":
                                        if v3 in django_list_fields:
                                            f.write("\t" + k2 + "=")
                                            for key3, value3 in v2.items():  ## loop over dictionnary of argument
                                                if key3 == "type":
                                                    f.write("forms." + v3)
                                                if key3 == "label":
                                                    f.write("(label=\"{}\",".format(value3))
                                                if key3 == "required":
                                                    f.write("required={},".format(value3))
                                                if key3 == "help_text":
                                                    f.write("help_text=\"{}\",".format(value3))

                                                if key3 == "widget":
                                                    f.write("widget={}(".format(value3))
                                                    if k2 in dict_user_parameters_values.keys():  ## create dropdown menu if list for all user parameters values
                                                        if type(dict_user_parameters_values[k2]) is list:
                                                            f.write("choices={},".format(
                                                                tuple([(name, name) for name in dict_user_parameters_values[k2]])))

                                                if key3 == "widget_attrs":
                                                    if "data-min" in value3.keys():
                                                        if 'data-grid' not in value3:
                                                            value3['data-grid'] = "true"
                                                        if 'data-skin' not in value3:
                                                            value3['data-skin'] = "modern"
                                                        if 'data-label' in value3:
                                                            value3['data-postfix'] = " " + value3['data-label']
                                                        else:
                                                            value3['data-postfix'] = " " + k2
                                                        value3['class'] = "js-range-slider"

                                                        if k2 in dict_user_parameters_values.keys():  ## create slider if int value or create dropdown menu if list for all user parameters values
                                                            if type(dict_user_parameters_values[k2]) is int or type(
                                                                    dict_user_parameters_values[k2]) is float:
                                                                value3['data-from'] = dict_user_parameters_values[k2]
                                                            else:
                                                                f.write("attrs={}".format(value3))

                                                        f.write("attrs={}".format(value3))
                                                    else:
                                                        f.write("attrs={}".format(value3))


                                            if "widget_attrs" not in v2.keys():  ## add class by default to form field when not entered by the user
                                                if v2['type'] == "FloatField":
                                                    attrs_float = {'class': 'form-control', 'step': 0.0001, 'max': 1.0, 'min': 0.0}
                                                    f.write("attrs={}".format(attrs_float))
                                                else:
                                                    attrs = {'class': 'form-control'}
                                                    f.write("attrs={}".format(attrs))

                                            f.write(")")

                                            if k2 in dict_user_parameters_values.keys():  ## create dropdown menu if list for all user parameters values
                                                if type(dict_user_parameters_values[k2]) is not list:
                                                    f.write(",initial={}".format(dict_user_parameters_values[k2]))


                                            f.write(")\n")

                                        if k2 in dict_user_parameters_values.keys():  ## verifying that user_parameters values entered are not written differently than the ones input forms
                                            n.append(k2)

                ### create __init__ function to create matrice
                if dict_user_parameters_matrices:
                    if input_form_name in validate_input_form_name and "True" in list_validate_k2 and matrice_name in list_matrices_name:  ## if name of fields are in the matrices dict declared in the paste.yaml file
                        f.write("\n\tdef __init__(self, *args, **kwargs):\n")
                        f.write("\n\t\tsuper({}, self).__init__(*args, **kwargs)\n".format(input_form_name))
                    for matrice_name, matrice_value in dict_user_parameters_matrices.items():
                        if input_form_name in validate_input_form_name and "True" in list_validate_k2 and matrice_name in list_matrices_name: ## if name of fields are in the matrices dict declared in the paste.yaml file
                            f.write("\n\t\tself.{}_cells = []".format(matrice_name))

                            for matrice_fields_name, matrice_fields_value in matrice_value.items():
                                for key, value in matrice_fields_value.items():
                                    if type(value) is dict:
                                        f.write("\n\t\tself.{}_{}_names = {}".format(matrice_name, key,matrice_fields_value[key]["values"]))
                                f.write("\n\t\tself.{}_values={}".format(matrice_name,matrice_fields_value["default_value"]))

                            for matrice_fields_name, matrice_fields_value in matrice_value.items():
                                for key, value in matrice_fields_value.items():
                                    if "rows" in key:
                                        row_matrice = key
                                        len_rows = len(matrice_fields_value[key]["values"])

                                    if "columns" in key:
                                        column_matrice = key
                                        len_columns = len(matrice_fields_value[key]["values"])

                            f.write("\n\t\tfor row in range(0, {}):".format(len_rows))
                            f.write("\n\t\t\tself.{}_cells.append({{\"row_label\": f\"{{self.{}_{}_names[row]}}\",\"data\": []}})".format(matrice_name,matrice_name, row_matrice))
                            f.write("\n\t\t\tfor col in range(0, {}):".format(len_columns))
                            f.write("\n\t\t\t\tself.{}_cells[row][\"data\"].append({{\"name\": f\"{}_{{self.{}_{}_names[row]}}_{{self.{}_{}_names[col]}}\","
                                    "\"value\": self.{}_values}})".format(matrice_name,matrice_fields_name,matrice_name, row_matrice, matrice_name, column_matrice, matrice_name))
                            f.write("\n\n")


                #### Javascript for slider
                template_file.write("\n<script>"
                                    "\n$(document).ready(function(){"
                                    "\n\t$(\".js-range-slider\").ionRangeSlider();"
                                    "\n});"
                                    "\n</script>")

                #### javascript for tooltip
                template_file.write("\n<script>"
                                    "\n$(document).ready(function(){"
                                    "\n\t$('[data-toggle=\"tooltip\"]').tooltip();"
                                    "\n});"
                                "\n</script>")

                ### Javascript for Checkbox value
                template_file.write("\n<script>"
                        "\n$(\"form\").submit(function () {"
                        "\nvar this_master = $(this);"
                            "\nthis_master.find('input[type=\"checkbox\"]').each( function () {"
                                "\nvar checkbox_this = $(this);"
                                 "\nif( checkbox_this.is(\":checked\") == true ) {"
                                    "\ncheckbox_this.attr('value','1');"
                                "\n} else {"
                                    "\ncheckbox_this.prop('checked',true);"
                                    "\n//DONT' ITS JUST CHECK THE CHECKBOX TO SUBMIT FORM DATA"
                                    "\ncheckbox_this.attr('value','0');"
                               " \n}"
                            "\n})"
                        "\n})"
                        "\n</script>")
                template_file.write("\n{% endblock %}")

                step_number = step_number + 1
            if len(set(n)) != len(
                    dict_user_parameters_values):  ## gérer le initial_infected qui est présent à 3 endroits différents ? et l'écrire plutôt comme ça il est pas dans le formulaire
                self.stdout.write(self.style.ERROR(
                    'error - Please verify that the values in user_parameters values are in the form.'))
                sys.exit()


            ## Creating view file containing Wizard form view and json reponse views for dependant fields in the forms (hide and show according to the value of another field)
            print("\nCreating view file, makes the link between models and templates")
            views_file = open(BASE_DIR + "/" + app_name + "/views.py", "w+")
            views_file.write("import os\nfrom django.shortcuts import render\nfrom django.conf import settings"
                             "\nfrom django.views import View"
                             "\nfrom django.http import JsonResponse\nfrom formtools.wizard.forms import ManagementForm\n"
                             "from formtools.wizard.views import SessionWizardView\nfrom .models import *\n"
                             "from .tasks import *\n"
                             "from celery import current_app\n"
                             "from django.core.exceptions import ValidationError\n"
                             "from django.core.files.storage import FileSystemStorage\n"
                             "import requests\nimport re\nimport fileinput\n")

            views_file.write("\n")
            template_name = form_wizard_name + "TemplatesFiles"
            input_forms_name_list = template_name + " = {" + ", ".join(input_form_template_name_list) + "}"
            views_file.write("{}\n".format(input_forms_name_list))
            views_file.write("BASE_DIR=os.getcwd()")
            views_file.write("\n\n")
            views_file.write("class {}(SessionWizardView):\n".format(form_wizard_name))
            views_file.write("\tform_list = {}\n".format(input_form_name_list))
            views_file.write("\tfile_storage = FileSystemStorage(location=os.path.join(settings.MEDIA_ROOT))\n")
            views_file.write("\n")
            views_file.write(
                "\tdef get_template_names(self):\n\t\treturn [{}TemplatesFiles[self.steps.current]]\n".format(
                    form_wizard_name))
            views_file.write("\n")
            views_file.write("\tdef get_form(self, step=None, data=None, files=None):"
                             "\n\t\tif step is None:"
                             "\n\t\t\tstep = self.steps.current"
                             "\n\t\tform_class = self.form_list[step]"
                             "\n\t\tkwargs = self.get_form_kwargs(step)"
                             "\n\t\tkwargs.update({"
                             "\n\t\t\t'data': data,"
                             "\n\t\t\t'files': files,"
                             "\n\t\t\t'prefix': self.get_form_prefix(step, form_class),"
                             "\n\t\t\t'initial': self.get_form_initial(step),"
                             "\n\t\t})"
                             "\n\t\treturn form_class(**kwargs)""")
            views_file.write("\n\n")
            views_file.write("\tdef get_context_data(self, form, **kwargs):"
                             "\n\t\tcontext = super({}, self).get_context_data(form=form, **kwargs)"
                             "\n\t\tcontext.update({{"
                             "\n\t\t\t'all_data': self.get_all_cleaned_data(),"
                             "\n\t\t}})"
                             "\n\t\treturn context".format(form_wizard_name))
            views_file.write("\n\n")
            views_file.write("\tdef post(self, *args, **kwargs):"
                             "\n\t\tif self.request.method == 'GET':"
                             "\n\t\t\tself.storage.reset()"
                             "\n\t\t\tself.storage.current_step = self.steps.first"
                             "\n\t\t\tself.admin_view.model_form = self.get_step_form()"
                             "\n\t\telse:"
                             "\n\t\t\twizard_goto_step = self.request.POST.get('wizard_goto_step', None)"
                             "\n\t\t\tif wizard_goto_step and wizard_goto_step in self.get_form_list():"
                             "\n\t\t\t\treturn self.render_goto_step(wizard_goto_step)"
                             "\n\t\tmanagement_form = ManagementForm(self.request.POST, prefix=self.prefix)"
                             "\n\t\tif not management_form.is_valid():"
                             "\n\t\t\traise ValidationError('ManagementForm data is missing or has been tampered.')")
            views_file.write("\n\n")
            views_file.write("\n\t\tform_current_step = management_form.cleaned_data['current_step']"
                             "\n\t\tif (form_current_step != self.steps.current and"
                             "\n\t\t\t\tself.storage.current_step is not None):"
                             "\n\t\t\tself.storage.current_step = form_current_step"
                             "\n\t\tform = self.get_form(data=self.request.POST, files=self.request.FILES)")
            views_file.write("\n\n")
            views_file.write("\t\tinput_file_path = BASE_DIR + \"/{}/{}\"\n".format(str(documents["input_files_path"]['path']),str(documents["model_emulsion_file"]['file_name'])))
            views_file.write("\t\tcode_path = BASE_DIR + \"{}\"\n".format(str(documents["input_files_path"]['path'])))
            views_file.write("\t\tvar = \" {} {} ".format(emulsion_setup["emulsion_syntax"]["command"],emulsion_setup["emulsion_syntax"]["options"]))
            views_file.write("{input_file_path} --code-path {code_path}\".format(input_file_path=input_file_path, code_path=code_path)\n")
            views_file.write("\n\t\tdict_non_emulsion_parameters = {}")
            views_file.write("\n\t\tdict_non_emulsion_parameters_in_scenario = {}")
            views_file.write("\n\t\tif form.is_valid():")
            views_file.write("\n\t\t\tself.storage.set_step_data(self.steps.current, self.process_step(form))")
            views_file.write("\n\t\t\tself.storage.set_step_files(self.steps.current, self.process_step_files(form))")

            ### Run emulsion to retrieve list of modifiable parameters
            from emulsion.model.emulsion_model import EmulsionModel
            input_dir = documents['input_files_path']['path']
            self.model = EmulsionModel(filename=input_file_path, input_dir=input_dir)
            namespace = self.model._namespace
            ## Creating list of emuslion parameters in namespace
            parameters_modifiable = []
            for item in namespace.keys():
                parameters_modifiable.append(item)


            number_of_repetitions = emulsion_setup['number_of_repetitions']
            figure_text_size = emulsion_setup['figure_text_size']

            # Creating view file according to scenarios dictionary
            session_list = []
            cmd_list = []
            output_directory_list=[]
            scenario_dict={}
            previous_fields = {}
            name_of_matrices_cells_list = []
            dict_matrices_name_form = {}
            list_non_emulsion_parameters_in_scenario = []
            input_form_name_non_emulsion = []
            list_non_emulsion_parameters_global = []
            list_blocks_values_fields = list(block_values.keys())
            list_matrices_name_view = []

            for step in input_forms:
                for input_form_name in step.keys():
                    if input_form_name in last_input_form:
                        if len(dict_optional_yes) > 0: ## If length of list of scenario optional > 0
                            last_step_number = input_form_step_number[input_form_name] + 1
                            views_file.write("\n\n\t\t\tif self.steps.current == '{}':".format(str(input_form_step_number[input_form_name])))
                            views_file.write("\n\n\t\t\t\tselecting_scenarios_to_run = self.request.POST.getlist('{}-selecting_scenarios_to_run')".format((input_form_step_number[input_form_name])))
                            views_file.write("\n\n\t\t\t\tself.request.session['selecting_scenarios_to_run'] = selecting_scenarios_to_run")
                            views_file.write("\n\n\t\t\tif self.steps.current == '{}':".format(
                            str(last_step_number)))
                        else:
                            views_file.write(
                                "\n\n\t\t\tif self.steps.current == '{}':".format(
                                    str(input_form_step_number[input_form_name])))
                    else:
                        views_file.write(
                        "\n\n\t\t\tif self.steps.current == '{}':".format(str(input_form_step_number[input_form_name])))


                    list_matrices_name_form = []
                    name_of_block_values_fields = []
                    non_emulsion_param=[]
                    list_non_emulsion_parameters=[]

                    for attributes in step[input_form_name]:
                        for field in attributes.keys():

                            dont_add_matrice_name = []
                            if dict_user_parameters_matrices:
                                for matrice_name, matrice_value in dict_user_parameters_matrices.items():
                                    if field != matrice_name:
                                        dont_add_matrice_name.append(field)

                            if input_form_name in last_input_form: ## last input form fields
                                if len(dict_optional_yes) > 0:

                                    last_step_number = input_form_step_number[input_form_name] + 1
                                    views_file.write("\n\n\t\t\t\t{} = self.request.POST.get('{}-{}')".format(field, str(last_step_number), field))
                                else:

                                    views_file.write(
                                        "\n\n\t\t\t\t{} = self.request.POST.get('{}-{}')".format(field, str(
                                            input_form_step_number[input_form_name]), field))

                            else:

                                if field not in parameters_modifiable and field not in block_values:
                                    non_emulsion_param.append(field)

                                if field in block_values:
                                    views_file.write("\n\n\t\t\t\t{}_id = self.request.POST.get('{}-{}')".format(field, str(
                                    input_form_step_number[input_form_name]), field))
                                    views_file.write("\n\n\t\t\t\t{}_name = list({}.objects.filter(id=int({}_id)).values_list('name', flat=True))".format(field, field, field))

                                    for block_values_key, block_values_value in block_values[field].items():
                                        if block_values_key != "name" and block_values_key not in list_blocks_values_fields:
                                            views_file.write(
                                                "\n\n\t\t\t\t{} = self.request.POST.get('{}-{}')".format(block_values_key, str(
                                                    input_form_step_number[input_form_name]), block_values_key))
                                            if block_values_key not in name_of_block_values_fields:
                                                name_of_block_values_fields.append(block_values_key)

                                else:
                                    views_file.write(
                                        "\n\n\t\t\t\t{} = self.request.POST.get('{}-{}')".format(field, str(
                                            input_form_step_number[input_form_name]), field))


                            # Verify that matrice name in input form exists in parameters
                            if dict_user_parameters_matrices:
                                for matrice_name, matrice_value in dict_user_parameters_matrices.items():
                                    if matrice_name == field:
                                        if field not in list_matrices_name_view:
                                            list_matrices_name_view.append(field)
                                        list_matrices_name_form = list_matrices_name_form + matrice_cells_name_dict[matrice_name]

                                dict_matrices_name_form[input_form_name] = list_matrices_name_form



                    # Then add matrices values in the input form view
                    if dict_user_parameters_matrices:
                        for matrice_name, matrice_value in dict_user_parameters_matrices.items():
                            if input_form_name in validate_input_form_name and "True" in list_validate_k2 and matrice_name in list_matrices_name_view:
                                for item in matrice_cells_name_dict[matrice_name]:
                                    views_file.write("\n\n\t\t\t\t{} = self.request.POST.get('{}-{}')".format(item, str(
                                        input_form_step_number[input_form_name]), item))
                                    name_of_matrices_cells_list.append(item)

                    views_file.write("\n")

                    for param_all_key, param_all_value in param_all.items():
                        scenario_key = param_all_key

                        input_form_field_list = []
                        list_scenario_param = []
                        dict_non_emulsion_parameters = {}

                        if input_form_name in scenarios_form_list[scenario_key]:
                            list_scenarios_form = scenarios_form_list[scenario_key]
                            if input_form_name == list_scenarios_form[-1]: ## if it is the last input form of the scenario


                                output = "output_directory_path " + "+" + "\"" + "output_" + scenario_key + "\""
                                output_directory_list.append(output)
                                cmd_output_file = "var" + "_" + scenario_key + " = " + "var " #+ "+ " + "(\" --output-dir {}\"" + ".format(" + output + "))"
                                views_file.write("\n\t\t\t\t{}".format(cmd_output_file))

                                ## List on non emulsion params from input_forms name (fields)
                                for non_emulsion_param_item in non_emulsion_param:
                                    if non_emulsion_param_item not in list_non_emulsion_parameters:
                                        list_non_emulsion_parameters.append(non_emulsion_param_item)

                                #Add matrices values in the emulsion command if it is an emulsion parameters
                                emulsion_parameters_modifiable_matrice = [elem for elem in parameters_modifiable if elem in name_of_matrices_cells_list]
                                for form_matrice_name in dict_matrices_name_form.keys():
                                    if input_form_name == form_matrice_name:
                                        for item in dict_matrices_name_form[input_form_name]:
                                            if item in emulsion_parameters_modifiable_matrice:
                                                condition = "if " + item + " != " + "''" + " or " + item + " is not None" + ":"
                                                views_file.write("\n\t\t\t\t{}".format(condition))
                                                #cmd_add_parameters = "var" + "_" + scenario_key + " = " + "var" + "_" + scenario_key + " + " + " (\" -p" + " " + item + "={}\"" + ".format(" + item + "))"
                                                views_file.write("\n\t\t\t\t\tvar" + "_" + scenario_key + " = " + "var" + "_" + scenario_key)
                                                views_file.write(" + (\" {} ".format(emulsion_setup["emulsion_syntax"]["params"]))
                                                views_file.write(item + "={}\"" + ".format(" + item + "))")
                                                #views_file.write("\n\t\t\t\t\t{}".format(cmd_add_parameters))
                                            if item not in list_non_emulsion_parameters and item not in parameters_modifiable: ## Add non emulsion matrice parameters to list of non emulsion paramaters
                                                list_non_emulsion_parameters.append(item)
                                views_file.write("\n")

                                ## Add block values parameters in the emulsion command if it is an emulsion parameters
                                emulsion_parameters_modifiable_block_values = [elem for elem in
                                                                               parameters_modifiable if
                                                                               elem in name_of_block_values_fields]

                                non_emulsion_parameters_modifiable_block_values = [elem for elem in
                                                                               name_of_block_values_fields if
                                                                               elem not in emulsion_parameters_modifiable_block_values]

                                for elem in non_emulsion_parameters_modifiable_block_values:
                                    if elem not in list_non_emulsion_parameters:
                                        list_non_emulsion_parameters.append(elem) ## Add non emulsion parameters in block values

                                if input_form_name not in input_form_name_non_emulsion:
                                    input_form_name_non_emulsion.append(input_form_name)

                                #### Create dictionnary for each scenario as key and non emulsion parameters as values
                                views_file.write("\n\t\t\t\tdict_non_emulsion_parameters[\"{}\"]={{".format(scenario_key))
                                mydict1={}
                                for elem in list_non_emulsion_parameters:
                                    if elem == list_non_emulsion_parameters[-1]:
                                        views_file.write("\"{}\" : {} ".format(elem, elem))
                                        mydict1[elem]=elem
                                    else:
                                        views_file.write("\"{}\" : {} ,".format(elem, elem))
                                        mydict1[elem]=elem

                                views_file.write("}")
                                dict_non_emulsion_parameters[scenario_key] = mydict1

                                views_file.write(
                                    "\n\t\t\t\tdict_non_emulsion_parameters_in_scenario[\"{}\"]={{".format(scenario_key)) ### non emulsion paramaters for each scenario in scenario specific parameters

                                list_dict_non_emulsion_parameters_in_scenario = []
                                for param_item in param_all[scenario_key]: ### All parameters associated to a specific scenario
                                    if type(param_item) is dict:  ## for each form step, if scenario param is a dictionary, then this parameter is from scenarios
                                        for key2, value2 in param_item.items():
                                            if key2 not in parameters_modifiable:
                                                list_dict_non_emulsion_parameters_in_scenario.append({key2: value2})
                                                list_non_emulsion_parameters_in_scenario.append(key2)

                                for list_dict_non_emulsion_parameters_in_scenario_item in list_dict_non_emulsion_parameters_in_scenario:

                                    for list_dict_non_emulsion_parameters_in_scenario_item_key, list_dict_non_emulsion_parameters_in_scenario_item_value in list_dict_non_emulsion_parameters_in_scenario_item.items():
                                        if list_dict_non_emulsion_parameters_in_scenario_item == list_dict_non_emulsion_parameters_in_scenario[-1]:
                                            views_file.write("\"{}\":{}".format(list_dict_non_emulsion_parameters_in_scenario_item_key,list_dict_non_emulsion_parameters_in_scenario_item_value))
                                        else:
                                            views_file.write("\"{}\":{},".format(list_dict_non_emulsion_parameters_in_scenario_item_key,list_dict_non_emulsion_parameters_in_scenario_item_value))
                                views_file.write("}")

                                views_file.write("\n\t\t\t\tself.request.session['dict_non_emulsion_parameters_{}'] = dict_non_emulsion_parameters".format(input_form_name))
                                views_file.write(
                                    "\n\t\t\t\tself.request.session['dict_non_emulsion_parameters_in_scenario_{}'] = dict_non_emulsion_parameters_in_scenario".format(input_form_name))


                                for item in emulsion_parameters_modifiable_block_values:
                                    condition = "if " + item + " != " + "''" + " or " + item + " is not None" + ":"
                                    views_file.write("\n\t\t\t\t{}".format(condition))
                                    #cmd_add_parameters = "var" + "_" + scenario_key + " = " + "var" + "_" + scenario_key + " + " + " (\" -p" + " " + item + "={}\"" + ".format(" + item + "))"
                                    views_file.write("\n\t\t\t\t\tvar" + "_" + scenario_key + " = " + "var" + "_" + scenario_key)
                                    views_file.write(" + (\" {} ".format(emulsion_setup["emulsion_syntax"]["params"]))
                                    views_file.write(item + "={}\"" + ".format(" + item + "))")
                                    #views_file.write("\n\t\t\t\t\t{}".format(cmd_add_parameters))

                                views_file.write("\n")

                                ## Add emulsion setup parameters in the emulsion command
                                if number_of_repetitions != '':
                                    #cmd_add_number_of_repetitions = "var" + "_" + scenario_key + " = " + "var" + "_" + scenario_key + " + " + "\"" + " -r" + " " + str(number_of_repetitions) + "\""
                                    views_file.write("\n\n\t\t\t\tvar" + "_" + scenario_key + " = " + "var" + "_" + scenario_key + " + " + "\"" + " -r" + " " + str(
                                        number_of_repetitions) + "\"".format(emulsion_setup["emulsion_syntax"]["runs"]))
                                    #views_file.write("\n\n\t\t\t\t{}".format(cmd_add_number_of_repetitions))

                                for param_item in param_all[scenario_key]:
                                    if type(param_item) is dict:  ## for each form step, if scenario param is a dictionary, then this parameter is from scenarios
                                        for key2, value2 in param_item.items():
                                            if key2 in parameters_modifiable:  ## if scenario keys are in emulsion parameters
                                                list_scenario_param.append(param_item)
                                                scenario_dict[scenario_key] = list_scenario_param
                                                #cmd_add_parameters = "var" + "_" + scenario_key + " = " + "var" + "_" + scenario_key + " + " + "\"" + " -p" + " " + key2 + "=" + str(value2) + "\""
                                                views_file.write("\n\t\t\t\tvar" + "_" + scenario_key + " = " + "var" + "_" + scenario_key)
                                                views_file.write(" + \" {} ".format(emulsion_setup["emulsion_syntax"]["params"]))
                                                views_file.write(key2 + "=" + str(value2) + "\"")
                                                #views_file.write("\n\t\t\t\t{}\n".format(cmd_add_parameters))

                            fields = []
                            list_fields = []
                            for attributes in step[input_form_name]: ### Faire pour le previous fields
                                for field in attributes.keys():
                                    input_form_field_list.append(field)
                                    if input_form_name != list_scenarios_form[-1]:
                                        fields.append(field)
                                        previous_fields[input_form_name] = fields

                                        if field in matrice_cells_name_dict.keys():
                                            list_fields = list_fields + matrice_cells_name_dict[field]

                                        previous_fields[input_form_name] = list(set(fields) | set(list_fields) | set(emulsion_parameters_modifiable_block_values))

                            if input_form_name == list_scenarios_form[-1] and len(list_scenarios_form) > 1: ### If forms of scenarios are more that one, add the previous form associated to the scenario
                                for item in list_scenarios_form:
                                    for previous_step_name, previous_field_value in previous_fields.items():
                                        if item == previous_step_name:
                                            views_file.write(
                                                "\n\n\t\t\t\tprev_data_{} = self.storage.get_step_data('{}')\n".format(
                                                    str(input_form_step_number[previous_step_name]),
                                                    str(input_form_step_number[previous_step_name])))

                                            for previous_field in previous_field_value:
                                                if previous_field in parameters_modifiable: ### if value of the previous field is in parameters_modifiable then add it to the current emulsion command
                                                    previous_field_new = previous_field + "_" + str(
                                                        input_form_step_number[previous_step_name])
                                                    views_file.write(
                                                        "\n\t\t\t\t{} = prev_data_{}.get('{}-{}', '')".format(
                                                            previous_field_new,
                                                            str(input_form_step_number[previous_step_name]),
                                                            str(input_form_step_number[previous_step_name]),
                                                            previous_field))
                                                    condition = "if " + previous_field_new + " != " + "''" + " or " + previous_field_new + " is not None" + ":" ## use django session to retrive the values of fields entered in the previous form
                                                    views_file.write("\n\t\t\t\t{}".format(condition))
                                                    #cmd_add_parameters = "var" + "_" + scenario_key + " = " + "var" + "_" + scenario_key + " + " + " (\" -p" + " " + previous_field + "={}\"" + ".format(" + previous_field_new + "))"
                                                    views_file.write("\n\t\t\t\t\tvar" + "_" + scenario_key + " = " + "var" + "_" + scenario_key)
                                                    views_file.write(" + (\" {} ".format(emulsion_setup["emulsion_syntax"]["params"]))
                                                    views_file.write(previous_field + "={}\"" + ".format(" + previous_field_new + "))")

                                                    #views_file.write("\n\t\t\t\t\t{}\n".format(cmd_add_parameters))

                                                    for param_item in param_all[scenario_key]:
                                                        if type(param_item) is dict:
                                                            for key2, value2 in param_item.items():
                                                                if key2 in parameters_modifiable:  ## if scenario keys are in emulsion parameters
                                                                    list_scenario_param.append(param_item)
                                                                    scenario_dict[key] = list_scenario_param


                            if input_form_name == list_scenarios_form[-1]: ## if scenario is the last form of a scenario
                                emulsion_parameters_modifiable = [elem for elem in parameters_modifiable if
                                                                  elem in input_form_field_list]


                                ## If input form fields or scenarios parameters are emulsion parameters, then add it to emulsion cmd as parameters
                                for item in emulsion_parameters_modifiable:
                                    condition = "if " + item + " != " + "''" + " or " + item + " is not None" + ":"
                                    views_file.write("\n\t\t\t\t{}".format(condition))
                                    #cmd_add_parameters = "var" + "_" + scenario_key + " = " + "var" + "_" + scenario_key + " + " + " (\" -p" + " " + item + "={}\"" + ".format(" + item + "))"

                                    views_file.write("\n\t\t\t\t\tvar" + "_" + scenario_key + " = " + "var" + "_" + scenario_key)
                                    views_file.write(" + (\" {} ".format(emulsion_setup["emulsion_syntax"]["params"]))
                                    views_file.write(item + "={}\"" + ".format(" + item + "))")

                                    #views_file.write("\n\t\t\t\t\t{}".format(cmd_add_parameters))


                                ## Create a session of each emulsion command to run it later in the done function
                                session = "self.request.session[" + "'" + "var" + "_" + scenario_key + "'" + "]" + " = " + "var" + "_" + scenario_key
                                views_file.write("\n\n\t\t\t\t{}\n\n".format(session))
                                session_done = "var" + "_" + scenario_key + " = " + "self.request.session[" + "'" + "var" + "_" + scenario_key + "'" + "]"
                                cmd_name = "var" + "_" + scenario_key
                                session_list.append(session_done)
                                cmd_list.append(cmd_name)

                    ## list of final non emulsion parameters in form field, block values (not name of block_values) and matrice (only fields of matrices)
                    list_non_emulsion_parameters_global.extend(list_non_emulsion_parameters)
                ## Calculate the time_unit and add it to the emulsion cmd

                for attributes in step[input_form_name]:
                    for field in attributes.keys():
                        if "total_duration" == field:
                            total_duration = "total_duration = int(total_duration)"
                            views_file.write("\n\n\t\t\t\t{}".format(total_duration))
                            total_duration_session_name = "self.request.session[" + "'" + "total_duration" + "'" "]"
                            total_duration_session = "self.request.session[" + "'" + "total_duration" + "'" "]" + " = " + "total_duration"
                            views_file.write("\n\n\t\t\t\t{}".format(total_duration_session))

                        if "simulation_name" == field:
                            simulation_name_session_name = "self.request.session[" + "'" + "simulation_name" + "'" "]"
                            simulation_name_session = "self.request.session[" + "'" + "simulation_name" + "'" "]" + " = " + "simulation_name"
                            views_file.write("\n\n\t\t\t\t{}".format(simulation_name_session))

            #views_file.write("\n\t\t\t\tBASE_DIR = \"{}\"".format(BASE_DIR))
            views_file.write("\n\t\t\t\tshinyapp_file = BASE_DIR + \"/shinyapp/\" + \"app.R\"")
            views_file.write("\n\t\t\t\tfor line in fileinput.input(shinyapp_file, inplace=True):"
                             "\n\t\t\t\t\tprint(re.sub(r'simulation_name <- \".+\"', 'simulation_name <- \\\"{}\\\"'.format(simulation_name), line.rstrip()))")

            views_file.write("\n\t\t\t\t")
            views_file.write(
                    "\n\t\t\t\tscenario_dict={}\n".format(scenario_dict))

            views_file.write("\n\n\t\t\tif self.steps.current == self.steps.last:"
                             "\n\t\t\t\treturn self.render_done(form, **kwargs)"
                             "\n\t\t\telse:"
                             "\n\t\t\t\treturn self.render_next_step(form)"
                             "\n\n\t\treturn self.render(form)")

            ## Create done function in wizard view to run emulsion. Retrieve the session variables (emulsion command) and run the emulsion task using celery fonction from tasks.py file
            views_file.write("\n\n\tdef done(self, form_list, form_dict, **kwargs):")
            for item in session_list:
                views_file.write("\n\t\t{}".format(item))
            views_file.write("\n")

            views_file.write("\n\t\toutput_directory_path = BASE_DIR + \"/emulsion_outputs/\"")
            views_file.write("\n\t\tsimulation_name = str({})".format(simulation_name_session_name))
            views_file.write("\n")

            if total_duration:
                for item in cmd_list:
                    views_file.write(
                        "\n\t\t{} = {} + (\" -t \") + str(int({})) + \" {} \" + output_directory_path + \"output_\" + simulation_name + \"/output_\" + \"{}\"".format(item, item, total_duration_session_name, emulsion_setup["emulsion_syntax"]["outputs"], item.replace("var_", "")))
            views_file.write("\n")

            views_file.write("\n\t\tlist_scenarios_name = {}".format(list_scenarios_name))
            views_file.write("\n\t\tscenario_output_directory = {}")
            views_file.write("\n\t\tfor item in list_scenarios_name:")
            views_file.write(
                "\n\t\t\tscenario_output_directory[item] = output_directory_path + \"output_\" + simulation_name + \"/output_\" + item + \"/counts.csv\"")
            views_file.write("\n")

            views_file.write("\n\t\tcmd_dict = {")
            for i in cmd_list:
                if i == cmd_list[-1]:
                    views_file.write("\'{}\' : {}".format(i, i))
                else:
                    views_file.write("\'{}\' : {},".format(i, i))
            views_file.write("}")


            if len(dict_optional_no) > 0 and type(dict_optional_no["no"]) is list:
                views_file.write("\n\t\tscenario_not_optional = {}".format(["var_" + s for s in dict_optional_no["no"]]))
            else:
                views_file.write("\n\t\tscenario_not_optional = {}")
            views_file.write("\n\t\tlist_scenarios_name_to_run = []")

            views_file.write("\n")
            views_file.write("\n\t\tfor scenario in scenario_not_optional:")
            views_file.write("\n\t\t\tfor key, value in cmd_dict.items():")
            views_file.write("\n\t\t\t\tif scenario == key:")
            views_file.write("\n\t\t\t\t\tlist_scenarios_name_to_run.append(key)")

            views_file.write("\n")
            views_file.write("\n\t\tscenario_optional_list = self.request.session['selecting_scenarios_to_run']")
            views_file.write("\n\t\tfor scenario_optional in scenario_optional_list:")
            views_file.write("\n\t\t\tvar_scenario_optional = \"var_\" + scenario_optional")
            views_file.write("\n\t\t\tfor keys, values in cmd_dict.items():")
            views_file.write("\n\t\t\t\tif var_scenario_optional == keys:")
            views_file.write("\n\t\t\t\t\tlist_scenarios_name_to_run.append(var_scenario_optional)")

            views_file.write("\n")

            views_file.write("\n\t\tself.request.session['dict_non_emulsion_parameters'] ={")
            for input_form_name_non_emulsion_item in input_form_name_non_emulsion:
                if input_form_name_non_emulsion_item == input_form_name_non_emulsion[-1]:
                    views_file.write("**self.request.session['dict_non_emulsion_parameters_{}']".format(input_form_name_non_emulsion_item))
                else:
                    views_file.write("**self.request.session['dict_non_emulsion_parameters_{}'],".format(
                        input_form_name_non_emulsion_item))
            views_file.write("}")

            views_file.write("\n\t\tself.request.session['dict_non_emulsion_parameters_in_scenario'] ={")
            for input_form_name_non_emulsion_item in input_form_name_non_emulsion:
                if input_form_name_non_emulsion_item == input_form_name_non_emulsion[-1]:
                    views_file.write("**self.request.session['dict_non_emulsion_parameters_in_scenario_{}']".format(
                        input_form_name_non_emulsion_item))
                else:
                    views_file.write("**self.request.session['dict_non_emulsion_parameters_in_scenario_{}'],".format(
                        input_form_name_non_emulsion_item))
            views_file.write("}")

            views_file.write("\n\t\tfor dict_non_emulsion_parameters_key, dict_non_emulsion_parameters_value in self.request.session['dict_non_emulsion_parameters'].items():")
            views_file.write("\n\t\t\tfor dict_non_emulsion_parameters_in_scenario_key, dict_non_emulsion_parameters_in_scenario_value in self.request.session['dict_non_emulsion_parameters_in_scenario'].items():")
            views_file.write("\n\t\t\t\tif dict_non_emulsion_parameters_in_scenario_key == dict_non_emulsion_parameters_key:")
            views_file.write("\n\t\t\t\t\tfor key, value in dict_non_emulsion_parameters_value.items():")
            views_file.write("\n\t\t\t\t\t\tfor key1, value1 in dict_non_emulsion_parameters_in_scenario_value.items():")
            views_file.write("\n\t\t\t\t\t\t\tif key == key1:")
            views_file.write("\n\t\t\t\t\t\t\t\tself.request.session['dict_non_emulsion_parameters'][dict_non_emulsion_parameters_key].update(dict_non_emulsion_parameters_in_scenario_value)")


            views_file.write("\n")
            views_file.write("\n\t\tdict_eco= {}")
            views_file.write("\n\t\tfor key, values in self.request.session['dict_non_emulsion_parameters'].items():")
            views_file.write("\n\t\t\tnew_list = []")
            views_file.write("\n\t\t\tfor key_value, value_value in values.items():")
            views_file.write("\n\t\t\t\tif value_value is not None:")
            views_file.write("\n\t\t\t\t\tnew_list.append(str(value_value))")
            views_file.write("\n\t\t\tdict_eco[key] = new_list")

            views_file.write("\n")
            views_file.write("\n\t\tdict_each_scenario = {}")
            views_file.write("\n\t\tfor item in list_scenarios_name_to_run:")
            views_file.write("\n\t\t\tlist_each_scenario = []"
                             "\n\t\t\tlist_each_scenario.append(scenario_output_directory[item.replace('var_', '')])"
                             "\n\t\t\tlist_each_scenario.append(item.replace('var_',''))"
                             "\n\t\t\tlist_each_scenario.append(simulation_name)")
            views_file.write("\n\t\t\tfor number in dict_eco[item.replace('var_', '')]:")
            views_file.write("\n\t\t\t\tlist_each_scenario.append(number)")
            views_file.write("\n\t\t\tdict_each_scenario[item.replace('var_', '')] = list_each_scenario")

            views_file.write("\n")
            views_file.write("\n\t\tapp_name = \"{}\"".format(app_name))

            views_file.write("\n\t\tlist_scenarios_name_comp = ' '.join(list_scenarios_name_to_run).replace('var_','').split()")
            views_file.write("\n\t\tlist_scenario_comparison =[BASE_DIR + \"/\" + app_name + \"/\" + \"Rscripts/output_Rdata_\" + simulation_name + \"/\" + item + \".Rdata\" for item in list_scenarios_name_comp]")
            views_file.write("\n\t\tlist_scenario_comparison.append(simulation_name)")

            views_file.write("\n")
            views_file.write("\n\t\ttask = chord([chain(run_scenario.si(cmd_dict[item]), "
                             "run_statistics_for_each_scenario.si(dict_each_scenario[item.replace('var_', '')])) for item in list_scenarios_name_to_run],"
                             "run_scenarios_comparison.si(list_scenario_comparison))()")

            views_file.write("\n\t\tcontext={}")
            views_file.write("\n\t\tcontext['task_id'] = task.id")
            views_file.write("\n\n\t\treturn render(self.request, 'done.html', context)")

            ## Create the task view class to return json response in html template and take account the status of the emulsion run in the background before rendering the result template page.
            views_file.write("\n\nclass TaskView(View):\n")
            views_file.write("\n\tdef get(self, request, task_id):")
            views_file.write("\n\t\ttask = current_app.AsyncResult(task_id)")
            views_file.write("\n\t\tresponse_data = {'task_status': task.status, 'task_id': task.id}")
            views_file.write("\n\t\tif task.status == 'SUCCESS':")
            views_file.write("\n\t\t\tresponse_data['results'] = task.get()")
            views_file.write("\n\t\treturn JsonResponse(response_data)")

            ## View for result file
            views_file.write("\n\ndef dashapp(request):"
                             "\n\treturn render(request, 'result.html')")

            views_file.write("\n\ndef shiny(request):"
                "\n\treturn render(request, 'shiny.html')")

            views_file.write("\n\ndef shiny_contents(request):"
                "\n\tresponse = requests.get('http://localhost:8100')"
                "\n\tsoup = BeautifulSoup(response.content, 'html.parser')"
                "\n\treturn JsonResponse({'html_contents': str(soup)})")


            print("\nCreating the emulsion task file using celery")

            if not os.path.exists(BASE_DIR + "/" + app_name + "/Rscripts/"):
                os.makedirs(BASE_DIR + "/" + app_name + "/Rscripts/")

            ## Create the emulsion task file in the background using celery
            celery_task_file = open(BASE_DIR + "/" + app_name + "/tasks.py", "w+")
            celery_task_file.write("from celery import shared_task, chord, group, chain")
            celery_task_file.write("\nimport subprocess"
                                   "\nimport os"
                                   "\nBASE_DIR=os.getcwd()")

            celery_task_file.write("\n\n@shared_task")
            celery_task_file.write("\ndef run_scenario(scenario_name=\"\"):")
            cmd = "cmd" + "=" + "subprocess.run(scenario_name, shell=True)"
            celery_task_file.write("\n\t{}".format(cmd))

            celery_task_file.write("\n\n@shared_task")
            celery_task_file.write("\ndef run_statistics_for_each_scenario(*list_parameters_for_each_scenario):")
            celery_task_file.write("\n\tcmd = [\"Rscript\", \"statistics.R\"]")
            celery_task_file.write("\n\tflat_list = [item for sublist in list(list_parameters_for_each_scenario) for item in sublist]")
            celery_task_file.write("\n\tcmd = cmd + flat_list")
            celery_task_file.write("\n\tp = subprocess.Popen(cmd, cwd=BASE_DIR + \"/{}/Rscripts/\" ,"
                                   "stdin=subprocess.PIPE, stdout=subprocess.PIPE, "
                                   "stderr=subprocess.PIPE)".format(app_name))
            celery_task_file.write("\n\toutput, error = p.communicate()")
            celery_task_file.write("\n\tif p.returncode == 0:"
                                   "\n\t\tprint(\'R OUTPUT:\\n {0}\'.format(output))"
                                   "\n\telse:"
                                   "\n\t\tprint(\'R ERROR:\\n {0}\'.format(error))")

            celery_task_file.write("\n\n@shared_task")
            celery_task_file.write("\ndef run_scenarios_comparison(*list_of_scenarios):")
            celery_task_file.write("\n\tcmd = [\"Rscript\", \"statistics_comparison_scenario.R\"]")
            celery_task_file.write("\n\tflat_list = [item for sublist in list(list_of_scenarios) for item in sublist]")
            celery_task_file.write("\n\tcmd = cmd + flat_list")
            celery_task_file.write("\n\tp = subprocess.Popen(cmd, cwd=BASE_DIR + \"/{}/Rscripts/\", "
                                   "stdin=subprocess.PIPE, stdout=subprocess.PIPE, "
                                   "stderr=subprocess.PIPE)".format(app_name))
            celery_task_file.write("\n\toutput, error = p.communicate()")
            celery_task_file.write("\n\tif p.returncode == 0:"
                                   "\n\t\tprint(\'R OUTPUT:\\n {0}\'.format(output))"
                                   "\n\telse:"
                                   "\n\t\tprint(\'R ERROR:\\n {0}\'.format(error))")

            ## Creating R statistics files for each scenario
            commands_name = []
            statistics_for_each_scenario_file = open(BASE_DIR + "/" + app_name + "/Rscripts/statistics.R", "w+")
            statistics_for_each_scenario_file.write("library(dplyr)\nlibrary(readr)\nlibrary(tidyr)\nlibrary(ggplot2)")
            statistics_for_each_scenario_file.write("\n\nargs <- commandArgs(trailingOnly=TRUE)")
            statistics_for_each_scenario_file.write("\ndata <- read_csv(args[1])")
            statistics_for_each_scenario_file.write("\nscenario_name <- args[2]")
            statistics_for_each_scenario_file.write("\nsimulation_name <- args[3]")
            statistics_for_each_scenario_file.write("\ndata['scenario_name'] <- scenario_name"
                                                    "\ndata['simulation_name'] <- simulation_name"
                                                    "\nwrite.csv(data, args[1])\n") ## adding scenario name into emulsion output file counts.csv


            # Create list that corresponds to non emulsion parameters and add it to statistics.R
            for item_scenario in list_non_emulsion_parameters_in_scenario:
                if item_scenario not in list_non_emulsion_parameters_global:
                    list_non_emulsion_parameters_global.append(item_scenario)
                else:
                    print("This field is already added in non emulsion parameters")


            final_list_non_emulsion_parameters = list_non_emulsion_parameters_global
            for name_matrice in list_matrices_name_view:
                if name_matrice in final_list_non_emulsion_parameters:
                    final_list_non_emulsion_parameters.remove(name_matrice)

            ## Add non emulsion parameters as argument in statistics.R
            for parameters in final_list_non_emulsion_parameters:
                statistics_for_each_scenario_file.write("\n{} <- as.numeric(args[{}])".format(parameters, final_list_non_emulsion_parameters.index(parameters)+4))

            statistics_for_each_scenario_file.write("\nassign(paste(\"data\", scenario_name, sep=\"_\"),data)")
            statistics_for_each_scenario_file.write("\nMAX.STEP <- max(data$step - 1)")
            statistics_for_each_scenario_file.write("\nNB.SIMU <- max(data$simu_id + 1)")
            statistics_for_each_scenario_file.write("\n")

            command=""
            command_desc={}
            dataframe_command_variables={}
            by_value=""

            for item in statistics['for_each_scenario']:
                for key1, value1 in item.items():

                    target_variable = ""
                    summarise_cmd = ""
                    command = key1 + " <- data"
                    dataframe_command_variables[key1]=[]
                    commands_name.append(key1)

                    for item2 in value1:
                        for key2, value2 in item2.items():
                            if key2 == "groupby":
                                groupby = ', '.join(value2)
                                groupby_cmd = "group_by(" + groupby + ")"
                                command = command + " %>% " + groupby_cmd

                                if type(value2) is list:
                                    for item in value2:

                                        dataframe_command_variables[key1].append(item)

                            if key2 == "target_variable":
                                target_variable = ', '.join(value2)
                                if target_variable not in dataframe_command_variables[key1]:
                                    dataframe_command_variables[key1].append(target_variable)

                            if key2 == "summarise":
                                summarise_list = []
                                if type(value2) is list:

                                    for summarise_item in value2:

                                        if type(summarise_item) is str:

                                            summarise_list.append(summarise_item + "_" + target_variable + "=" + summarise_item + "(" + target_variable + ")")
                                            summarise_new_variable = summarise_item + "_" + target_variable

                                            if summarise_new_variable not in dataframe_command_variables[key1]:
                                                dataframe_command_variables[key1].append(summarise_new_variable)

                                        if type(summarise_item) is dict:

                                            for summarise_item_key, summarise_item_value in summarise_item.items():

                                                for summarise_item_value_key, summarise_item_value_value in summarise_item_value.items():
                                                    if summarise_item_value_key == "value":
                                                        if target_variable != "":
                                                            adding_target_variables = re.sub(r'(.*\()\.\.\.', r'\1'+target_variable, summarise_item_value_value) # recreate quantile function by adding the target variable I
                                                            summarise_dataframe_variable = summarise_item_key + "_" + target_variable
                                                        else:
                                                            adding_target_variables =  summarise_item_value_value # recreate quantile function by adding the target variable I

                                                            summarise_dataframe_variable = summarise_item_key

                                                        if summarise_item_key not in dataframe_command_variables[key1]:
                                                            dataframe_command_variables[key1].append(summarise_dataframe_variable)

                                                        if target_variable != "":
                                                            modified_summarise = summarise_item_key + "_" + target_variable + "=" + adding_target_variables
                                                        else:
                                                            modified_summarise = summarise_item_key +  "=" + adding_target_variables

                                                        summarise_list.append(modified_summarise)

                                                    if summarise_item_value_key == "desc":
                                                        print(summarise_item_value_key, summarise_item_value_value)

                                        summarise_cmd = "summarise(" + ', '.join(summarise_list) + ")"
                                    command = command + " %>% " + summarise_cmd

                            if key2 == "sort":
                                if type(value2) is list:
                                    for item in value2:
                                        if item == summarise_new_variable:
                                            sort_cmd = "arrange(" + item + ")"
                                            command = command + " %>% " + sort_cmd

                                            if item not in dataframe_command_variables[key1]:
                                                dataframe_command_variables[key1].append(item)

                            if key2 == "condition":
                                if type(value2) is list:
                                    for item in value2:
                                        condition_cmd = "filter(" + item + ")"
                                        command = command + " %>% " + condition_cmd

                                        item_condition =  re.sub(r'(.*)\s==.*', r'\1', item)

                                        if item not in dataframe_command_variables[key1]:
                                            dataframe_command_variables[key1].append(item_condition)

                                elif type(value2) is dict:

                                    for name, formule in value2.items():
                                        condition_cmd = "filter(" + name + "==" + formule + ")"
                                        command = command + " %>% " + condition_cmd

                                        if name not in dataframe_command_variables[key1]:
                                            dataframe_command_variables[key1].append(name)

                            if key2 == "print_only":
                                if type(value2) is list:
                                    for item in value2:
                                        select_cmd = "select(" + item + ")"
                                        command = command + " %>% " + select_cmd

                                        if item not in dataframe_command_variables[key1]:
                                            dataframe_command_variables[key1].append(item)

                            if key2 == "add_new_column":
                                if type(value2) is dict:
                                    for name, formule in value2.items():
                                        mutate_cmd = "mutate(" + name + "=" + formule["value"] + ")"
                                        command = command + " %>% " + mutate_cmd
                                        command_desc[key1] = formule["desc"]

                                        if name not in dataframe_command_variables[key1]:
                                            dataframe_command_variables[key1].append(name)

                            if key2 == "inner_join":
                                command_to_join = ""
                                if type(value2) is dict:
                                    for dict_key, dict_value in value2.items():
                                        if dict_key == "with":
                                            command_to_join = dict_value
                                        if dict_key == "by":
                                            if type(dict_value) is list:
                                                by_value = (', '.join('"' + item + '"' for item in dict_value))

                                    if command_to_join in commands_name:
                                        #command = key1 + " <- " + command_to_join + " %>% inner_join(" + "data" + ",by=" + by_value + ")"
                                        inner_join_command = "inner_join(" + command_to_join + " ,by=" + by_value + ")"
                                    command = command + " %>% " + inner_join_command

                statistics_for_each_scenario_file.write("\n{}".format(command))
                statistics_for_each_scenario_file.write("\n{}['scenario_name'] <- scenario_name".format(key1))
                statistics_for_each_scenario_file.write("\nassign(paste(\"{}\", scenario_name, sep=\"_\"),{})".format(key1, key1))
                statistics_for_each_scenario_file.write("\n")

            statistics_for_each_scenario_file.write("\ncommands_list <- grep(pattern = scenario_name, ls(), value = TRUE)")
            statistics_for_each_scenario_file.write("\nmain_dir <- getwd()")
            statistics_for_each_scenario_file.write("\nsub_dir <- paste(\"output_Rdata_\", simulation_name,sep=\"\")")
            statistics_for_each_scenario_file.write("\nif (file.exists(sub_dir)){\n\tsetwd(file.path(main_dir, sub_dir))"
                                                    "\n} else {"
                                                    "\n\tdir.create(file.path(main_dir, sub_dir))"
                                                    "\n\tsetwd(file.path(main_dir, sub_dir))"
                                                    "\n}")

            statistics_for_each_scenario_file.write("\nfile_name <- paste(main_dir,\"/\", sub_dir, \"/\", scenario_name,\".Rdata\",sep=\"\")")
            statistics_for_each_scenario_file.write("\nsave(list = commands_list, file = file_name)")

            ## Creating R statistics files for scenario comparison
            left_join_by_comp = ""
            left_join_with_comp = ""
            mutate_cmd_comp = ""
            key1_comp = ""
            target_variable_comp =" "
            command_scenario_comparison_list = []

            statistics_comparison_scenario_file = open(BASE_DIR + "/" + app_name + "/Rscripts/statistics_comparison_scenario.R", "w+")
            statistics_comparison_scenario_file.write("library(dplyr)\nlibrary(readr)\nlibrary(tidyr)\nlibrary(ggplot2)")
            statistics_comparison_scenario_file.write("\n\nargs <- commandArgs(trailingOnly=TRUE)")
            statistics_comparison_scenario_file.write("\nfor (arg in args) {"
                                                      "\n\t if ( arg == (tail(args, n=1))) {"
                                                      "\n\t\tsimulation_name <- tail(args, n=1)"
                                                      "\n\t} else {"
                                                      "\n\t\tload(arg)\n}"
                                                      "\n\t}")

            for item_comp in statistics['comparison_of_scenarios']:

                command_comp = ""
                for key1_comp, value1_comp in item_comp.items():
                    commands_name.append(key1_comp)
                    command_scenario_comparison_list.append(key1_comp)
                    dataframe_command_variables[key1_comp]=[]
                    for item2_comp in value1_comp:
                        for key2_comp, value2_comp in item2_comp.items():
                            summarise_new_variable_comp = ""
                            if key2_comp == "scenarios_to_compare":
                                join1 = value2_comp[0]
                                join2 = value2_comp[1]
                                suffix_comp = (', '.join('"_' + item + '"' for item in value2_comp))

                            if key2_comp == "left_join":
                                for key3_comp, value3_comp in value2_comp.items():

                                    if key3_comp == "by":
                                        if type(value3_comp) is list:
                                            left_join_by_comp = (', '.join('"' + item + '"' for item in value3_comp))

                                            for item_comp in value3_comp:
                                                dataframe_command_variables[key1_comp].append(item_comp)

                                    if key3_comp == "with":
                                            left_join_with_comp = value3_comp

                            if key2_comp == "groupby":
                                groupby_comp = ', '.join(value2_comp)
                                groupby_cmd_comp = "group_by(" + groupby_comp + ")"
                                command_comp = command_comp + " %>% " + groupby_cmd_comp

                                if type(value2_comp) is list:
                                    for item_comp in value2_comp:
                                        if item_comp not in dataframe_command_variables[key1_comp]:
                                            dataframe_command_variables[key1_comp].append(item_comp)

                            if key2_comp == "target_variable":
                                target_variable_comp = ', '.join(value2_comp)
                                dataframe_command_variables[key1_comp].append(target_variable_comp)

                            if key2_comp == "summarise":
                                if type(value2_comp) is list:

                                    summarise_comp = ', '.join(value2_comp)
                                    summarise_cmd_comp = "summarise(" + summarise_comp + "_" + target_variable_comp + "=" + summarise_comp + "(" + target_variable_comp + "))"
                                    summarise_new_variable_comp = summarise_comp + "_" + target_variable_comp ## test if it is identical to sort value

                                    dataframe_command_variables[key1_comp].append(summarise_new_variable_comp)
                                    command_comp = command_comp + " %>% " + summarise_cmd_comp

                                elif type(value2_comp) is dict:

                                    for name_comp, formule_comp in value2_comp.items():
                                        summarise_cmd_comp = "summarise(" + name_comp + "=" + formule_comp + ")"
                                        command_comp = command_comp + " %>% " + summarise_cmd_comp

                                        if name_comp not in dataframe_command_variables[key1_comp]:
                                            dataframe_command_variables[key1_comp].append(name_comp)


                            if key2_comp == "sort":
                                if type(value2_comp) is list:
                                    for item_comp in value2_comp:
                                        if item_comp == summarise_new_variable_comp:
                                            sort_cmd_comp = "arrange(" + item_comp + ")"
                                            command_comp = command_comp + " %>% " + sort_cmd_comp

                                        if item_comp == target_variable_comp:
                                            sort_cmd_comp = "arrange(" + item_comp + ")"
                                            command_comp = command_comp + " %>% " + sort_cmd_comp

                            if key2_comp == "add_new_column":
                                if type(value2_comp) is dict:
                                    for name_comp, formule_comp in value2_comp.items():
                                        mutate_cmd_comp = "mutate(" + name_comp + "=" + formule_comp["value"] + ")"

                                        if name_comp not in dataframe_command_variables[key1_comp]:
                                            dataframe_command_variables[key1_comp].append(name_comp)

                                        if key1_comp in command_scenario_comparison_list:
                                            key1_comp_new = key1_comp + "_scenario_comparison"
                                            command_desc[key1_comp_new] = formule_comp["desc"]
                                        else:
                                            command_desc[key1_comp] = formule_comp["desc"]

                if left_join_with_comp != "raw data":

                    command_comp_join = key1_comp +  " <- " +  "left_join(" + left_join_with_comp + "_" + join1 + "," + left_join_with_comp + "_" + join2 + ", by=c(" + left_join_by_comp + ")," + "suffix=c(" + suffix_comp + "))" + " %>% " + mutate_cmd_comp
                    statistics_comparison_scenario_file.write("\nif (exists('{}_{}') & is.data.frame(get('{}_{}')) && (exists('{}_{}') && is.data.frame(get('{}_{}')))){{".format(left_join_with_comp, join1, left_join_with_comp, join1, left_join_with_comp, join2, left_join_with_comp, join2))
                    statistics_comparison_scenario_file.write("\n\t{}".format(command_comp_join))
                    statistics_comparison_scenario_file.write(
                        "\n\tassign(paste(\"{}\", \"scenario_comparison\", sep=\"_\"),{})".format(key1_comp, key1_comp))
                    statistics_comparison_scenario_file.write("\n}")
                    statistics_comparison_scenario_file.write("\n")

                if left_join_with_comp == "raw data":

                    command_comp_join_raw_data = key1_comp + " <- " + "left_join(" + "data_" + join1 + command_comp + "," + "data_" + join2 + command_comp + ", by=c(" + left_join_by_comp + ")," + "suffix=c(" + suffix_comp + "))" + " %>% " + mutate_cmd_comp
                    statistics_comparison_scenario_file.write("\nif (exists('data_{}') & is.data.frame(get('data_{}')) && (exists('data_{}') && is.data.frame(get('data_{}')))){{".format(join1, join1, join2, join2))
                    statistics_comparison_scenario_file.write("\n\t{}".format(command_comp_join_raw_data))
                    statistics_comparison_scenario_file.write(
                        "\n\tassign(paste(\"{}\", \"scenario_comparison\", sep=\"_\"),{})".format(key1_comp, key1_comp))
                    statistics_comparison_scenario_file.write("\n}")
                    statistics_comparison_scenario_file.write("\n")

            statistics_comparison_scenario_file.write(
                "\ncommands_list <- grep(pattern = \"scenario_comparison\", ls(), value = TRUE)")
            statistics_comparison_scenario_file.write("\nmain_dir <- getwd()")
            statistics_comparison_scenario_file.write("\nsub_dir <- paste(\"output_Rdata_\", simulation_name,sep=\"\")")
            statistics_comparison_scenario_file.write("\nif (file.exists(sub_dir)){\n\tsetwd(file.path(main_dir, sub_dir))"
                                                    "\n} else {"
                                                    "\n\tdir.create(file.path(main_dir, sub_dir))"
                                                    "\n\tsetwd(file.path(main_dir, sub_dir))"
                                                    "\n}")

            statistics_comparison_scenario_file.write(
                "\nfile_name <- paste(main_dir,\"/\", sub_dir, \"/\",\"scenarios_comparison.Rdata\",sep=\"\")")
            statistics_comparison_scenario_file.write("\nsave(list = commands_list, file = file_name)")
            print("\nCreating command file to import block values from yaml file data into database")

            ## create import data file using networkx and block_values dictionnary
            url_list = []
            import_data_file = open(BASE_DIR + "/" + app_name + "/management/commands/import_data_file.py", "w+")
            import_data_file.write("from django.core.management.base import BaseCommand, CommandError\nimport csv\n"
                                   "import os\nimport sys\nimport pandas as pd\nimport numpy as np\nfrom {}.models import *".format(
                app_name))
            import_data_file.write(
                "\nclass Command(BaseCommand):\n\thelp = 'Import data from csv file to database'\n")
            import_data_file.write("\n\tdef add_arguments(self, parser):")
            for key, value in block_values.items():
                import_data_file.write("\n\t\tparser.add_argument('--filename_{}', type=str)".format(key))

            import_data_file.write("\n\n\tdef handle(self, *args, **options):\n")

            import_model_created = []
            ajax_created = []

            for key, values in sorted_d.items():
                for element in values:
                    if type(element) is list:
                        for i in reversed(element):
                            if i not in ajax_created:
                                ajax_created.append(i)
                                ### Start writing json view function for chained fields script for successor
                                views_file.write("\n\ndef load_{}_details(request):\n"
                                                 "\t{}_id = request.GET.get('{}', None)\n"
                                                 "\tdata = {{\n"
                                                 "\t\t\'is_taken\': list({}.objects.filter(id={}_id).values())\n"
                                                 "\t}}\n"
                                                 "\treturn JsonResponse(data, safe=False)\n".format(i, i, i, i, i))

                                url_list.append("load_{}_details".format(i))

                                ### Continue writing import data script
                            if i not in import_model_created:
                                import_data_file.write("\n")
                                import_data_file.write("\n\t\tfilename_{}= options['filename_{}']\n"
                                                       "\t\tdata_{} = pd.read_csv(filename_{}, sep=',')\n"
                                                       "\t\t{}.objects.all().delete()".format(i, i, i, i,
                                                                                              i))
                                import_data_file.write(
                                    "\n\t\tfor index_{}, row_{} in data_{}.iterrows():".format(i, i, i))
                                import_data_file.write("\n\t\t\t{}_model = {}()".format(i, i))
                                import_model_created.append(i)

                                for field, field_type in block_values[i].items():
                                    if field_type != "manytomany":
                                        import_data_file.write(
                                            "\n\t\t\t{}_model.set_{}(row_{}['{}'])".format(i, field, i,
                                                                                           field))

                                import_data_file.write("\n\t\t\t{}_model.save()".format(i))

                                for field, field_type in block_values[i].items():
                                    if field_type == "manytomany":
                                        import_data_file.write("\n\t\t\t{}_list = []".format(i))
                                        import_data_file.write(
                                            "\n\t\t\tfor each in row_{}[\"{}\"].split(\'|\'):".format(i, field))
                                        import_data_file.write("\n\t\t\t\t{}_list.append(each)".format(i))
                                        import_data_file.write(
                                            "\n\t\t\t{}_model.{}.add(*{}.objects.filter(name__in={}_list))".format(i,
                                                                                                                   field,
                                                                                                                   field,
                                                                                                                   i))
                                import_data_file.write("\n")

            print("\nCreating application url file")
            url_file = open(BASE_DIR + "/" + app_name + "/urls.py", "w+")
            url_file.write("from django.urls import path,  include, re_path")
            url_file.write("\nfrom . import views")
            url_file.write("\nfrom .views import {}".format(form_wizard_name))
            url_file.write("\nfrom .forms import {}\n".format(", ".join(input_form_name_list)))
            url_file.write("\nurlpatterns = [")
            url_file.write("\n\tpath('task/<str:task_id>/', views.TaskView.as_view(), name='task'),")
            url_file.write("\n\tpath(\"select2/\',select2/\", include(\"django_select2.urls\")),")
            url_file.write("\n\tre_path(r'^form/$', {}.as_view([{}]), name='form'),".format(form_wizard_name, ", ".join(
                input_form_name_list)))
            for item in url_list:
                url_file.write("\n\tpath('ajax/{}/', views.{}, name=\'{}\'),".format(item, item, item))
            url_file.write("\n\tpath('shiny/', views.shiny, name='shiny'),")
            url_file.write("\n\tpath('shiny_contents/', views.shiny_contents, name='shiny_contents'),")
            url_file.write("\n]")

            ## Create done.html file for Wizard form
            print("\nCreating file that shows emulsion task progress and redirect to result file")
            done_file = open(BASE_DIR + "/" + app_name + "/templates/" + "done.html", "w+")
            done_file.write("{% extends \"base.html\" %}"
                            "\n{% block content %}"
                            "\n<div class=\"center\" style=\"text-align:center;\" id=\"progress-title\"></div>")

            done_file.write("{% if task_id %}"
                            "\n<script>"
                            "\n\tvar taskUrl = \"{% url 'task' task_id=task_id %}\";"
                            "\n\tvar result = \"{% url 'shiny' %}\";"
                            "\n\tvar dots = 1;"
                            "\n\tvar progressTitle = document.getElementById('progress-title');"
                            "\n\tupdateProgressTitle();")

            done_file.write("\n\tvar timer = setInterval(function() {"
                            "\n\t\tupdateProgressTitle();"
                            "\n\t\taxios.get(taskUrl)"
                            "\n\t\t.then(function(response){"
                            "\n\t\t\tvar taskStatus = response.data.task_status"
                            "\n\t\t\tif (taskStatus === 'SUCCESS') {"
                            "\n\t\t\t\tclearTimer('<h4>You are being redirected to the result page for statistics.</h4>');"
                            "\n\t\t\t\twindow.setTimeout(function () {"
                            "\n\t\t\t\tlocation.href = result"
                            "\n\t\t\t}, 2000);"
                            "\n\t\t\t} else if (taskStatus === 'FAILURE') {"
                            "\n\t\t\t\tclearTimer('An error occurred');"
                            "\n\t\t\t\t}"
                            "\n\t\t\t\t})"
                            "\n\t\t\t\t.catch(function(err){"
                            "\n\t\t\t\tconsole.log('err', err);"
                            "\n\t\t\t\tclearTimer('An error occurred');"
                            "\n\t\t\t\t});"
                            "\n\t\t\t\t}, 800);")
            done_file.write("\n\tfunction updateProgressTitle() {"
                            "\n\t\tprogressTitle.innerHTML = '<h3>Processing simulations. Please wait for results.</h3>';"
                            "\n\t\tprogressTitle.innerHTML += '<p class=\"fa-3x\"><i class=\"fas fa-spinner fa-pulse\"></i></p>';"
                            "\n\t\t}")
            done_file.write("\n\tfunction clearTimer(message) {"
                            "\n\t\tclearInterval(timer);"
                            "\n\t\tprogressTitle.innerHTML = message;"
                            "\n\t}"
                            "\n</script>"
                            "\n{% endif %}"
                            "\n{% endblock %}")

            ## Create base.html file
            base_file = open(BASE_DIR + "/" + app_name + "/templates/" + "base.html", "w+")
            base_file.write("<!DOCTYPE html>\n<html lang=\"en\">\n<head>"
                            "\n\t{% load static %}")
            base_file.write("\n\t<link rel=\"stylesheet\" href=\"{% static \'css/style.css\' %}\">")
            base_file.write("\n\t<meta charset=\"utf-8\">"
                            "\n\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">"
                            "\n\t<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css\" integrity=\"sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO\" crossorigin=\"anonymous\">"
                            "\n\t<script src=\"http://cdn.plot.ly/plotly-latest.min.js\"></script>"
                            "\n\t<link rel=\"stylesheet\" href=\"https://unpkg.com/vue-form-wizard/dist/vue-form-wizard.min.css\">"
                            "\n\t<script src=\"https://unpkg.com/vue-form-wizard/dist/vue-form-wizard.js\"></script>"
                            "\n\t<script src=\"https://cdn.jsdelivr.net/npm/vue\"></script>"
                            "\n\t<script src=\"https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js\"></script>"
                            "\n\t<script defer src=\"https://use.fontawesome.com/releases/v5.0.7/js/all.js\"></script>"
                            "\n\t<link href=\"http://code.jquery.com/ui/1.12.0/themes/smoothness/jquery-ui.css\" rel=\"stylesheet\">"
                            "\n\t<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/css/ion.rangeSlider.min.css\"/>"
                            "\n\t<script src=\"https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script>"
                            "\n\t<script src=\"https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/js/ion.rangeSlider.min.js\"></script>"
                            "\n\t<script src=\"https://use.fontawesome.com/releases/v5.15.1/js/all.js\" data-auto-a11y=\"true\"></script>"
                            "\n\t<link rel=\"stylesheet\" href=\"http://netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css\">"
                            "\n\t<link href=\"https://cdn.jsdelivr.net/gh/gitbrent/bootstrap-switch-button@1.1.0/css/bootstrap-switch-button.min.css\" rel=\"stylesheet\">"
                            "\n\t<script src=\"https://cdn.jsdelivr.net/gh/gitbrent/bootstrap-switch-button@1.1.0/dist/bootstrap-switch-button.min.js\"></script>"
                            "\n\t<script SRC=\"https://cdn.jsdelivr.net/npm/bootstrap-switch-button@1.1.0/dist/bootstrap-switch-button.min.js\"></script>"
                            "\n</head>")

            base_file.write("\n<body>"
                            "\n<nav class=\"navbar navbar-expand-lg navbar-light\" >"
                            "\n\t<a class=\"navbar-brand\" href=\"{% url 'form' %}\">Home</a>"
                            "\n\t\t<button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarNavAltMarkup\" aria-controls=\"navbarNavAltMarkup\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">"
                            "\n\t\t\t<span class=\"navbar-toggler-icon\"></span>"
                            "\n\t\t</button>"
                            "\n\t\t<div class=\"collapse navbar-collapse\" id=\"navbarNavAltMarkup\">"
                            "\n\t\t\t<div class=\"navbar-nav\">"
                            "\n\t\t\t\t<a class=\"nav-item nav-link\" href=\"{% url 'form' %}\">Form</a>"
                            "\n\t\t\t</div>"
                            "\n\t\t</div>"
                            "\n\t</nav>"
                            "\n<br>"
                            "\n<br>"
                            "\n\t<div class=\"container-fluid\">"
                            "\n\t\t<div class=\"row justify-content-md-center\">"
                            "\n\t\t\t<div class=\"col-md-2\"></div>"
                            "\n\t\t\t<div class=\"col-md-8\">{% block head %}{% endblock %}</div>"
                            "\n\t\t\t<div class=\"col-md-2\"></div>"
                            "\n\t\t</div>"
                            "\n\t</div>"

                            "\n\t<div class=\"container-fluid\">"
                            "\n\t\t<div class=\"row justify-content-md-center\">"
                            "\n\t\t\t<div class=\"col-md-2\"></div>"
                            "\n\t\t\t<div class=\"col-md-7\">{% block content %}{% endblock %}</div>"
                            "\n\t\t\t<div class=\"col-md-3\"></div>"
                            "\n\t\t</div>"
                            "\n\t</div>"

                            "\n\t<div class=\"container-fluid\">"
                            "\n\t\t<div class=\"row justify-content-md-center\">"
                            "\n\t\t\t<div class=\"col-md-2\"></div>"
                            "\n\t\t\t<div class=\"col-md-7\">{% block button %}{% endblock %}</div>"
                            "\n\t\t\t<div class=\"col-md-3\">{% block input %}{% endblock %}</div>"
                            "\n\t\t</div>"
                            "\n\t</div>"
                            "\n<footer class=\"footer\">"
                            "\n\t{% block footer %}"
                            "\n\t\t<div class=\"container-fluid\">"
                            "\n\t\t<div class=\"row justify-content-md-center\">"
                            "\n\t\t\t<div class=\"col-md-3\">"
                            "\n\t\t\t\t<div>copyright © INRAE </div>"
                            "\n\t\t\t</div>"
                            "\n\t\t\t<div class=\"col-md-6\">"
                            "\n\t\t\t\t<a href=\"https://www.inrae.fr/\" >"
                            "\n\t\t\t\t<img class=\"footerimage\" id=\"inrae\" src=\"{% static 'images/logo-inrae_inra_logo.png'%}\">"
                            "\n\t\t\t\t</a>"
                            "\n\t\t\t\t<a href=\"https://www.oniris-nantes.fr/\" >"
                            "\n\t\t\t\t<img class=\"footerimage\" id=\"oniris\" src=\"{% static 'images/Oniris_inra_logo.png'%}\">"
                            "\n\t\t\t\t</a>"
                            "\n\t\t\t</div>"
                            "\n\t\t\t<div class=\"col-md-3\">Legal notice</div>"
                            "\n\t\t</div>"
                            "\n\t</div>"
                            "\n\t{% endblock footer %}"
                            "\n</footer>"
                            "\n</body>"
                            "\n{% block js %}{% endblock %}"
                            "\n</html>")

            ## Create the statistic result file
            result_file = open(BASE_DIR + "/" + app_name + "/templates/" + "result.html", "w+")
            result_file.write("{% extends \"base.html\" %}"
                              "\n{% block content %}"
                              "\n<h2>Simulation results from EMULSION coming soon... </h2>"
                              "\n<div> <a href=\"{% url 'form' %}\">"
                              "Back to the form</a> </div>"
                              "\n<div style=\"text-align:center;\" class=\"fa-10x\">"
                              "<i class=\"fas fa-chart-bar\"></i></div>"
                              "\n{% endblock content %}")

            shiny_result_file = open(BASE_DIR + "/" + app_name + "/templates/" + "shiny.html", "w+")
            shiny_result_file.write("<h1>Statistics</h1>\n"
                                    "<div> <a href=\"{% url 'form' %}\">Back to the form</a> </div>\n")

            shiny_result_file.write("\n<iframe src=\"{}\" style=\"top:0; left:0; bottom:0; right:0; width:100%; height:100%; border:none; margin:0; padding:0; overflow:hidden; z-index:999999;\">"
                                    "\n\tYour browser doesn't support iframes"
                                    "\n</iframe>".format(calcul_uri))

            # print("\nModifying projects url file and include application urls")
            project_urls = open(BASE_DIR + "/" + os.path.basename(BASE_DIR) + "/urls.py", "w+")

            project_urls.write("from django.contrib import admin"
                               "\nfrom django.urls import path, include, re_path"
                               "\nfrom django.http import HttpResponseRedirect")

            project_urls.write("\nurlpatterns = ["
                               "\n\tpath('admin/', admin.site.urls),"
                               "\n\tpath('', include('{}.urls')),"
                               "\n\tre_path(r'^$', lambda r: HttpResponseRedirect('form/')),"
                               "\n]".format(app_name))

            print("\nMaking django migrations to take account all our changes")
            management.call_command('makemigrations', app_name)
            management.call_command('migrate', app_name)
            management.call_command('makemigrations', interactive=True)
            management.call_command('migrate', interactive=True)

            commands_import = []
            for key, value in cmd_import_data_files.items():
                commands_import.append("--" + key + "=" + value)
            import_data_arguments = ' '.join(commands_import)

            print("\n")
            print("\nmanage.py import_data_file {} ".format(import_data_arguments))

            ##### Creating R shiny app
            shinyapp_path = BASE_DIR + "/shinyapp"
            try:
                os.mkdir(shinyapp_path)
            except OSError:
                print("Creation of the directory %s failed" % shinyapp_path)
            else:
                print("Successfully created the directory %s " % shinyapp_path)

            shinyapp_file = open(BASE_DIR + "/shinyapp/" + "app.R", "w+")

            shinyapp_file.write("library(dplyr)\nlibrary(ggplot2)\nlibrary(shiny)\nlibrary(shinythemes) \nlibrary(shinyjs)"
                                "\nlibrary(shinyBS)\nlibrary(DT)"
                                "\nlibrary(tidyr)\n"
                                '\ntheme_set(theme_bw() + theme(text=element_text(size={}), legend.position="bottom", legend.direction = "horizontal", legend.box = "vertical"))'
                                "\nDJANGO_PROJECT_DIR <- dirname(getwd())"
                                "\nsimulation_name <- \"test\"".format(figure_text_size))

            ### Add simulation_name in shiny app "app.R"
            shinyapp_file.write("\n\nui <- navbarPage(\"Statistics\", theme = shinytheme(\"cerulean\"),")
            outputs_pages = documents["outputs_pages"]
            dict_graphics = {}
            dict_ggplot = {}
            for page in outputs_pages:
                dict_graphics[page]= []
            graphics = documents["graphics"]
            plot_annotations = {}
            plot_variables = {}
            plot_type = {}
            plot_options = {}
            plot_variables_str = {}
            plot_annotations_str = {}
            plot_type_str = {}
            plot_options_str = {}
            scenario_command_str = []
            graphics_data_name = []
            dict_label = {}
            dict_ggplot_plot_annotations = {}
            label_scenario_comp = ""
            join_list_scenario_command_str = ""

            for graph_key, graph_value in graphics.items():
                if any("scenario" in d for d in graph_value) is True: ## for each scenario # if scenario is registered

                    for graph_item_str in graph_value:

                        for graph_item_str_key, graph_item_str_value in graph_item_str.items():

                            if graph_item_str_key == "data":
                                data_command_str = graph_item_str_value
                                if data_command_str not in graphics_data_name:
                                    graphics_data_name.append(data_command_str)

                            if graph_item_str_key == "on_page":
                                if set(graph_item_str_value).issubset(outputs_pages) is True:

                                    command_pages_str = graph_item_str_value

                            if graph_item_str_key == "single_graph":
                                if graph_item_str_value == "no":
                                    single_graph_value_str = "no"

                                if graph_item_str_value == "yes":
                                    single_graph_value_str = "yes"

                            if graph_item_str_key == "label":
                                label_list = graph_item_str_value



                            if graph_item_str_key == "plot_variables":
                                plot_variables_str = graph_item_str_value

                            if graph_item_str_key == "plot_options":
                                plot_options_str = graph_item_str_value

                            if graph_item_str_key == "plot_type":
                                plot_type_str = graph_item_str_value

                            if graph_item_str_key == "scenario":
                                scenario_command_str = [data_command_str + "_" + item for item in graph_item_str_value]

                                join_list_scenario_command_str = '_'.join(item for item in scenario_command_str)
                                i = 0
                                if single_graph_value_str == "no":
                                    for command_pages_item in command_pages_str:
                                        dict_graphics[command_pages_item].append(scenario_command_str[i])
                                        dict_label[scenario_command_str[i]] = label_list[i]
                                        i = i + 1

                                if single_graph_value_str == "yes":
                                    for command_pages_item in command_pages_str:
                                        join_list_scenario_command_str = '_'.join(item for item in scenario_command_str)
                                        dict_graphics[command_pages_item].append(scenario_command_str)
                                        #tuple_scenario_command_str = tuple(scenario_command_str)
                                        dict_label[join_list_scenario_command_str] = label_list

                            if graph_item_str_key == "plot_annotations":
                                plot_annotations_str = graph_item_str_value

                                dict_ggplot_plot_annotations[join_list_scenario_command_str] = plot_annotations_str

                        print(scenario_command_str)
                        for scenario_command_item in scenario_command_str:
                                dict_ggplot[scenario_command_item] = {"plot_variables": plot_variables_str,
                                                                    "plot_type": plot_type_str,
                                                                    "plot_options": plot_options_str,
                                                                    }

                elif any("scenario" in d for d in graph_value) is False: ## for scenario comparison # if scenario is not registered

                    for graph_item in graph_value:
                        for graph_item_key, graph_item_value in graph_item.items():
                            if graph_item_key == "data":

                                data_command = graph_item_value
                                if data_command not in graphics_data_name:
                                    graphics_data_name.append(data_command)

                                if type(data_command) is str and data_command in command_scenario_comparison_list:
                                    data_command = graph_item_value + "_scenario_comparison"

                                if type(data_command) is list and set(data_command).issubset(set(command_scenario_comparison_list)):
                                    data_command = [item + "_scenario_comparison" for item in data_command]

                            if graph_item_key == "on_page":
                                if set(graph_item_value).issubset(outputs_pages) is True:
                                    command_pages = graph_item_value

                            if graph_item_key == "single_graph":
                                if graph_item_value == "no":
                                    single_graph_value = "no"

                                if graph_item_value == "yes":
                                    single_graph_value = "yes"

                            if graph_item_key == "plot_annotations":
                                plot_annotations = graph_item_value


                            if graph_item_key == "plot_variables":
                                plot_variables = graph_item_value

                            if graph_item_key == "plot_options":
                                plot_options = graph_item_value

                            if graph_item_key == "plot_type":
                                plot_type = graph_item_value

                            if graph_item_key == "label":
                                label_scenario_comp = ' '.join(graph_item_value)

                    dict_label[data_command] = label_scenario_comp

                    for command_pages_item in command_pages:
                        dict_graphics[command_pages_item].append(data_command)

                        if type(data_command) is list:
                            for data_command_item in data_command:
                                dict_ggplot[data_command_item] = {"plot_variables": plot_variables, "plot_type": plot_type, "plot_options": plot_options, "plot_annotations": plot_annotations}
                        elif type(data_command) is str:
                            dict_ggplot[data_command] = {"plot_variables": plot_variables, "plot_type": plot_type,
                                                              "plot_annotations": plot_annotations, "plot_options": plot_options}

                        for command_pages_item in command_pages_str:
                            scenario_command= command_pages_item + "_" + data_command

            graphics_keys_list = []
            for dict_graphics_key, dict_graphics_value in dict_graphics.items():
                shinyapp_file.write("\n\n\ttabPanel(\"{}\",".format(dict_graphics_key))
                shinyapp_file.write("\n\t\tnavlistPanel(id = \"TabsetPanel_{}\", ".format(dict_graphics_key))

                for graphics_value_item in dict_graphics_value:
                    if graphics_value_item == dict_graphics_value[-1]:

                        if type(graphics_value_item) is str:
                            new_key = dict_graphics_key + "_" + graphics_value_item

                            shinyapp_file.write(
                                "\n\t\t\ttabPanel(\"{}\",plotOutput(\"{}\"))".format(dict_label[graphics_value_item], new_key))

                            graphics_keys_list.append(new_key)

                        if type(graphics_value_item) is list:
                            join_list = '_'.join(item for item in graphics_value_item)
                            new_key_list = dict_graphics_key + "_" + join_list
                            shinyapp_file.write(
                                "\n\t\t\ttabPanel(\"{}\",plotOutput(\"{}\"))".format(' '.join(dict_label[join_list]), new_key_list))
                            graphics_keys_list.append(new_key_list)
                    else:
                        if type(graphics_value_item) is str:

                            new_key = dict_graphics_key + "_" + graphics_value_item
                            shinyapp_file.write("\n\t\t\ttabPanel(\"{}\",plotOutput(\"{}\")),".format(dict_label[graphics_value_item], new_key))
                            graphics_keys_list.append(new_key)

                        if type(graphics_value_item) is list:
                            join_list = '_'.join(item for item in graphics_value_item)
                            new_key_list = dict_graphics_key + "_" + join_list

                            shinyapp_file.write("\n\t\t\ttabPanel(\"{}\",plotOutput(\"{}\")),".format(' '.join(dict_label[join_list]),new_key_list))
                            graphics_keys_list.append(new_key_list)

                if dict_graphics_key == list(dict_graphics.keys())[-1]:
                    shinyapp_file.write("\n\t))")
                else:
                    shinyapp_file.write("\n\t)),")
            shinyapp_file.write("\n)")

            shinyapp_file.write("\n\nserver <- function(input, output)\n{")
            for scenario_name in list_scenarios_name:
                shinyapp_file.write("\n\n\tif (file.exists(paste(DJANGO_PROJECT_DIR,\"/{}/Rscripts/output_Rdata_\", simulation_name, \"/{}.Rdata\", sep=\"\")))".format(app_name, scenario_name))
                shinyapp_file.write("\n\t\tload(paste(DJANGO_PROJECT_DIR,\"/{}/Rscripts/output_Rdata_\", simulation_name, \"/{}.Rdata\", sep=\"\"))".format(app_name, scenario_name))
            shinyapp_file.write(
                "\n\n\tif (file.exists(paste(DJANGO_PROJECT_DIR,\"/{}/Rscripts/output_Rdata_\", simulation_name, \"/scenarios_comparison.Rdata\", sep=\"\")))".format(app_name))

            shinyapp_file.write("\n\t\tload(paste(DJANGO_PROJECT_DIR,\"/{}/Rscripts/output_Rdata_\", simulation_name, \"/scenarios_comparison.Rdata\", sep=\"\"))".format(app_name))

            shinyapp_file.write("\n\n\tis_exists_data <- function(data) {"
                                "\n\t\tif (exists(data) ){"
                                "\n\t\t\tSwitch <- T"
                                "\n\t\t}else{"
                                "\n\t\t\tSwitch <- F"
                                "\n\t\t}"
                                "\n\t}")


            ## Creating dictionnary listing the aes mandatory and optional of each ggplot geoms
            plot_type_aes_dict = {"line": { "plot_variables_mandatory": ["x", "y"], "plot_variables_optional": ["alpha", "group", "linetype", "linewidth", "colour","size"]},
                                  "point": { "plot_variables_mandatory": ["x", "y"], "plot_variables_optional": ["alpha", "group", "size", "fill", "colour", "shape"]},
                                  "boxplot":{ "plot_variables_mandatory": ["x","y"], "plot_variables_optional": ["lower", "upper", "middle","x", "y", "xmin", "xmax", "ymin", "ymax", "alpha", "fill", "group", "linetype", "colour", "shape", "size", "weight"]},
                                  "ribbon": { "plot_variables_mandatory": ["x", "ymin", "ymax"], "plot_variables_optional": ["alpha", "colour", "fill", "group", "linetype","linewidth"]},
                                  "area": { "plot_variables_mandatory": ["x", "y"], "plot_variables_optional": ["alpha", "colour", "fill", "group", "linetype","size"]},
                                  "bar": { "plot_variables_mandatory": ["x","y"], "plot_variables_optional": ["y", "alpha", "group", "colour", "fill", "linetype", "size"]},
                                  "col": { "plot_variables_mandatory": ["x", "y"], "plot_variables_optional": ["alpha", "group", "colour", "fill", "linetype", "size"]},
                                  "histogram": { "plot_variables_mandatory": ["x", "y"], "plot_variables_optional": ["alpha", "colour", "group", "fill", "linetype", "size"]},
                                  "smooth":  { "plot_variables_mandatory": ["x", "y"], "plot_variables_optional": ["alpha", "colour", "group", "fill", "linetype", "size", "weight", "ymax", "ymin"]}
                                  }
            ## Reading the paste.yaml file to create the app.R for graphics using shiny
            for dict_graphics_key, dict_graphics_value in dict_graphics.items():
                for graphics_value_item in dict_graphics_value:
                    if type(graphics_value_item) is str: ## if the graphs contains only one data, then it an str
                        ## create the name of the graphics
                        counter_str = 0
                        new_key = dict_graphics_key + "_" + graphics_value_item
                        shinyapp_file.write("\n\n\toutput${} <- renderPlot({{".format(new_key))
                        shinyapp_file.write("\n\t\t\tggplot({}, ".format(graphics_value_item))

                        verify_mandatory_variables_str=[] ## this list contains the mandatory aes according to each type of plot
                        current_dataframes_values_str=[] ## This list contains the dataframes values of the currunt plot type read

                        if dict_ggplot[graphics_value_item]["plot_variables"]: ## aes in the ggplot, verifying if they exist in the dataframes already before adding it
                            shinyapp_file.write("aes(") ## if plot_variables is added, then add aes in the graph command
                            for plot_variables_key_str, plot_variables_value_str in dict_ggplot[graphics_value_item]["plot_variables"].items(): ## loop over the plot_variables
                                for command_key_str, command_value_str in dataframe_command_variables.items(): ## loop over the dataframes commands variables. This dataframe has been initiated while reading the commands above (while creating the commands in R)
                                    x = graphics_value_item.replace(command_key_str, "")
                                    new_command_key_str = command_key_str + x

                                    if new_command_key_str == graphics_value_item: ## command_key correspond to the name of the command in the Rscripts
                                        current_dataframes_values_str = command_value_str ## retrieve the command values for the current R command used in this plot
                                        verify_mandatory_variables_str.append(plot_variables_key_str) ## create a list of plot_variables. It will help us verify if all mandatory variables are entered by the user
                                        if plot_variables_value_str in command_value_str: ## if the value read in the plot_variables are in the commands values of the R command the add it to the plot, else print a warning
                                            if plot_variables_key_str == list(dict_ggplot[graphics_value_item]["plot_variables"].keys())[-1]:
                                                shinyapp_file.write("{}={})".format(plot_variables_key_str,plot_variables_value_str))
                                            else:
                                                shinyapp_file.write("{}={}, ".format(plot_variables_key_str,plot_variables_value_str))
                                        else:
                                            warnings.warn('This is a default warning.')
                        shinyapp_file.write(")")

                        ## Plot_type of graphics, verify if it is informed for this plot
                        if dict_ggplot[graphics_value_item]["plot_type"]:
                            for plot_type_item in dict_ggplot[graphics_value_item]["plot_type"]:
                                verify_mandatory_variables_temp = []
                                if type(plot_type_item) is dict: ## the plot_type can be informed as a dictionnary or a str
                                    for plot_type_key, plot_type_value in plot_type_item.items(): ## plot_type_value contains the plot_type informations
                                        shinyapp_file.write("+ \n\t\t\tgeom_{}(".format(plot_type_key))

                                        if "plot_variables" in plot_type_value.keys(): ### if plot_variables is informed
                                            shinyapp_file.write("aes(") ## write aes

                                            for item, value in plot_type_value["plot_variables"].items(): ## loop over the plot_variables dictionnary
                                                if "plot_variables_mandatory" in plot_type_aes_dict[plot_type_key].keys() or "plot_variables_optional" in plot_type_aes_dict[plot_type_key].keys(): ## verify that plot_type aes dictionnary contains plot_variables_mandatory and plot_variables_optional keys
                                                    if item in plot_type_aes_dict[plot_type_key]["plot_variables_mandatory"] or item in plot_type_aes_dict[plot_type_key]["plot_variables_optional"]: ## verify if plot_variables item are in the plot_type aes dictionnary, if so write it in the graph command
                                                        if item in plot_type_aes_dict[plot_type_key]["plot_variables_mandatory"]: # Add mandatory variables from plot_type plot_variables also
                                                            if item not in verify_mandatory_variables_str:
                                                                verify_mandatory_variables_temp.append(item)

                                                        if len(list(plot_type_value["plot_variables"].keys())) > 1: ## If len of plot_type dict is higher than 1
                                                            if item == list(plot_type_value["plot_variables"].keys())[-1]: ## if it is the last item

                                                                if value in current_dataframes_values_str: ## if the value of the item is a dataframes value, then don't add quotes
                                                                    shinyapp_file.write(",{}={}".format(item,value))

                                                                elif value == "scenario_name":
                                                                    shinyapp_file.write(",{}=scenario_name".format(item))
                                                                    counter_str = counter_str + 1

                                                                else:
                                                                    print('\nWarning:')
                                                                    print("The plot_variables item {} and its value {} is not a value of {} dataframes.".format(
                                                                            item, value, graphics_value_item))

                                                                    shinyapp_file.write(",{}=\"{}\"".format(item, value)) ## else add quotes

                                                            elif item == list(plot_type_value["plot_variables"].keys())[0]: # if it is the first item
                                                                if value in current_dataframes_values_str:
                                                                    shinyapp_file.write("{}={}".format(item,value))
                                                                elif value == "scenario_name":
                                                                    shinyapp_file.write("{}=scenario_name".format(item))
                                                                    counter_str = counter_str + 1
                                                                else:
                                                                    print('\nWarning:')
                                                                    print("The plot_variables item {} and its value {} is not a value of {} dataframes.".format(item, value, graphics_value_item))
                                                                    shinyapp_file.write("{}=\"{}\"".format(item, value))

                                                            else: ## if it is an item intermediate item
                                                                if value in current_dataframes_values_str:
                                                                    shinyapp_file.write(",{}={} ".format(item,value))
                                                                elif value == "scenario_name":
                                                                    shinyapp_file.write(",{}=scenario_name".format(item))
                                                                    counter_str = counter_str + 1
                                                                else:
                                                                    print('\nWarning:')
                                                                    print("The plot_variables item {} and its value {} is not a value of {} dataframes.".format(
                                                                            item, value, graphics_value_item))

                                                                    shinyapp_file.write(", {}=\"{}\" ".format(item, value))

                                                        elif len(list(plot_type_value["plot_variables"].keys())) == 1: ## if there is only one item in the plot_type dict

                                                            if value in current_dataframes_values_str:
                                                                shinyapp_file.write("{}={}".format(item, value))
                                                            elif value == "scenario_name":
                                                                shinyapp_file.write("{}=scenario_name".format(item))
                                                                counter_str = counter_str + 1
                                                            else:
                                                                print('\nWarning:')
                                                                print("The plot_variables item {} and its value {} is not a value of {} dataframes.".format(
                                                                        item, value, graphics_value_item))

                                                                shinyapp_file.write("{}=\"{}\"".format(item, value))

                                        ## for parenthesis there are two conditions
                                        if set(('plot_options', 'plot_variables')).issubset(plot_type_value): ## Add only one ")" if the are 'plot_options' and 'plot_variables' in the plot_type dict
                                            shinyapp_file.write(")")

                                        if len(list(plot_type_value.keys())) == 1 and "plot_variables" in plot_type_value: ## if only plot_variables add only one ")"
                                            shinyapp_file.write("))")

                                        if "plot_options" in plot_type_value.keys(): ## if plot_options is informed in the plot_type dict
                                            if set(('plot_options', 'plot_variables')).issubset(plot_type_value): ## add a "," if the plot_type item contains plot_variables and plot_options
                                                shinyapp_file.write(", ")

                                            for item, value in plot_type_value["plot_options"].items():  # loop over plot_type optional items

                                                if "plot_variables_optional" in plot_type_aes_dict[plot_type_key].keys():

                                                    try:
                                                        float(value)
                                                        if item in plot_type_aes_dict[plot_type_key]["plot_variables_optional"]: ## if item in the plot_type aes dict
                                                            if item == list(plot_type_value["plot_options"].keys())[-1]:
                                                                shinyapp_file.write("{}={}".format(item, value))

                                                            else:
                                                                shinyapp_file.write("{}={}, ".format(item, value))

                                                    except ValueError:
                                                        if item in plot_type_aes_dict[plot_type_key]["plot_variables_optional"]:
                                                            if item == list(plot_type_value["plot_options"].keys())[-1]:
                                                                shinyapp_file.write("{}=\"{}\"".format(item, value))

                                                            else:
                                                                shinyapp_file.write("{}=\"{}\", ".format(item, value))

                                            shinyapp_file.write(")")

                                if type(plot_type_item) is str: ## if plot type item does not have aes
                                    if plot_type_item in plot_type_aes_dict.keys():
                                        shinyapp_file.write("+ \n\t\t\tgeom_{}()".format(plot_type_item))

                            ## write labs of the graph
                            if "title" in dict_ggplot[graphics_value_item]["plot_annotations"].keys():
                                shinyapp_file.write(" + \n\t\t\tlabs(title= \"{}\"".format(dict_ggplot[graphics_value_item]["plot_annotations"]["title"]))

                            if "x_title" in dict_ggplot[graphics_value_item]["plot_annotations"].keys():
                                shinyapp_file.write(", x = \"{}\"".format(dict_ggplot[graphics_value_item]["plot_annotations"]["x_title"]))

                            if "y_title" in dict_ggplot[graphics_value_item]["plot_annotations"].keys():
                                shinyapp_file.write(", y = \"{}\"".format(dict_ggplot[graphics_value_item]["plot_annotations"]["y_title"]))

                            shinyapp_file.write(")\n\t\t})")

                            ## create observe function from shinyjs to update the view of the graph according to the data
                            shinyapp_file.write("\n\n\tobserve({")
                            shinyapp_file.write("\n\t\tif (exists('{}')){{".format(graphics_value_item))
                            shinyapp_file.write("\n\t\t\tshow(\"{}\")".format(new_key))
                            shinyapp_file.write("\n\t\t\tobserveEvent(input$TabsetPanel_{}, {{".format(dict_graphics_key))
                            shinyapp_file.write("\n\t\t\t\tshowTab(inputId = \"TabsetPanel_{}\", target = \"{}\")\n\t\t\t}})".format(dict_graphics_key, graphics_value_item))
                            shinyapp_file.write("\n\t\t} else {")
                            shinyapp_file.write("\n\t\t\thide(\"{}\")".format(new_key))
                            shinyapp_file.write(
                                "\n\t\t\tobserveEvent(input$TabsetPanel_{}, {{".format(dict_graphics_key))
                            shinyapp_file.write(
                                "\n\t\t\t\thideTab(inputId = \"TabsetPanel_{}\", target = \"{}\")\n\t\t\t}})".format(
                                    dict_graphics_key, graphics_value_item))
                            shinyapp_file.write("\n\t\t}\n\t})")


                    if type(graphics_value_item) is list: ## if graph contains multiple data
                        join_list = '_'.join(item for item in graphics_value_item)

                        new_key_list = dict_graphics_key + "_" + join_list  ## create the name of the command
                        shinyapp_file.write("\n\n\toutput${} <- renderPlot({{".format(new_key_list))
                        shinyapp_file.write("\n\t\t\tggplot(")

                        plot_variables_already_written = []
                        aes_already_written = []
                        for graphics_value_item_item in graphics_value_item: ## Create the command with the aes, plot_type, plot_options
                            verify_mandatory_variables_bis = []
                            current_dataframes_values = []

                            if dict_ggplot[graphics_value_item_item]["plot_variables"]:  ## aes in the ggplot, verifying if they exist in the dataframes already before adding it

                                if "aes" not in aes_already_written:
                                    aes_already_written.append("aes")
                                    shinyapp_file.write("data=NULL, aes(")
                                else:
                                    pass

                                for plot_variables_key, plot_variables_value in dict_ggplot[graphics_value_item_item]["plot_variables"].items():
                                    for command_key, command_value in dataframe_command_variables.items(): ## dataframe of values in statistics
                                        x = graphics_value_item_item.replace(command_key, "")
                                        new_command_key = command_key + x

                                        if new_command_key == graphics_value_item_item:

                                            current_dataframes_values = command_value
                                            if plot_variables_key not in verify_mandatory_variables_bis:
                                                verify_mandatory_variables_bis.append(plot_variables_key)  ## create a list of plot_variables. It will help us verify if all mandatory variables are entered by the user.
                                            if plot_variables_value in command_value:
                                                tuples_to_add = "{}={}".format(plot_variables_key, plot_variables_value)

                                                if tuples_to_add not in plot_variables_already_written:
                                                    plot_variables_already_written.append(tuples_to_add)
                                                    if plot_variables_key == list(dict_ggplot[graphics_value_item_item]["plot_variables"].keys())[-1]:
                                                        shinyapp_file.write("{}={})) ".format(plot_variables_key, plot_variables_value))
                                                    else:
                                                        shinyapp_file.write("{}={}, ".format(plot_variables_key, plot_variables_value))
                                                else:
                                                    pass
                                            else:
                                                warnings.warn('This is a default warning.')

                            if dict_ggplot[graphics_value_item_item]["plot_type"]:

                                if type(dict_ggplot[graphics_value_item_item]["plot_type"]) is list:
                                    verify_mandatory_variables_bis_temp = []

                                    for plot_type_item in dict_ggplot[graphics_value_item_item]["plot_type"]:

                                        if type(plot_type_item) is dict:
                                            for plot_type_item_key, plot_type_item_value in plot_type_item.items():
                                                shinyapp_file.write("+ \n\t\t\t{{if(is_exists_data('{}'))geom_{}(data={}, ".format(graphics_value_item_item, plot_type_item_key,graphics_value_item_item))

                                                if "plot_variables" in plot_type_item_value.keys():
                                                    shinyapp_file.write("aes(")

                                                    for item, value in plot_type_item_value["plot_variables"].items():
                                                        if "plot_variables_mandatory" in plot_type_aes_dict[plot_type_item_key].keys() or "plot_variables_optional" in plot_type_aes_dict[plot_type_item_key].keys():
                                                            if item in plot_type_aes_dict[plot_type_item_key]["plot_variables_mandatory"] or item in plot_type_aes_dict[plot_type_item_key]["plot_variables_optional"]:

                                                                if item in plot_type_aes_dict[plot_type_item_key]["plot_variables_mandatory"]:
                                                                    if item not in verify_mandatory_variables_bis:
                                                                        verify_mandatory_variables_bis_temp.append(item)

                                                                if len(list(plot_type_item_value["plot_variables"].keys())) > 1:
                                                                    if item == list(plot_type_item_value["plot_variables"].keys())[-1]:

                                                                        if value == "data":
                                                                            shinyapp_file.write("{}=\"{}\"".format(item, command_desc[graphics_value_item_item]))

                                                                        elif value == "scenario_name":  ## keyword used when you have multiple plot in one graph and you want to specify that each graph correspond to a scenario
                                                                            ## single_graph == yes will permit to refer to the scenario and add a label scenario name for each graph
                                                                            shinyapp_file.write(",{}=".format(item))

                                                                            for scenarios_name in list_scenarios_name:
                                                                                if scenarios_name in graphics_value_item_item:
                                                                                    new_scenario_name = "_" + scenarios_name
                                                                                    vari = graphics_value_item_item.replace(
                                                                                        new_scenario_name, "")
                                                                                    if vari in graphics_data_name:
                                                                                        shinyapp_file.write(
                                                                                            "\"{}\"".format(
                                                                                                scenarios_name))

                                                                        else:
                                                                            if value in current_dataframes_values:
                                                                                shinyapp_file.write(",{}={}".format(item, value))
                                                                            else:

                                                                                shinyapp_file.write(
                                                                                        ",{}=\"{}\"".format(item,
                                                                                                            value))

                                                                    elif item == list(plot_type_item_value["plot_variables"].keys())[0]:
                                                                        if value == "data":
                                                                            shinyapp_file.write("{}=\"{}\"".format(item, command_desc[graphics_value_item_item]))

                                                                        elif value == "scenario_name":  ## keyword used when you have multiple plot in one graph and you want to specify that each graph correspond to a scenario
                                                                            ## single_graph == yes will permit to refer to the scenario and add a label scenario name for each graph
                                                                            shinyapp_file.write("{}=".format(item))

                                                                            for scenarios_name in list_scenarios_name:
                                                                                if scenarios_name in graphics_value_item_item:
                                                                                    new_scenario_name = "_" + scenarios_name
                                                                                    vari = graphics_value_item_item.replace(
                                                                                        new_scenario_name, "")
                                                                                    if vari in graphics_data_name:
                                                                                        shinyapp_file.write("\"{}\"".format(scenarios_name))

                                                                        else:
                                                                            if value in current_dataframes_values:
                                                                                shinyapp_file.write("{}={}".format(item, value))
                                                                            else:
                                                                                shinyapp_file.write("{}=\"{}\"".format(item, value))
                                                                    else:

                                                                        if value == "data":
                                                                            shinyapp_file.write("{}=\"{}\"".format(item, command_desc[graphics_value_item_item]))

                                                                        elif value == "scenario_name":  ## keyword used when you have multiple plot in one graph and you want to specify that each graph correspond to a scenario
                                                                            ## single_graph == yes will permit to refer to the scenario and add a label scenario name for each graph
                                                                            shinyapp_file.write("{}=".format(item))

                                                                            for scenarios_name in list_scenarios_name:
                                                                                if scenarios_name in graphics_value_item_item:
                                                                                    new_scenario_name = "_" + scenarios_name
                                                                                    vari = graphics_value_item_item.replace(
                                                                                        new_scenario_name, "")
                                                                                    if vari in graphics_data_name:
                                                                                        shinyapp_file.write(
                                                                                            "\"{}\"".format(
                                                                                                scenarios_name))

                                                                        else:
                                                                            if value in current_dataframes_values:
                                                                                shinyapp_file.write(",{}={} ".format(item, value))
                                                                            else:
                                                                                shinyapp_file.write(", {}=\"{}\" ".format(item, value))

                                                                elif len(list(plot_type_item_value["plot_variables"].keys())) == 1:
                                                                    if value == "data":
                                                                        shinyapp_file.write(",{}=\"{}\"".format(item,command_desc[graphics_value_item_item]))

                                                                    elif value == "scenario_name":  ## keyword used when you have multiple plot in one graph and you want to specify that each graph correspond to a scenario
                                                                        ## single_graph == yes will permit to refer to the scenario and add a label scenario name for each graph
                                                                        shinyapp_file.write("{}=".format(item))

                                                                        for scenarios_name in list_scenarios_name:
                                                                            if scenarios_name in graphics_value_item_item:
                                                                                new_scenario_name = "_" + scenarios_name
                                                                                vari = graphics_value_item_item.replace(
                                                                                    new_scenario_name, "")
                                                                                if vari in graphics_data_name:
                                                                                    shinyapp_file.write(
                                                                                        "\"{}\"".format(scenarios_name))

                                                                    else:
                                                                        if value in current_dataframes_values:
                                                                            shinyapp_file.write("{}={}".format(item, value))
                                                                        else:
                                                                            shinyapp_file.write("{}=\"{}\"".format(item, value))

                                                ### close plot_variables
                                                if set(('plot_options', 'plot_variables')).issubset(plot_type_item_value):
                                                    shinyapp_file.write(")")

                                                if len(list(plot_type_item_value.keys())) == 1 and "plot_variables" in plot_type_item_value:
                                                    shinyapp_file.write("))}")

                                                if "plot_options" in plot_type_item_value.keys():
                                                    if set(('plot_options', 'plot_variables')).issubset(plot_type_item_value):
                                                        shinyapp_file.write(", ")

                                                    for item, value in plot_type_item_value["plot_options"].items():
                                                        if "plot_variables_optional" in plot_type_aes_dict[plot_type_item_key].keys():

                                                            try:
                                                                float(value)
                                                                if item in plot_type_aes_dict[plot_type_item_key][
                                                                    "plot_variables_optional"] and item == \
                                                                        list(plot_type_item_value["plot_options"].keys())[
                                                                            -1]:
                                                                    shinyapp_file.write("{}={}".format(item, value))
                                                                else:
                                                                    shinyapp_file.write(
                                                                        "{}={}, ".format(item, value))

                                                            except ValueError:
                                                                if item in plot_type_aes_dict[plot_type_item_key][
                                                                    "plot_variables_optional"] and item == \
                                                                        list(plot_type_item_value["plot_options"].keys())[
                                                                            -1]:
                                                                    shinyapp_file.write("{}=\"{}\"".format(item, value))

                                                                else:
                                                                    shinyapp_file.write("{}=\"{}\", ".format(item, value))
                                                    ### close plot_options
                                                    shinyapp_file.write(")}")

                                        if type(plot_type_item) is str:
                                            if plot_type_item in plot_type_aes_dict.keys():
                                                shinyapp_file.write("+ \n\t\t\t{{if(is_exists_data('{}'))(geom_{}(data={}))}}".format(graphics_value_item_item,plot_type_item,graphics_value_item_item))


# if type(graphics_value_item) is list:
#
# (dict_label[join_list]), new_key_list))
#
# if type(graphics_value_item) is str:
#
#     ndict_label[graphics_value_item]


                        print(dict_ggplot_plot_annotations[join_list])
                        if "title" in dict_ggplot_plot_annotations[join_list].keys():
                            shinyapp_file.write(" + \n\t\t\tlabs(title= \"{}\"".format(
                                dict_ggplot_plot_annotations[join_list]["title"]))

                        if "x_title" in dict_ggplot_plot_annotations[join_list].keys():
                            shinyapp_file.write(", x = \"{}\"".format(
                                dict_ggplot_plot_annotations[join_list]["x_title"]))

                        if "y_title" in dict_ggplot_plot_annotations[join_list].keys():
                            shinyapp_file.write(", y = \"{}\"".format(
                                dict_ggplot_plot_annotations[join_list]["y_title"]))

                        shinyapp_file.write(")\n\t\t})")

                        ## create observe function to hide/show plot according to choosen scenarios when running emulsion
                        shinyapp_file.write("\n\n\tobserve({")
                        shinyapp_file.write("\n\t\tif (")
                        for item in graphics_value_item:
                            if item == graphics_value_item[-1]:
                                shinyapp_file.write(" exists('{}') ".format(item))
                            else:
                                shinyapp_file.write(" exists('{}') ||".format(item))
                        shinyapp_file.write("){")

                        shinyapp_file.write("\n\t\t\tshow(\"{}\")".format(new_key_list))
                        shinyapp_file.write("\n\t\t\tobserveEvent(input$TabsetPanel_{}, {{".format(dict_graphics_key))
                        shinyapp_file.write(
                            "\n\t\t\t\tshowTab(inputId = \"TabsetPanel_{}\", target = \"{}\")\n\t\t\t}})".format(
                                dict_graphics_key, join_list))
                        shinyapp_file.write("\n\t\t} else {")
                        shinyapp_file.write("\n\t\t\thide(\"{}\")".format(new_key_list))
                        shinyapp_file.write(
                            "\n\t\t\tobserveEvent(input$TabsetPanel_{}, {{".format(dict_graphics_key))
                        shinyapp_file.write(
                            "\n\t\t\t\thideTab(inputId = \"TabsetPanel_{}\", target = \"{}\")\n\t\t\t}})".format(
                                dict_graphics_key, join_list))
                        shinyapp_file.write("\n\t\t}\n\t})")

            shinyapp_file.write("\n}\n# Run the application\nshinyApp(ui = ui, server = server)")

            # f.close()
            # views_file.close()
            # model_file.close()
            # celery_task_file.close()
            # url_file.close()
