from django.core.management.base import BaseCommand, CommandError
import csv
import os
import sys
import pandas as pd
import numpy as np
from vaccination_app.models import *
class Command(BaseCommand):
	help = 'Import data from csv file to database'

	def add_arguments(self, parser):
		parser.add_argument('--filename_risk_level_breeder', type=str)
		parser.add_argument('--filename_vaccine_characteristics', type=str)

	def handle(self, *args, **options):


		filename_vaccine_characteristics= options['filename_vaccine_characteristics']
		data_vaccine_characteristics = pd.read_csv(filename_vaccine_characteristics, sep=',')
		vaccine_characteristics.objects.all().delete()
		for index_vaccine_characteristics, row_vaccine_characteristics in data_vaccine_characteristics.iterrows():
			vaccine_characteristics_model = vaccine_characteristics()
			vaccine_characteristics_model.set_name(row_vaccine_characteristics['name'])
			vaccine_characteristics_model.set_delay_vacc(row_vaccine_characteristics['delay_vacc'])
			vaccine_characteristics_model.set_dur_vacc(row_vaccine_characteristics['dur_vacc'])
			vaccine_characteristics_model.set_vacc_waning(row_vaccine_characteristics['vacc_waning'])
			vaccine_characteristics_model.set_V1_mortality_reduction(row_vaccine_characteristics['V1_mortality_reduction'])
			vaccine_characteristics_model.set_V2_mortality_reduction(row_vaccine_characteristics['V2_mortality_reduction'])
			vaccine_characteristics_model.set_V1_transmission_reduction(row_vaccine_characteristics['V1_transmission_reduction'])
			vaccine_characteristics_model.set_V2_transmission_reduction(row_vaccine_characteristics['V2_transmission_reduction'])
			vaccine_characteristics_model.set_vaccine_cost(row_vaccine_characteristics['vaccine_cost'])
			vaccine_characteristics_model.save()


		filename_risk_level_breeder= options['filename_risk_level_breeder']
		data_risk_level_breeder = pd.read_csv(filename_risk_level_breeder, sep=',')
		risk_level_breeder.objects.all().delete()
		for index_risk_level_breeder, row_risk_level_breeder in data_risk_level_breeder.iterrows():
			risk_level_breeder_model = risk_level_breeder()
			risk_level_breeder_model.set_name(row_risk_level_breeder['name'])
			risk_level_breeder_model.set_prop_R(row_risk_level_breeder['prop_R'])
			risk_level_breeder_model.save()
