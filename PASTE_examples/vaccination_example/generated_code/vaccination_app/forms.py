from django import forms 
from django.forms.widgets import Select, HiddenInput, CheckboxInput, NumberInput, TextInput 
from .models import *
from django_select2.forms import ModelSelect2Widget

class initial_conditions(forms.Form):
	risk_level_breeder= forms.ModelChoiceField(queryset=risk_level_breeder.objects.all(),label="Risk level during breeder",required=True,help_text="Choose a risk level",widget=forms.Select(attrs={'class': 'form-control'}))
	prop_R=forms.FloatField(widget=forms.HiddenInput(),required=False)
	vaccine_characteristics= forms.ModelChoiceField(queryset=vaccine_characteristics.objects.all(),label="Vaccine characteristics",required=True,help_text="Choose a vaccine",widget=forms.Select(attrs={'class': 'form-control'}))
	delay_vacc=forms.IntegerField(widget=forms.HiddenInput(),required=False)
	dur_vacc=forms.IntegerField(widget=forms.HiddenInput(),required=False)
	vacc_waning=forms.CharField(widget=forms.HiddenInput(),required=False)
	V1_mortality_reduction=forms.FloatField(widget=forms.HiddenInput(),required=False)
	V2_mortality_reduction=forms.FloatField(widget=forms.HiddenInput(),required=False)
	V1_transmission_reduction=forms.FloatField(widget=forms.HiddenInput(),required=False)
	V2_transmission_reduction=forms.FloatField(widget=forms.HiddenInput(),required=False)
	vaccine_cost=forms.IntegerField(widget=forms.HiddenInput(),required=False)
	cost_purchase_calf=forms.IntegerField(label="cost_purchase_calf",required=True,help_text="cost_purchase_calf",widget=NumberInput(attrs={'class': 'form-control'}),initial=950)
	selling_price_bull=forms.IntegerField(label="selling_price_bull",required=True,help_text="selling_price_bull",widget=NumberInput(attrs={'class': 'form-control'}),initial=1330)
	annual_feeding_cost=forms.IntegerField(label="annual_feeding_cost",required=True,help_text="annual_feeding_cost",widget=NumberInput(attrs={'class': 'form-control'}),initial=312)

	def __init__(self, *args, **kwargs):

		super(initial_conditions, self).__init__(*args, **kwargs)

		self.initial_herd_composition_cells = []
		self.initial_herd_composition_rows_names = ['NV', 'V1', 'V2']
		self.initial_herd_composition_columns_names = ['LowRisk', 'MediumRisk', 'HighRisk']
		self.initial_herd_composition_values=0
		for row in range(0, 3):
			self.initial_herd_composition_cells.append({"row_label": f"{self.initial_herd_composition_rows_names[row]}","data": []})
			for col in range(0, 3):
				self.initial_herd_composition_cells[row]["data"].append({"name": f"nb_animals_{self.initial_herd_composition_rows_names[row]}_{self.initial_herd_composition_columns_names[col]}","value": self.initial_herd_composition_values})


class select_scenario(forms.Form):
	selecting_scenarios_to_run = forms.MultipleChoiceField(
	required=False,
	help_text="When a box is selected, this scenario is added to the reference scenarios and run in Emulsion",
	widget=forms.CheckboxSelectMultiple(attrs={'class': 'inline'}),
	choices=(('disease_no_vaccination', 'disease_no_vaccination'), ('disease_initial_vaccination', 'disease_initial_vaccination'), ('disease_periodic_vaccination', 'disease_periodic_vaccination')))
class execution(forms.Form):
	total_duration=forms.IntegerField(label="Simulation duration (half days)",required=True,widget=NumberInput(attrs={'data-min': 0, 'data-step': 2, 'data-value': 60, 'data-skin': 'round', 'data-label': 'half days', 'data-grid': 'true', 'data-grid-num': 5, 'data-max': 100, 'data-postfix': ' half days', 'class': 'js-range-slider'}))
	simulation_name=forms.CharField(label="Simulation name",required=True,widget=forms.TextInput(attrs={'class': 'form-control'}))
	simulation_description=forms.CharField(label="Simulation description",required=False,widget=forms.Textarea(attrs={'class': 'form-control'}))
