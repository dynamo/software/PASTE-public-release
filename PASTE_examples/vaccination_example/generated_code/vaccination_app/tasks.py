from celery import shared_task, chord, group, chain
import subprocess
import os
BASE_DIR=os.getcwd()

@shared_task
def run_scenario(scenario_name=""):
	cmd=subprocess.run(scenario_name, shell=True)

@shared_task
def run_statistics_for_each_scenario(*list_parameters_for_each_scenario):
	cmd = ["Rscript", "statistics.R"]
	flat_list = [item for sublist in list(list_parameters_for_each_scenario) for item in sublist]
	cmd = cmd + flat_list
	p = subprocess.Popen(cmd, cwd=BASE_DIR + "/vaccination_app/Rscripts/" ,stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	output, error = p.communicate()
	if p.returncode == 0:
		print('R OUTPUT:\n {0}'.format(output))
	else:
		print('R ERROR:\n {0}'.format(error))

@shared_task
def run_scenarios_comparison(*list_of_scenarios):
	cmd = ["Rscript", "statistics_comparison_scenario.R"]
	flat_list = [item for sublist in list(list_of_scenarios) for item in sublist]
	cmd = cmd + flat_list
	p = subprocess.Popen(cmd, cwd=BASE_DIR + "/vaccination_app/Rscripts/", stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	output, error = p.communicate()
	if p.returncode == 0:
		print('R OUTPUT:\n {0}'.format(output))
	else:
		print('R ERROR:\n {0}'.format(error))