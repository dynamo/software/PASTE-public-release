This README file describes the installation of decision tools according to yaml file and how to run it locally.

# General Information

The aim of this project is developing the software
infrastructure to automatize the production of **decision tools** from the framework **EMULSION** epidemiological models.
Framework EMULSION is intended for modellers in epidemiology, to help them design,
simulate, and revise complex mechanistic **stochastic models**, without having
to write or rewrite huge amounts of code.

- **Main contributors and contact:**
  - Guita Niang 
  - Sébastien Picault (`sebastien.picault@inrae.fr`)
  - Vianney Sicard 
  - Pauline Ezanno 


## INPUT FILES

  - EMULSION model input file
  - YAML File to describe the tool
  - CSV files linked to the block parameters in the tool description yaml file
  - The README: steps for installing the application
  - The required modules to install for the application
  - Examples of django files that are generated when the tool description yaml file is parsed
  - Some static files containing the logos and pages CSS description

## Requirements

### Install R

1. Check the version of your R (version == R version 3.6.1 (2019-07-05))   

       R --version

2. If not installed, run

       sudo apt install r-base

3. Install R packages, run

       sudo R -e "install.packages('shiny', repos='https://cran.rstudio.com/')"
       sudo R -e "install.packages('shinythemes', repos='https://cran.rstudio.com/')"
       sudo R -e "install.packages('shinyBS', repos='https://cran.rstudio.com/')"
       sudo R -e "install.packages('tidyverse', repos='https://cran.rstudio.com/')"
       sudo R -e "install.packages('readr', repos='https://cran.rstudio.com/')"
       sudo R -e "install.packages('dplyr', repos='https://cran.rstudio.com/')"
       sudo R -e "install.packages('tidyr', repos='https://cran.rstudio.com/')"
       sudo R -e "install.packages('ggplot2', repos='https://cran.rstudio.com/')"
       sudo R -e "install.packages('DT', repos='https://cran.rstudio.com/')"

### Install Redis server

- On Linux:

       sudo apt install redis-server

- On MacOS, with `homebrew`:

       brew install redis

### Install Python3

    1. Check the version of your python (version >= Python 3.7.5)   

            python3 -V

    2. If not installed, run

            sudo apt-get install python3

### Install Pip3

    1. Check the version of your pip3 (version >= pip 20.3.3)   

            pip3 --version

    2. If not installed, run

            sudo apt-get install python3-pip

    3. (Optional) Upgrade the module installer

            python3 -m pip install --upgrade pip

PS: If you don't want to use a virtual environment, you can still install everything locally.
I will recommend you to create a virtual environment. It is better to avoid conflicts between modules.

### Create a virtual environment

    1. Check if you have a virtual environment (version >= 20.2.2)

            pip show virtualenv  

    2. If not installed, run

            sudo pip3 install virtualenv

    3. If installed, create a virtual environment

            python3 -m venv <myenvname>

    4. Activate the virtual environment

            source <myenvname>/bin/activate


### Clone the **PASTE** repository 

From Sourcesup (replace `<username>` by your username on SourceSup)

    1. Check if you have git installed (version >= 2.20.1)

            git --version

    2. If not installed, run

            sudo apt install git

    3. Clone the PASTE-public-release repository:

        - From Forgemia:

            git clone git@forgemia.inra.fr:dynamo/software/PASTE-public-release.git

    4. Go to PASTE-public-release directory. This directory contains the **PASTE_CORE** directory.

            cd PASTE-public-release/

    PS: Make sure you have an account on SourceSup or Forgemia and that you have rights on the folder you want to clone.

    Do not forget to activate ssh key adding it to ssh configuration ssh-add ~/.ssh/id_rsa_inra

### Install the python3 requirements packages

      1. Run the requirements file from the documentation directory.

              pip3 install -r PASTE_CORE/documentation/requirements_important_modules.txt

      This command will install the following components :
  - django
  - emulsion==1.2b13
  - django-formtools 
  - django-select2
  - django-mathfilters
  - channels
  - Celery
  - redis
  - redis-server
  - bs4
  - requests
  - ansicolors


PS: If you have an error like:    
  ``ERROR: Failed building wheel for <a module>`` and ``error: invalid command 'bdist_wheel'``        
    No worries, that means pip tries to build a wheel for the module via **setup.py bdist_wheel**, but it fails.
    Then, pip falls back and install it directly using **setup.py install**.      
    Please have a look at the last line in the terminal. If it says that everything is installed
    successfully, then everything went well. If you are not sure try rerun the
    modules installation command, you will see that all the requirements are already satisfied.        
<br>

## Modify the tool description yaml file ``<path_to_paste_public_release_directory>/PASTE_examples/vaccination_example/vaccination_tool_specification/paste.yaml`` according to the vaccination example.
NB: The paste_specification folder can be copied in any wanted folder. You just have to make sure to inform the path to the folder.
- In **vaccination_example** directory, open paste.yaml file in vaccination_tool_specification directory using an editor.

    1. In section ``django_project_name: paste_project``, replace ``paste_project`` with your project name. Here ``vaccination_project``.

    2. In section ``django_app_name: paste_app``, replace ``paste_app`` with your application name. Here``vaccination_app``

    3. Make sure you entered the path to your csv input files (corresponds to block_values) correctly, i.e. relatively to the paste_specification directory.
       
## Create Django project and application according to paste.yaml file

- Run the script create_app.sh from the **paste-public-release/** directory to create the django application.
    If you rerun this script to create a new django project, if the name of your django project is the same.
    It is removed and replaced by the new one.
    
    The paste_specification directory containing input files: the paste.yaml file and the csv files for block_values. This directory can be located anywhere.
    The django project directory to be created can be located anywhere also in your path.
    Make sure to add the path for these two directories before executing create_app.sh script to create your django application. 
    The create_app.sh script uses management, static and vaccination_tool_specification directories.

        chmod +x create_app.sh
        ./create_app.sh -i <path_to_paste_public_release_directory>/PASTE_examples/vaccination_example/vaccination_tool_specification -d <path_to_paste_public_release_directory>/PASTE_examples
    
- Go to the django project directory that correspond to the **base directory**.

        cd PASTE_examples/vaccination_project

- Export the new Django settings file created. This file contains the configuration of Celery and static files. Run the command from the **base directory**.

        export DJANGO_SETTINGS_MODULE=vaccination_project.settings_paste

- Run the Django development server along with Celery worker process. Run the command from the **base directory**.<br>
PS: Do not forget to create and activate virtual environment as you did previously.


  1. Run redis-server to be sure that it is started or to start it

         redis-server

  2. Run the django server and celery

         celery -A vaccination_project worker -l info & python3 manage.py runserver

  3. Run the Shiny server

         R -e "shiny::runApp(appDir='shinyapp', port=8100)"


- Open a web browser with the following url:

        http://127.0.0.1:8000

- PASTE website

When you open the paste website, the first page corresponds to the form. The form has many pages, each page correspond to
input_forms informed in the paste.yaml file. A scenario can correspond to one or multiple page.

The step number indicates in which page number of the form we are, out of how many pages are there in the form.
The icons Next and Previous allow user to navigate through the different pages of the form.
And the Icon run submit the form.

When you reach the last page, as you submit the form, the EMULSION simulation runs and the result files added in subdiretories (which is named 
according to simulation name) in the emulsion outputs directory that is located in the **bdr_project directory**. 

Now, read the README_interface.md that explains how to navigate on the interface.


# Useful Links

1. Django
    - Tutorial: [https://docs.djangoproject.com/en/3.0/](https://docs.djangoproject.com/en/3.0/)
    - Licence: [https://github.com/django/django/blob/master/LICENSE](https://github.com/django/django/blob/master/LICENSE)

2. Celery
    - Tutorial: [https://docs.celeryproject.org/en/master/django/first-steps-with-django.html](https://docs.celeryproject.org/en/master/django/first-steps-with-django.html)
    - Licence: [https://github.com/celery/celery/blob/master/LICENSE](https://github.com/celery/celery/blob/master/LICENSE)

3. Redis
    - Tutorial: [https://redis.io](https://redis.io)
    - Licence: [https://redis.io/docs/about/license/](https://redis.io/docs/about/license/)

4. Channels
    - Tutorial: [https://channels.readthedocs.io/en/stable/introduction.html](https://channels.readthedocs.io/en/stable/introduction.html)
    - Licence: [https://github.com/django/channels/blob/main/LICENSE](https://github.com/django/channels/blob/main/LICENSE)

5. Requests
   - Tutorial: [https://requests.readthedocs.io/en/latest/](https://requests.readthedocs.io/en/latest/)
   - Licence: [https://github.com/psf/requests/blob/main/LICENSE](https://github.com/psf/requests/blob/main/LICENSE)s
