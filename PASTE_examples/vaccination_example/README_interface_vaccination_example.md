This README file describes the Vaccination model ran as example in PASTE.

# General Information

In this example, we have created a decision-support tool based on the EMULSION model of Vaccination.
We will explain below the interface generated by the paste engine.

# The interface created by PASTE engine coupled with the BRD model

Once the OAD corresponding to the model vaccination is created, the user can now navigate through pages, launch the simulation via Emulsion and get graphics on statistics.
The form is constituted of 3 sub-forms:

- The first page corresponds to "**initial conditions**" sub-form. It contains different parameters, emulsion and economics (non emulsion) parameters.
  Some of them are already filled by default but the values can be changed by the user.

<figure role="figure">
  <figcaption>Initial conditions form with all associated fields.</figcaption>
  <img src="README_images/initial_conditions_vaccination_example.png" alt="MarineGEO circle logo" style="height: 100%; width:100%;"/>
</figure>

  The fields "**risk level during breeder**" and "**vaccine characteristics**" is created from **block_values** in paste.yaml. They are drop-down lists in html.
  The user can choose one of each propositions from the list. Each proposition is associated to a bunch of emulsion or non emulsion parameters (indicated in csv files from block_values) that can be shown or not.

  The fields "**Cost purchase calf**", "**Selling price bull**", "**Annual feeding cost**" are economics parameters
  (non emulsion parameters). They are added to statistics and allow users to create graphics with both emulsion and non emulsion parameters.

  The field initial herd composition is matrices that allow user to add initial composition of animals in herd using vaccinal status and individual risk level.

<figure role="figure">
  <figcaption>Initial Herd composition matrice</figcaption>
  <img src="README_images/matrice.png" alt="MarineGEO circle logo" style="height: 100%; width:100%;"/>
</figure>

  After choosing all parameters for the initial conditions form, the user click on next button to submit this form and go to the next page.
  Doing that, the emulsion parameters associated is added to the emulsion run command according to the scenario.

- The next page is the "**Scenario selection**" form. These scenarios are optional. The user can select or not scenarios he wants to run. These are added to the non-optional scenarios that have been described in the paste.yaml file.
  For our example, we have selected both of the optional scenarios. Finally, we will have four scenarios to simulate.
  Then the user submit this form by clicking on the next button again or go back to the previous page by clicking on the previous button.


<figure role="figure">
  <figcaption>Selecting scenarios form </figcaption>
  <img src="README_images/scenarios_selection_vaccination_example.png" alt="MarineGEO circle logo" style="height: 100%; width:100%;"/>
</figure>

- This leads to the last page corresponding to the "**Execution**" sub-form. It contains "**Simulation duration (half days)**", "**Simulation name**" and "**Simulation description**" fields.
  The "**Simulation duration (half days)**" allow the user to specify the number of time steps to run in each repetition of the simulation.


<figure role="figure">
  <figcaption>Execution page</figcaption>
  <img src="README_images/execution_vaccination_example.png" alt="MarineGEO circle logo" style="height: 100%; width:100%;"/>
</figure>

  Finally, the user click on button submit of the execution sub-form and the simulation is launched in the background using Emulsion. The message "Processing simulations...".


- At the end of the simulations, the user is redirected on Rshiny application page to see the graphics according to the statistics calculated based on the paste.yaml file information.
  The statistics page contains also sub-pages. Each sub-page show different graphics using the menu on the left side.

<figure role="figure">
  <figcaption>Grahics page with the first sub-page **Batches** selected.</figcaption>
  <img src="README_images/mortality_page.png" alt="MarineGEO circle logo" style="height: 100%; width:100%;"/>
</figure>

<figure role="figure">
  <figcaption>Sub-page **Economics** showing gain graphic</figcaption>
  <img src="README_images/cost.png" alt="MarineGEO circle logo" style="height: 100%; width:100%;"/>
</figure>

The user can click on the button "**Back to the form**" to go back to the first page of the form and fill it again to run new simulations with different values of parameters.
