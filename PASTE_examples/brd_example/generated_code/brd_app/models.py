from django.db import models

class risk_level(models.Model): 

	name=models.CharField(max_length=100)
	def get_name(self):
		return self.name
	def set_name(self,name):
		self.name = name


	prop_lowrisk=models.FloatField()
	def get_prop_lowrisk(self):
		return self.prop_lowrisk
	def set_prop_lowrisk(self,prop_lowrisk):
		self.prop_lowrisk = prop_lowrisk


	prop_mediumrisk=models.FloatField()
	def get_prop_mediumrisk(self):
		return self.prop_mediumrisk
	def set_prop_mediumrisk(self,prop_mediumrisk):
		self.prop_mediumrisk = prop_mediumrisk


	prop_highrisk=models.FloatField()
	def get_prop_highrisk(self):
		return self.prop_highrisk
	def set_prop_highrisk(self,prop_highrisk):
		self.prop_highrisk = prop_highrisk

	def __str__(self):
		return ("{}").format(self.name)

class pathogen(models.Model): 

	name=models.CharField(max_length=100)
	def get_name(self):
		return self.name
	def set_name(self,name):
		self.name = name


	mean_dur_CS=models.IntegerField()
	def get_mean_dur_CS(self):
		return self.mean_dur_CS
	def set_mean_dur_CS(self,mean_dur_CS):
		self.mean_dur_CS = mean_dur_CS


	viral_infection=models.IntegerField()
	def get_viral_infection(self):
		return self.viral_infection
	def set_viral_infection(self,viral_infection):
		self.viral_infection = viral_infection


	pasteurella=models.IntegerField()
	def get_pasteurella(self):
		return self.pasteurella
	def set_pasteurella(self,pasteurella):
		self.pasteurella = pasteurella


	mycoplasma=models.IntegerField()
	def get_mycoplasma(self):
		return self.mycoplasma
	def set_mycoplasma(self,mycoplasma):
		self.mycoplasma = mycoplasma


	p_severe_clinical=models.FloatField()
	def get_p_severe_clinical(self):
		return self.p_severe_clinical
	def set_p_severe_clinical(self,p_severe_clinical):
		self.p_severe_clinical = p_severe_clinical


	dur_I_shape=models.FloatField()
	def get_dur_I_shape(self):
		return self.dur_I_shape
	def set_dur_I_shape(self,dur_I_shape):
		self.dur_I_shape = dur_I_shape


	dur_I_scale=models.FloatField()
	def get_dur_I_scale(self):
		return self.dur_I_scale
	def set_dur_I_scale(self,dur_I_scale):
		self.dur_I_scale = dur_I_scale


	p_reactivation_HighRisk=models.FloatField()
	def get_p_reactivation_HighRisk(self):
		return self.p_reactivation_HighRisk
	def set_p_reactivation_HighRisk(self,p_reactivation_HighRisk):
		self.p_reactivation_HighRisk = p_reactivation_HighRisk


	p_reactivation_MediumRisk=models.FloatField()
	def get_p_reactivation_MediumRisk(self):
		return self.p_reactivation_MediumRisk
	def set_p_reactivation_MediumRisk(self,p_reactivation_MediumRisk):
		self.p_reactivation_MediumRisk = p_reactivation_MediumRisk


	p_reactivation_LowRisk=models.FloatField()
	def get_p_reactivation_LowRisk(self):
		return self.p_reactivation_LowRisk
	def set_p_reactivation_LowRisk(self,p_reactivation_LowRisk):
		self.p_reactivation_LowRisk = p_reactivation_LowRisk


	mean_dur_E=models.IntegerField()
	def get_mean_dur_E(self):
		return self.mean_dur_E
	def set_mean_dur_E(self,mean_dur_E):
		self.mean_dur_E = mean_dur_E


	transmission_lowrisk1=models.FloatField()
	def get_transmission_lowrisk1(self):
		return self.transmission_lowrisk1
	def set_transmission_lowrisk1(self,transmission_lowrisk1):
		self.transmission_lowrisk1 = transmission_lowrisk1


	transmission_mediumrisk1=models.FloatField()
	def get_transmission_mediumrisk1(self):
		return self.transmission_mediumrisk1
	def set_transmission_mediumrisk1(self,transmission_mediumrisk1):
		self.transmission_mediumrisk1 = transmission_mediumrisk1


	transmission_highrisk1=models.FloatField()
	def get_transmission_highrisk1(self):
		return self.transmission_highrisk1
	def set_transmission_highrisk1(self,transmission_highrisk1):
		self.transmission_highrisk1 = transmission_highrisk1


	p_recovery=models.FloatField()
	def get_p_recovery(self):
		return self.p_recovery
	def set_p_recovery(self,p_recovery):
		self.p_recovery = p_recovery


	proportion_carrier_low=models.FloatField()
	def get_proportion_carrier_low(self):
		return self.proportion_carrier_low
	def set_proportion_carrier_low(self,proportion_carrier_low):
		self.proportion_carrier_low = proportion_carrier_low


	proportion_carrier_medium=models.FloatField()
	def get_proportion_carrier_medium(self):
		return self.proportion_carrier_medium
	def set_proportion_carrier_medium(self,proportion_carrier_medium):
		self.proportion_carrier_medium = proportion_carrier_medium


	proportion_carrier_high=models.FloatField()
	def get_proportion_carrier_high(self):
		return self.proportion_carrier_high
	def set_proportion_carrier_high(self,proportion_carrier_high):
		self.proportion_carrier_high = proportion_carrier_high


	proportion_recovered_low=models.FloatField()
	def get_proportion_recovered_low(self):
		return self.proportion_recovered_low
	def set_proportion_recovered_low(self,proportion_recovered_low):
		self.proportion_recovered_low = proportion_recovered_low


	proportion_recovered_medium=models.FloatField()
	def get_proportion_recovered_medium(self):
		return self.proportion_recovered_medium
	def set_proportion_recovered_medium(self,proportion_recovered_medium):
		self.proportion_recovered_medium = proportion_recovered_medium


	proportion_recovered_high=models.FloatField()
	def get_proportion_recovered_high(self):
		return self.proportion_recovered_high
	def set_proportion_recovered_high(self,proportion_recovered_high):
		self.proportion_recovered_high = proportion_recovered_high

	def __str__(self):
		return ("{}").format(self.name)

class batch_isolation(models.Model): 

	name=models.CharField(max_length=100)
	def get_name(self):
		return self.name
	def set_name(self,name):
		self.name = name


	interbatch_contact_rate=models.FloatField()
	def get_interbatch_contact_rate(self):
		return self.interbatch_contact_rate
	def set_interbatch_contact_rate(self,interbatch_contact_rate):
		self.interbatch_contact_rate = interbatch_contact_rate

	def __str__(self):
		return ("{}").format(self.name)
