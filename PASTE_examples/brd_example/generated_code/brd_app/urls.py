from django.urls import path,  include, re_path
from . import views
from .views import FormWizard
from .forms import initial_conditions, select_scenario, execution

urlpatterns = [
	path('task/<str:task_id>/', views.TaskView.as_view(), name='task'),
	path("select2/',select2/", include("django_select2.urls")),
	re_path(r'^form/$', FormWizard.as_view([initial_conditions, select_scenario, execution]), name='form'),
	path('ajax/load_risk_level_details/', views.load_risk_level_details, name='load_risk_level_details'),
	path('ajax/load_pathogen_details/', views.load_pathogen_details, name='load_pathogen_details'),
	path('ajax/load_batch_isolation_details/', views.load_batch_isolation_details, name='load_batch_isolation_details'),
	path('shiny/', views.shiny, name='shiny'),
	path('shiny_contents/', views.shiny_contents, name='shiny_contents'),
]