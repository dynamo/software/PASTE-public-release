from django.core.management.base import BaseCommand, CommandError
import csv
import os
import sys
import pandas as pd
import numpy as np
from brd_app.models import *
class Command(BaseCommand):
	help = 'Import data from csv file to database'

	def add_arguments(self, parser):
		parser.add_argument('--filename_pathogen', type=str)
		parser.add_argument('--filename_batch_isolation', type=str)
		parser.add_argument('--filename_risk_level', type=str)

	def handle(self, *args, **options):


		filename_risk_level= options['filename_risk_level']
		data_risk_level = pd.read_csv(filename_risk_level, sep=',')
		risk_level.objects.all().delete()
		for index_risk_level, row_risk_level in data_risk_level.iterrows():
			risk_level_model = risk_level()
			risk_level_model.set_name(row_risk_level['name'])
			risk_level_model.set_prop_lowrisk(row_risk_level['prop_lowrisk'])
			risk_level_model.set_prop_mediumrisk(row_risk_level['prop_mediumrisk'])
			risk_level_model.set_prop_highrisk(row_risk_level['prop_highrisk'])
			risk_level_model.save()


		filename_pathogen= options['filename_pathogen']
		data_pathogen = pd.read_csv(filename_pathogen, sep=',')
		pathogen.objects.all().delete()
		for index_pathogen, row_pathogen in data_pathogen.iterrows():
			pathogen_model = pathogen()
			pathogen_model.set_name(row_pathogen['name'])
			pathogen_model.set_mean_dur_CS(row_pathogen['mean_dur_CS'])
			pathogen_model.set_viral_infection(row_pathogen['viral_infection'])
			pathogen_model.set_pasteurella(row_pathogen['pasteurella'])
			pathogen_model.set_mycoplasma(row_pathogen['mycoplasma'])
			pathogen_model.set_p_severe_clinical(row_pathogen['p_severe_clinical'])
			pathogen_model.set_dur_I_shape(row_pathogen['dur_I_shape'])
			pathogen_model.set_dur_I_scale(row_pathogen['dur_I_scale'])
			pathogen_model.set_p_reactivation_HighRisk(row_pathogen['p_reactivation_HighRisk'])
			pathogen_model.set_p_reactivation_MediumRisk(row_pathogen['p_reactivation_MediumRisk'])
			pathogen_model.set_p_reactivation_LowRisk(row_pathogen['p_reactivation_LowRisk'])
			pathogen_model.set_mean_dur_E(row_pathogen['mean_dur_E'])
			pathogen_model.set_transmission_lowrisk1(row_pathogen['transmission_lowrisk1'])
			pathogen_model.set_transmission_mediumrisk1(row_pathogen['transmission_mediumrisk1'])
			pathogen_model.set_transmission_highrisk1(row_pathogen['transmission_highrisk1'])
			pathogen_model.set_p_recovery(row_pathogen['p_recovery'])
			pathogen_model.set_proportion_carrier_low(row_pathogen['proportion_carrier_low'])
			pathogen_model.set_proportion_carrier_medium(row_pathogen['proportion_carrier_medium'])
			pathogen_model.set_proportion_carrier_high(row_pathogen['proportion_carrier_high'])
			pathogen_model.set_proportion_recovered_low(row_pathogen['proportion_recovered_low'])
			pathogen_model.set_proportion_recovered_medium(row_pathogen['proportion_recovered_medium'])
			pathogen_model.set_proportion_recovered_high(row_pathogen['proportion_recovered_high'])
			pathogen_model.save()


		filename_batch_isolation= options['filename_batch_isolation']
		data_batch_isolation = pd.read_csv(filename_batch_isolation, sep=',')
		batch_isolation.objects.all().delete()
		for index_batch_isolation, row_batch_isolation in data_batch_isolation.iterrows():
			batch_isolation_model = batch_isolation()
			batch_isolation_model.set_name(row_batch_isolation['name'])
			batch_isolation_model.set_interbatch_contact_rate(row_batch_isolation['interbatch_contact_rate'])
			batch_isolation_model.save()
