import os
from django.shortcuts import render
from django.conf import settings
from django.views import View
from django.http import JsonResponse
from formtools.wizard.forms import ManagementForm
from formtools.wizard.views import SessionWizardView
from .models import *
from .tasks import *
from celery import current_app
from django.core.exceptions import ValidationError
from django.core.files.storage import FileSystemStorage
import requests
import re
import fileinput

FormWizardTemplatesFiles = {"0": "initial_conditions.html", "1": "select_scenario.html","2": "execution.html"}


class FormWizard(SessionWizardView):
	form_list = ['initial_conditions', 'select_scenario', 'execution']
	file_storage = FileSystemStorage(location=os.path.join(settings.MEDIA_ROOT))

	def get_template_names(self):
		return [FormWizardTemplatesFiles[self.steps.current]]

	def get_form(self, step=None, data=None, files=None):
		if step is None:
			step = self.steps.current
		form_class = self.form_list[step]
		kwargs = self.get_form_kwargs(step)
		kwargs.update({
			'data': data,
			'files': files,
			'prefix': self.get_form_prefix(step, form_class),
			'initial': self.get_form_initial(step),
		})
		return form_class(**kwargs)

	def get_context_data(self, form, **kwargs):
		context = super(FormWizard, self).get_context_data(form=form, **kwargs)
		context.update({
			'all_data': self.get_all_cleaned_data(),
		})
		return context

	def post(self, *args, **kwargs):
		if self.request.method == 'GET':
			self.storage.reset()
			self.storage.current_step = self.steps.first
			self.admin_view.model_form = self.get_step_form()
		else:
			wizard_goto_step = self.request.POST.get('wizard_goto_step', None)
			if wizard_goto_step and wizard_goto_step in self.get_form_list():
				return self.render_goto_step(wizard_goto_step)
		management_form = ManagementForm(self.request.POST, prefix=self.prefix)
		if not management_form.is_valid():
			raise ValidationError('ManagementForm data is missing or has been tampered.')


		form_current_step = management_form.cleaned_data['current_step']
		if (form_current_step != self.steps.current and
				self.storage.current_step is not None):
			self.storage.current_step = form_current_step
		form = self.get_form(data=self.request.POST, files=self.request.FILES)

		input_file_path = "/home/gniang/renaming_atom_to_paste/brd_project/brd_tool_specification/input_files/brd-svepm-paste.yaml"
		code_path = "/home/gniang/renaming_atom_to_paste/brd_project/brd_tool_specification/input_files"
		var = " emulsion run --silent {input_file_path} --code-path {code_path}".format(input_file_path=input_file_path, code_path=code_path)

		dict_non_emulsion_parameters = {}
		dict_non_emulsion_parameters_in_scenario = {}
		if form.is_valid():
			self.storage.set_step_data(self.steps.current, self.process_step(form))
			self.storage.set_step_files(self.steps.current, self.process_step_files(form))

			if self.steps.current == '0':

				risk_level_id = self.request.POST.get('0-risk_level')

				risk_level_name = list(risk_level.objects.filter(id=int(risk_level_id)).values_list('name', flat=True))

				prop_lowrisk = self.request.POST.get('0-prop_lowrisk')

				prop_mediumrisk = self.request.POST.get('0-prop_mediumrisk')

				prop_highrisk = self.request.POST.get('0-prop_highrisk')

				batch_isolation_id = self.request.POST.get('0-batch_isolation')

				batch_isolation_name = list(batch_isolation.objects.filter(id=int(batch_isolation_id)).values_list('name', flat=True))

				interbatch_contact_rate = self.request.POST.get('0-interbatch_contact_rate')

				pathogen_id = self.request.POST.get('0-pathogen')

				pathogen_name = list(pathogen.objects.filter(id=int(pathogen_id)).values_list('name', flat=True))

				mean_dur_CS = self.request.POST.get('0-mean_dur_CS')

				viral_infection = self.request.POST.get('0-viral_infection')

				pasteurella = self.request.POST.get('0-pasteurella')

				mycoplasma = self.request.POST.get('0-mycoplasma')

				p_severe_clinical = self.request.POST.get('0-p_severe_clinical')

				dur_I_shape = self.request.POST.get('0-dur_I_shape')

				dur_I_scale = self.request.POST.get('0-dur_I_scale')

				p_reactivation_HighRisk = self.request.POST.get('0-p_reactivation_HighRisk')

				p_reactivation_MediumRisk = self.request.POST.get('0-p_reactivation_MediumRisk')

				p_reactivation_LowRisk = self.request.POST.get('0-p_reactivation_LowRisk')

				mean_dur_E = self.request.POST.get('0-mean_dur_E')

				transmission_lowrisk1 = self.request.POST.get('0-transmission_lowrisk1')

				transmission_mediumrisk1 = self.request.POST.get('0-transmission_mediumrisk1')

				transmission_highrisk1 = self.request.POST.get('0-transmission_highrisk1')

				p_recovery = self.request.POST.get('0-p_recovery')

				proportion_carrier_low = self.request.POST.get('0-proportion_carrier_low')

				proportion_carrier_medium = self.request.POST.get('0-proportion_carrier_medium')

				proportion_carrier_high = self.request.POST.get('0-proportion_carrier_high')

				proportion_recovered_low = self.request.POST.get('0-proportion_recovered_low')

				proportion_recovered_medium = self.request.POST.get('0-proportion_recovered_medium')

				proportion_recovered_high = self.request.POST.get('0-proportion_recovered_high')

				cost_of_antiobiotic_dose = self.request.POST.get('0-cost_of_antiobiotic_dose')

				cost_purchase_calf = self.request.POST.get('0-cost_purchase_calf')

				selling_price_bull = self.request.POST.get('0-selling_price_bull')

				total_feeding_cost = self.request.POST.get('0-total_feeding_cost')

				daily_disease_impact = self.request.POST.get('0-daily_disease_impact')

				number_batches = self.request.POST.get('0-number_batches')

				batch_size = self.request.POST.get('0-batch_size')

				var_no_disease_mixed_batches = var 

				dict_non_emulsion_parameters["no_disease_mixed_batches"]={"cost_of_antiobiotic_dose" : cost_of_antiobiotic_dose ,"cost_purchase_calf" : cost_purchase_calf ,"selling_price_bull" : selling_price_bull ,"total_feeding_cost" : total_feeding_cost ,"daily_disease_impact" : daily_disease_impact }
				dict_non_emulsion_parameters_in_scenario["no_disease_mixed_batches"]={}
				self.request.session['dict_non_emulsion_parameters_initial_conditions'] = dict_non_emulsion_parameters
				self.request.session['dict_non_emulsion_parameters_in_scenario_initial_conditions'] = dict_non_emulsion_parameters_in_scenario
				if mean_dur_CS != '' or mean_dur_CS is not None:
					var_no_disease_mixed_batches = var_no_disease_mixed_batches + (" -p mean_dur_CS={}".format(mean_dur_CS))
				if p_severe_clinical != '' or p_severe_clinical is not None:
					var_no_disease_mixed_batches = var_no_disease_mixed_batches + (" -p p_severe_clinical={}".format(p_severe_clinical))
				if dur_I_shape != '' or dur_I_shape is not None:
					var_no_disease_mixed_batches = var_no_disease_mixed_batches + (" -p dur_I_shape={}".format(dur_I_shape))
				if dur_I_scale != '' or dur_I_scale is not None:
					var_no_disease_mixed_batches = var_no_disease_mixed_batches + (" -p dur_I_scale={}".format(dur_I_scale))
				if p_reactivation_HighRisk != '' or p_reactivation_HighRisk is not None:
					var_no_disease_mixed_batches = var_no_disease_mixed_batches + (" -p p_reactivation_HighRisk={}".format(p_reactivation_HighRisk))
				if p_reactivation_MediumRisk != '' or p_reactivation_MediumRisk is not None:
					var_no_disease_mixed_batches = var_no_disease_mixed_batches + (" -p p_reactivation_MediumRisk={}".format(p_reactivation_MediumRisk))
				if p_reactivation_LowRisk != '' or p_reactivation_LowRisk is not None:
					var_no_disease_mixed_batches = var_no_disease_mixed_batches + (" -p p_reactivation_LowRisk={}".format(p_reactivation_LowRisk))
				if mean_dur_E != '' or mean_dur_E is not None:
					var_no_disease_mixed_batches = var_no_disease_mixed_batches + (" -p mean_dur_E={}".format(mean_dur_E))
				if transmission_lowrisk1 != '' or transmission_lowrisk1 is not None:
					var_no_disease_mixed_batches = var_no_disease_mixed_batches + (" -p transmission_lowrisk1={}".format(transmission_lowrisk1))
				if transmission_mediumrisk1 != '' or transmission_mediumrisk1 is not None:
					var_no_disease_mixed_batches = var_no_disease_mixed_batches + (" -p transmission_mediumrisk1={}".format(transmission_mediumrisk1))
				if transmission_highrisk1 != '' or transmission_highrisk1 is not None:
					var_no_disease_mixed_batches = var_no_disease_mixed_batches + (" -p transmission_highrisk1={}".format(transmission_highrisk1))
				if interbatch_contact_rate != '' or interbatch_contact_rate is not None:
					var_no_disease_mixed_batches = var_no_disease_mixed_batches + (" -p interbatch_contact_rate={}".format(interbatch_contact_rate))
				if p_recovery != '' or p_recovery is not None:
					var_no_disease_mixed_batches = var_no_disease_mixed_batches + (" -p p_recovery={}".format(p_recovery))
				if proportion_carrier_low != '' or proportion_carrier_low is not None:
					var_no_disease_mixed_batches = var_no_disease_mixed_batches + (" -p proportion_carrier_low={}".format(proportion_carrier_low))
				if proportion_carrier_medium != '' or proportion_carrier_medium is not None:
					var_no_disease_mixed_batches = var_no_disease_mixed_batches + (" -p proportion_carrier_medium={}".format(proportion_carrier_medium))
				if proportion_carrier_high != '' or proportion_carrier_high is not None:
					var_no_disease_mixed_batches = var_no_disease_mixed_batches + (" -p proportion_carrier_high={}".format(proportion_carrier_high))
				if proportion_recovered_low != '' or proportion_recovered_low is not None:
					var_no_disease_mixed_batches = var_no_disease_mixed_batches + (" -p proportion_recovered_low={}".format(proportion_recovered_low))
				if proportion_recovered_medium != '' or proportion_recovered_medium is not None:
					var_no_disease_mixed_batches = var_no_disease_mixed_batches + (" -p proportion_recovered_medium={}".format(proportion_recovered_medium))
				if proportion_recovered_high != '' or proportion_recovered_high is not None:
					var_no_disease_mixed_batches = var_no_disease_mixed_batches + (" -p proportion_recovered_high={}".format(proportion_recovered_high))
				if prop_lowrisk != '' or prop_lowrisk is not None:
					var_no_disease_mixed_batches = var_no_disease_mixed_batches + (" -p prop_lowrisk={}".format(prop_lowrisk))
				if prop_mediumrisk != '' or prop_mediumrisk is not None:
					var_no_disease_mixed_batches = var_no_disease_mixed_batches + (" -p prop_mediumrisk={}".format(prop_mediumrisk))
				if prop_highrisk != '' or prop_highrisk is not None:
					var_no_disease_mixed_batches = var_no_disease_mixed_batches + (" -p prop_highrisk={}".format(prop_highrisk))
				if viral_infection != '' or viral_infection is not None:
					var_no_disease_mixed_batches = var_no_disease_mixed_batches + (" -p viral_infection={}".format(viral_infection))
				if pasteurella != '' or pasteurella is not None:
					var_no_disease_mixed_batches = var_no_disease_mixed_batches + (" -p pasteurella={}".format(pasteurella))
				if mycoplasma != '' or mycoplasma is not None:
					var_no_disease_mixed_batches = var_no_disease_mixed_batches + (" -p mycoplasma={}".format(mycoplasma))


				var_no_disease_mixed_batches = var_no_disease_mixed_batches + " -r 20"
				var_no_disease_mixed_batches = var_no_disease_mixed_batches + " -p proportion_carrier_low=0"
				var_no_disease_mixed_batches = var_no_disease_mixed_batches + " -p proportion_carrier_medium=0"
				var_no_disease_mixed_batches = var_no_disease_mixed_batches + " -p proportion_carrier_high=0"
				var_no_disease_mixed_batches = var_no_disease_mixed_batches + " -p sorted_batches=0"
				if batch_size != '' or batch_size is not None:
					var_no_disease_mixed_batches = var_no_disease_mixed_batches + (" -p batch_size={}".format(batch_size))
				if number_batches != '' or number_batches is not None:
					var_no_disease_mixed_batches = var_no_disease_mixed_batches + (" -p number_batches={}".format(number_batches))

				self.request.session['var_no_disease_mixed_batches'] = var_no_disease_mixed_batches


				var_no_disease_sorted_batches = var 

				dict_non_emulsion_parameters["no_disease_sorted_batches"]={"cost_of_antiobiotic_dose" : cost_of_antiobiotic_dose ,"cost_purchase_calf" : cost_purchase_calf ,"selling_price_bull" : selling_price_bull ,"total_feeding_cost" : total_feeding_cost ,"daily_disease_impact" : daily_disease_impact }
				dict_non_emulsion_parameters_in_scenario["no_disease_sorted_batches"]={}
				self.request.session['dict_non_emulsion_parameters_initial_conditions'] = dict_non_emulsion_parameters
				self.request.session['dict_non_emulsion_parameters_in_scenario_initial_conditions'] = dict_non_emulsion_parameters_in_scenario
				if mean_dur_CS != '' or mean_dur_CS is not None:
					var_no_disease_sorted_batches = var_no_disease_sorted_batches + (" -p mean_dur_CS={}".format(mean_dur_CS))
				if p_severe_clinical != '' or p_severe_clinical is not None:
					var_no_disease_sorted_batches = var_no_disease_sorted_batches + (" -p p_severe_clinical={}".format(p_severe_clinical))
				if dur_I_shape != '' or dur_I_shape is not None:
					var_no_disease_sorted_batches = var_no_disease_sorted_batches + (" -p dur_I_shape={}".format(dur_I_shape))
				if dur_I_scale != '' or dur_I_scale is not None:
					var_no_disease_sorted_batches = var_no_disease_sorted_batches + (" -p dur_I_scale={}".format(dur_I_scale))
				if p_reactivation_HighRisk != '' or p_reactivation_HighRisk is not None:
					var_no_disease_sorted_batches = var_no_disease_sorted_batches + (" -p p_reactivation_HighRisk={}".format(p_reactivation_HighRisk))
				if p_reactivation_MediumRisk != '' or p_reactivation_MediumRisk is not None:
					var_no_disease_sorted_batches = var_no_disease_sorted_batches + (" -p p_reactivation_MediumRisk={}".format(p_reactivation_MediumRisk))
				if p_reactivation_LowRisk != '' or p_reactivation_LowRisk is not None:
					var_no_disease_sorted_batches = var_no_disease_sorted_batches + (" -p p_reactivation_LowRisk={}".format(p_reactivation_LowRisk))
				if mean_dur_E != '' or mean_dur_E is not None:
					var_no_disease_sorted_batches = var_no_disease_sorted_batches + (" -p mean_dur_E={}".format(mean_dur_E))
				if transmission_lowrisk1 != '' or transmission_lowrisk1 is not None:
					var_no_disease_sorted_batches = var_no_disease_sorted_batches + (" -p transmission_lowrisk1={}".format(transmission_lowrisk1))
				if transmission_mediumrisk1 != '' or transmission_mediumrisk1 is not None:
					var_no_disease_sorted_batches = var_no_disease_sorted_batches + (" -p transmission_mediumrisk1={}".format(transmission_mediumrisk1))
				if transmission_highrisk1 != '' or transmission_highrisk1 is not None:
					var_no_disease_sorted_batches = var_no_disease_sorted_batches + (" -p transmission_highrisk1={}".format(transmission_highrisk1))
				if interbatch_contact_rate != '' or interbatch_contact_rate is not None:
					var_no_disease_sorted_batches = var_no_disease_sorted_batches + (" -p interbatch_contact_rate={}".format(interbatch_contact_rate))
				if p_recovery != '' or p_recovery is not None:
					var_no_disease_sorted_batches = var_no_disease_sorted_batches + (" -p p_recovery={}".format(p_recovery))
				if proportion_carrier_low != '' or proportion_carrier_low is not None:
					var_no_disease_sorted_batches = var_no_disease_sorted_batches + (" -p proportion_carrier_low={}".format(proportion_carrier_low))
				if proportion_carrier_medium != '' or proportion_carrier_medium is not None:
					var_no_disease_sorted_batches = var_no_disease_sorted_batches + (" -p proportion_carrier_medium={}".format(proportion_carrier_medium))
				if proportion_carrier_high != '' or proportion_carrier_high is not None:
					var_no_disease_sorted_batches = var_no_disease_sorted_batches + (" -p proportion_carrier_high={}".format(proportion_carrier_high))
				if proportion_recovered_low != '' or proportion_recovered_low is not None:
					var_no_disease_sorted_batches = var_no_disease_sorted_batches + (" -p proportion_recovered_low={}".format(proportion_recovered_low))
				if proportion_recovered_medium != '' or proportion_recovered_medium is not None:
					var_no_disease_sorted_batches = var_no_disease_sorted_batches + (" -p proportion_recovered_medium={}".format(proportion_recovered_medium))
				if proportion_recovered_high != '' or proportion_recovered_high is not None:
					var_no_disease_sorted_batches = var_no_disease_sorted_batches + (" -p proportion_recovered_high={}".format(proportion_recovered_high))
				if prop_lowrisk != '' or prop_lowrisk is not None:
					var_no_disease_sorted_batches = var_no_disease_sorted_batches + (" -p prop_lowrisk={}".format(prop_lowrisk))
				if prop_mediumrisk != '' or prop_mediumrisk is not None:
					var_no_disease_sorted_batches = var_no_disease_sorted_batches + (" -p prop_mediumrisk={}".format(prop_mediumrisk))
				if prop_highrisk != '' or prop_highrisk is not None:
					var_no_disease_sorted_batches = var_no_disease_sorted_batches + (" -p prop_highrisk={}".format(prop_highrisk))
				if viral_infection != '' or viral_infection is not None:
					var_no_disease_sorted_batches = var_no_disease_sorted_batches + (" -p viral_infection={}".format(viral_infection))
				if pasteurella != '' or pasteurella is not None:
					var_no_disease_sorted_batches = var_no_disease_sorted_batches + (" -p pasteurella={}".format(pasteurella))
				if mycoplasma != '' or mycoplasma is not None:
					var_no_disease_sorted_batches = var_no_disease_sorted_batches + (" -p mycoplasma={}".format(mycoplasma))


				var_no_disease_sorted_batches = var_no_disease_sorted_batches + " -r 20"
				var_no_disease_sorted_batches = var_no_disease_sorted_batches + " -p proportion_carrier_low=0"
				var_no_disease_sorted_batches = var_no_disease_sorted_batches + " -p proportion_carrier_medium=0"
				var_no_disease_sorted_batches = var_no_disease_sorted_batches + " -p proportion_carrier_high=0"
				var_no_disease_sorted_batches = var_no_disease_sorted_batches + " -p sorted_batches=1"
				if batch_size != '' or batch_size is not None:
					var_no_disease_sorted_batches = var_no_disease_sorted_batches + (" -p batch_size={}".format(batch_size))
				if number_batches != '' or number_batches is not None:
					var_no_disease_sorted_batches = var_no_disease_sorted_batches + (" -p number_batches={}".format(number_batches))

				self.request.session['var_no_disease_sorted_batches'] = var_no_disease_sorted_batches


				var_disease_mixed_batches = var 

				dict_non_emulsion_parameters["disease_mixed_batches"]={"cost_of_antiobiotic_dose" : cost_of_antiobiotic_dose ,"cost_purchase_calf" : cost_purchase_calf ,"selling_price_bull" : selling_price_bull ,"total_feeding_cost" : total_feeding_cost ,"daily_disease_impact" : daily_disease_impact }
				dict_non_emulsion_parameters_in_scenario["disease_mixed_batches"]={}
				self.request.session['dict_non_emulsion_parameters_initial_conditions'] = dict_non_emulsion_parameters
				self.request.session['dict_non_emulsion_parameters_in_scenario_initial_conditions'] = dict_non_emulsion_parameters_in_scenario
				if mean_dur_CS != '' or mean_dur_CS is not None:
					var_disease_mixed_batches = var_disease_mixed_batches + (" -p mean_dur_CS={}".format(mean_dur_CS))
				if p_severe_clinical != '' or p_severe_clinical is not None:
					var_disease_mixed_batches = var_disease_mixed_batches + (" -p p_severe_clinical={}".format(p_severe_clinical))
				if dur_I_shape != '' or dur_I_shape is not None:
					var_disease_mixed_batches = var_disease_mixed_batches + (" -p dur_I_shape={}".format(dur_I_shape))
				if dur_I_scale != '' or dur_I_scale is not None:
					var_disease_mixed_batches = var_disease_mixed_batches + (" -p dur_I_scale={}".format(dur_I_scale))
				if p_reactivation_HighRisk != '' or p_reactivation_HighRisk is not None:
					var_disease_mixed_batches = var_disease_mixed_batches + (" -p p_reactivation_HighRisk={}".format(p_reactivation_HighRisk))
				if p_reactivation_MediumRisk != '' or p_reactivation_MediumRisk is not None:
					var_disease_mixed_batches = var_disease_mixed_batches + (" -p p_reactivation_MediumRisk={}".format(p_reactivation_MediumRisk))
				if p_reactivation_LowRisk != '' or p_reactivation_LowRisk is not None:
					var_disease_mixed_batches = var_disease_mixed_batches + (" -p p_reactivation_LowRisk={}".format(p_reactivation_LowRisk))
				if mean_dur_E != '' or mean_dur_E is not None:
					var_disease_mixed_batches = var_disease_mixed_batches + (" -p mean_dur_E={}".format(mean_dur_E))
				if transmission_lowrisk1 != '' or transmission_lowrisk1 is not None:
					var_disease_mixed_batches = var_disease_mixed_batches + (" -p transmission_lowrisk1={}".format(transmission_lowrisk1))
				if transmission_mediumrisk1 != '' or transmission_mediumrisk1 is not None:
					var_disease_mixed_batches = var_disease_mixed_batches + (" -p transmission_mediumrisk1={}".format(transmission_mediumrisk1))
				if transmission_highrisk1 != '' or transmission_highrisk1 is not None:
					var_disease_mixed_batches = var_disease_mixed_batches + (" -p transmission_highrisk1={}".format(transmission_highrisk1))
				if interbatch_contact_rate != '' or interbatch_contact_rate is not None:
					var_disease_mixed_batches = var_disease_mixed_batches + (" -p interbatch_contact_rate={}".format(interbatch_contact_rate))
				if p_recovery != '' or p_recovery is not None:
					var_disease_mixed_batches = var_disease_mixed_batches + (" -p p_recovery={}".format(p_recovery))
				if proportion_carrier_low != '' or proportion_carrier_low is not None:
					var_disease_mixed_batches = var_disease_mixed_batches + (" -p proportion_carrier_low={}".format(proportion_carrier_low))
				if proportion_carrier_medium != '' or proportion_carrier_medium is not None:
					var_disease_mixed_batches = var_disease_mixed_batches + (" -p proportion_carrier_medium={}".format(proportion_carrier_medium))
				if proportion_carrier_high != '' or proportion_carrier_high is not None:
					var_disease_mixed_batches = var_disease_mixed_batches + (" -p proportion_carrier_high={}".format(proportion_carrier_high))
				if proportion_recovered_low != '' or proportion_recovered_low is not None:
					var_disease_mixed_batches = var_disease_mixed_batches + (" -p proportion_recovered_low={}".format(proportion_recovered_low))
				if proportion_recovered_medium != '' or proportion_recovered_medium is not None:
					var_disease_mixed_batches = var_disease_mixed_batches + (" -p proportion_recovered_medium={}".format(proportion_recovered_medium))
				if proportion_recovered_high != '' or proportion_recovered_high is not None:
					var_disease_mixed_batches = var_disease_mixed_batches + (" -p proportion_recovered_high={}".format(proportion_recovered_high))
				if prop_lowrisk != '' or prop_lowrisk is not None:
					var_disease_mixed_batches = var_disease_mixed_batches + (" -p prop_lowrisk={}".format(prop_lowrisk))
				if prop_mediumrisk != '' or prop_mediumrisk is not None:
					var_disease_mixed_batches = var_disease_mixed_batches + (" -p prop_mediumrisk={}".format(prop_mediumrisk))
				if prop_highrisk != '' or prop_highrisk is not None:
					var_disease_mixed_batches = var_disease_mixed_batches + (" -p prop_highrisk={}".format(prop_highrisk))
				if viral_infection != '' or viral_infection is not None:
					var_disease_mixed_batches = var_disease_mixed_batches + (" -p viral_infection={}".format(viral_infection))
				if pasteurella != '' or pasteurella is not None:
					var_disease_mixed_batches = var_disease_mixed_batches + (" -p pasteurella={}".format(pasteurella))
				if mycoplasma != '' or mycoplasma is not None:
					var_disease_mixed_batches = var_disease_mixed_batches + (" -p mycoplasma={}".format(mycoplasma))


				var_disease_mixed_batches = var_disease_mixed_batches + " -r 20"
				var_disease_mixed_batches = var_disease_mixed_batches + " -p sorted_batches=0"
				if batch_size != '' or batch_size is not None:
					var_disease_mixed_batches = var_disease_mixed_batches + (" -p batch_size={}".format(batch_size))
				if number_batches != '' or number_batches is not None:
					var_disease_mixed_batches = var_disease_mixed_batches + (" -p number_batches={}".format(number_batches))

				self.request.session['var_disease_mixed_batches'] = var_disease_mixed_batches


				var_disease_sorted_batches = var 

				dict_non_emulsion_parameters["disease_sorted_batches"]={"cost_of_antiobiotic_dose" : cost_of_antiobiotic_dose ,"cost_purchase_calf" : cost_purchase_calf ,"selling_price_bull" : selling_price_bull ,"total_feeding_cost" : total_feeding_cost ,"daily_disease_impact" : daily_disease_impact }
				dict_non_emulsion_parameters_in_scenario["disease_sorted_batches"]={}
				self.request.session['dict_non_emulsion_parameters_initial_conditions'] = dict_non_emulsion_parameters
				self.request.session['dict_non_emulsion_parameters_in_scenario_initial_conditions'] = dict_non_emulsion_parameters_in_scenario
				if mean_dur_CS != '' or mean_dur_CS is not None:
					var_disease_sorted_batches = var_disease_sorted_batches + (" -p mean_dur_CS={}".format(mean_dur_CS))
				if p_severe_clinical != '' or p_severe_clinical is not None:
					var_disease_sorted_batches = var_disease_sorted_batches + (" -p p_severe_clinical={}".format(p_severe_clinical))
				if dur_I_shape != '' or dur_I_shape is not None:
					var_disease_sorted_batches = var_disease_sorted_batches + (" -p dur_I_shape={}".format(dur_I_shape))
				if dur_I_scale != '' or dur_I_scale is not None:
					var_disease_sorted_batches = var_disease_sorted_batches + (" -p dur_I_scale={}".format(dur_I_scale))
				if p_reactivation_HighRisk != '' or p_reactivation_HighRisk is not None:
					var_disease_sorted_batches = var_disease_sorted_batches + (" -p p_reactivation_HighRisk={}".format(p_reactivation_HighRisk))
				if p_reactivation_MediumRisk != '' or p_reactivation_MediumRisk is not None:
					var_disease_sorted_batches = var_disease_sorted_batches + (" -p p_reactivation_MediumRisk={}".format(p_reactivation_MediumRisk))
				if p_reactivation_LowRisk != '' or p_reactivation_LowRisk is not None:
					var_disease_sorted_batches = var_disease_sorted_batches + (" -p p_reactivation_LowRisk={}".format(p_reactivation_LowRisk))
				if mean_dur_E != '' or mean_dur_E is not None:
					var_disease_sorted_batches = var_disease_sorted_batches + (" -p mean_dur_E={}".format(mean_dur_E))
				if transmission_lowrisk1 != '' or transmission_lowrisk1 is not None:
					var_disease_sorted_batches = var_disease_sorted_batches + (" -p transmission_lowrisk1={}".format(transmission_lowrisk1))
				if transmission_mediumrisk1 != '' or transmission_mediumrisk1 is not None:
					var_disease_sorted_batches = var_disease_sorted_batches + (" -p transmission_mediumrisk1={}".format(transmission_mediumrisk1))
				if transmission_highrisk1 != '' or transmission_highrisk1 is not None:
					var_disease_sorted_batches = var_disease_sorted_batches + (" -p transmission_highrisk1={}".format(transmission_highrisk1))
				if interbatch_contact_rate != '' or interbatch_contact_rate is not None:
					var_disease_sorted_batches = var_disease_sorted_batches + (" -p interbatch_contact_rate={}".format(interbatch_contact_rate))
				if p_recovery != '' or p_recovery is not None:
					var_disease_sorted_batches = var_disease_sorted_batches + (" -p p_recovery={}".format(p_recovery))
				if proportion_carrier_low != '' or proportion_carrier_low is not None:
					var_disease_sorted_batches = var_disease_sorted_batches + (" -p proportion_carrier_low={}".format(proportion_carrier_low))
				if proportion_carrier_medium != '' or proportion_carrier_medium is not None:
					var_disease_sorted_batches = var_disease_sorted_batches + (" -p proportion_carrier_medium={}".format(proportion_carrier_medium))
				if proportion_carrier_high != '' or proportion_carrier_high is not None:
					var_disease_sorted_batches = var_disease_sorted_batches + (" -p proportion_carrier_high={}".format(proportion_carrier_high))
				if proportion_recovered_low != '' or proportion_recovered_low is not None:
					var_disease_sorted_batches = var_disease_sorted_batches + (" -p proportion_recovered_low={}".format(proportion_recovered_low))
				if proportion_recovered_medium != '' or proportion_recovered_medium is not None:
					var_disease_sorted_batches = var_disease_sorted_batches + (" -p proportion_recovered_medium={}".format(proportion_recovered_medium))
				if proportion_recovered_high != '' or proportion_recovered_high is not None:
					var_disease_sorted_batches = var_disease_sorted_batches + (" -p proportion_recovered_high={}".format(proportion_recovered_high))
				if prop_lowrisk != '' or prop_lowrisk is not None:
					var_disease_sorted_batches = var_disease_sorted_batches + (" -p prop_lowrisk={}".format(prop_lowrisk))
				if prop_mediumrisk != '' or prop_mediumrisk is not None:
					var_disease_sorted_batches = var_disease_sorted_batches + (" -p prop_mediumrisk={}".format(prop_mediumrisk))
				if prop_highrisk != '' or prop_highrisk is not None:
					var_disease_sorted_batches = var_disease_sorted_batches + (" -p prop_highrisk={}".format(prop_highrisk))
				if viral_infection != '' or viral_infection is not None:
					var_disease_sorted_batches = var_disease_sorted_batches + (" -p viral_infection={}".format(viral_infection))
				if pasteurella != '' or pasteurella is not None:
					var_disease_sorted_batches = var_disease_sorted_batches + (" -p pasteurella={}".format(pasteurella))
				if mycoplasma != '' or mycoplasma is not None:
					var_disease_sorted_batches = var_disease_sorted_batches + (" -p mycoplasma={}".format(mycoplasma))


				var_disease_sorted_batches = var_disease_sorted_batches + " -r 20"
				var_disease_sorted_batches = var_disease_sorted_batches + " -p sorted_batches=1"
				if batch_size != '' or batch_size is not None:
					var_disease_sorted_batches = var_disease_sorted_batches + (" -p batch_size={}".format(batch_size))
				if number_batches != '' or number_batches is not None:
					var_disease_sorted_batches = var_disease_sorted_batches + (" -p number_batches={}".format(number_batches))

				self.request.session['var_disease_sorted_batches'] = var_disease_sorted_batches



			if self.steps.current == '1':

				selecting_scenarios_to_run = self.request.POST.getlist('1-selecting_scenarios_to_run')

				self.request.session['selecting_scenarios_to_run'] = selecting_scenarios_to_run

			if self.steps.current == '2':

				total_duration = self.request.POST.get('2-total_duration')

				simulation_name = self.request.POST.get('2-simulation_name')

				simulation_description = self.request.POST.get('2-simulation_description')


				total_duration = int(total_duration)

				self.request.session['total_duration'] = total_duration

				self.request.session['simulation_name'] = simulation_name
				BASE_DIR = "/home/gniang/renaming_atom_to_paste/brd_project"
				shinyapp_file = BASE_DIR + "/shinyapp/" + "app.R"
				for line in fileinput.input(shinyapp_file, inplace=True):
					print(re.sub(r'simulation_name <- ".+"', 'simulation_name <- \"{}\"'.format(simulation_name), line.rstrip()))
				
				scenario_dict={'no_disease_mixed_batches': [{'proportion_carrier_low': 0}, {'proportion_carrier_medium': 0}, {'proportion_carrier_high': 0}, {'sorted_batches': 0}], 'no_disease_sorted_batches': [{'proportion_carrier_low': 0}, {'proportion_carrier_medium': 0}, {'proportion_carrier_high': 0}, {'sorted_batches': 1}], 'disease_mixed_batches': [{'sorted_batches': 0}], 'disease_sorted_batches': [{'sorted_batches': 1}]}


			if self.steps.current == self.steps.last:
				return self.render_done(form, **kwargs)
			else:
				return self.render_next_step(form)

		return self.render(form)

	def done(self, form_list, form_dict, **kwargs):
		var_no_disease_mixed_batches = self.request.session['var_no_disease_mixed_batches']
		var_no_disease_sorted_batches = self.request.session['var_no_disease_sorted_batches']
		var_disease_mixed_batches = self.request.session['var_disease_mixed_batches']
		var_disease_sorted_batches = self.request.session['var_disease_sorted_batches']

		output_directory_path = "/home/gniang/renaming_atom_to_paste/brd_project/emulsion_outputs/"
		simulation_name = str(self.request.session['simulation_name'])

		var_no_disease_mixed_batches = var_no_disease_mixed_batches + (" -t ") + str(int(self.request.session['total_duration'])) + " --output-dir " + output_directory_path + "output_" + simulation_name + "/output_" + "no_disease_mixed_batches"
		var_no_disease_sorted_batches = var_no_disease_sorted_batches + (" -t ") + str(int(self.request.session['total_duration'])) + " --output-dir " + output_directory_path + "output_" + simulation_name + "/output_" + "no_disease_sorted_batches"
		var_disease_mixed_batches = var_disease_mixed_batches + (" -t ") + str(int(self.request.session['total_duration'])) + " --output-dir " + output_directory_path + "output_" + simulation_name + "/output_" + "disease_mixed_batches"
		var_disease_sorted_batches = var_disease_sorted_batches + (" -t ") + str(int(self.request.session['total_duration'])) + " --output-dir " + output_directory_path + "output_" + simulation_name + "/output_" + "disease_sorted_batches"

		list_scenarios_name = ['no_disease_mixed_batches', 'no_disease_sorted_batches', 'disease_mixed_batches', 'disease_sorted_batches']
		scenario_output_directory = {}
		for item in list_scenarios_name:
			scenario_output_directory[item] = output_directory_path + "output_" + simulation_name + "/output_" + item + "/counts.csv"

		cmd_dict = {'var_no_disease_mixed_batches' : var_no_disease_mixed_batches,'var_no_disease_sorted_batches' : var_no_disease_sorted_batches,'var_disease_mixed_batches' : var_disease_mixed_batches,'var_disease_sorted_batches' : var_disease_sorted_batches}
		scenario_not_optional = ['var_no_disease_mixed_batches', 'var_no_disease_sorted_batches']
		list_scenarios_name_to_run = []

		for scenario in scenario_not_optional:
			for key, value in cmd_dict.items():
				if scenario == key:
					list_scenarios_name_to_run.append(key)

		scenario_optional_list = self.request.session['selecting_scenarios_to_run']
		for scenario_optional in scenario_optional_list:
			var_scenario_optional = "var_" + scenario_optional
			for keys, values in cmd_dict.items():
				if var_scenario_optional == keys:
					list_scenarios_name_to_run.append(var_scenario_optional)

		self.request.session['dict_non_emulsion_parameters'] ={**self.request.session['dict_non_emulsion_parameters_initial_conditions']}
		self.request.session['dict_non_emulsion_parameters_in_scenario'] ={**self.request.session['dict_non_emulsion_parameters_in_scenario_initial_conditions']}
		for dict_non_emulsion_parameters_key, dict_non_emulsion_parameters_value in self.request.session['dict_non_emulsion_parameters'].items():
			for dict_non_emulsion_parameters_in_scenario_key, dict_non_emulsion_parameters_in_scenario_value in self.request.session['dict_non_emulsion_parameters_in_scenario'].items():
				if dict_non_emulsion_parameters_in_scenario_key == dict_non_emulsion_parameters_key:
					for key, value in dict_non_emulsion_parameters_value.items():
						for key1, value1 in dict_non_emulsion_parameters_in_scenario_value.items():
							if key == key1:
								self.request.session['dict_non_emulsion_parameters'][dict_non_emulsion_parameters_key].update(dict_non_emulsion_parameters_in_scenario_value)

		dict_eco= {}
		for key, values in self.request.session['dict_non_emulsion_parameters'].items():
			new_list = []
			for key_value, value_value in values.items():
				if value_value is not None:
					new_list.append(str(value_value))
			dict_eco[key] = new_list

		dict_each_scenario = {}
		for item in list_scenarios_name_to_run:
			list_each_scenario = []
			list_each_scenario.append(scenario_output_directory[item.replace('var_', '')])
			list_each_scenario.append(item.replace('var_',''))
			list_each_scenario.append(simulation_name)
			for number in dict_eco[item.replace('var_', '')]:
				list_each_scenario.append(number)
			dict_each_scenario[item.replace('var_', '')] = list_each_scenario

		BASE_DIR = "/home/gniang/renaming_atom_to_paste/brd_project"
		app_name = "brd_app"
		list_scenarios_name_comp = ' '.join(list_scenarios_name_to_run).replace('var_','').split()
		list_scenario_comparison =[BASE_DIR + "/" + app_name + "/" + "Rscripts/output_Rdata_" + simulation_name + "/" + item + ".Rdata" for item in list_scenarios_name_comp]
		list_scenario_comparison.append(simulation_name)

		task = chord([chain(run_scenario.si(cmd_dict[item]), run_statistics_for_each_scenario.si(dict_each_scenario[item.replace('var_', '')])) for item in list_scenarios_name_to_run],run_scenarios_comparison.si(list_scenario_comparison))()
		context={}
		context['task_id'] = task.id

		return render(self.request, 'done.html', context)

class TaskView(View):

	def get(self, request, task_id):
		task = current_app.AsyncResult(task_id)
		response_data = {'task_status': task.status, 'task_id': task.id}
		if task.status == 'SUCCESS':
			response_data['results'] = task.get()
		return JsonResponse(response_data)

def dashapp(request):
	return render(request, 'result.html')

def shiny(request):
	return render(request, 'shiny.html')

def shiny_contents(request):
	response = requests.get('http://localhost:8100')
	soup = BeautifulSoup(response.content, 'html.parser')
	return JsonResponse({'html_contents': str(soup)})

def load_risk_level_details(request):
	risk_level_id = request.GET.get('risk_level', None)
	data = {
		'is_taken': list(risk_level.objects.filter(id=risk_level_id).values())
	}
	return JsonResponse(data, safe=False)


def load_pathogen_details(request):
	pathogen_id = request.GET.get('pathogen', None)
	data = {
		'is_taken': list(pathogen.objects.filter(id=pathogen_id).values())
	}
	return JsonResponse(data, safe=False)


def load_batch_isolation_details(request):
	batch_isolation_id = request.GET.get('batch_isolation', None)
	data = {
		'is_taken': list(batch_isolation.objects.filter(id=batch_isolation_id).values())
	}
	return JsonResponse(data, safe=False)
