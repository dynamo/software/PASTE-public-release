from django import forms 
from django.forms.widgets import Select, HiddenInput, CheckboxInput, NumberInput, TextInput 
from .models import *
from django_select2.forms import ModelSelect2Widget

class initial_conditions(forms.Form):
	risk_level= forms.ModelChoiceField(queryset=risk_level.objects.all(),label="Risk level of calves",required=True,help_text="BRD risk associated with calves at purchase",widget=forms.Select(attrs={'class': 'form-control'}))
	prop_lowrisk=forms.FloatField(widget=forms.HiddenInput(),required=False)
	prop_mediumrisk=forms.FloatField(widget=forms.HiddenInput(),required=False)
	prop_highrisk=forms.FloatField(widget=forms.HiddenInput(),required=False)
	batch_isolation= forms.ModelChoiceField(queryset=batch_isolation.objects.all(),label="Batch isolation",required=True,help_text="Level of isolation between batches",widget=forms.Select(attrs={'class': 'form-control'}))
	interbatch_contact_rate=forms.FloatField(widget=forms.HiddenInput(),required=False)
	pathogen= forms.ModelChoiceField(queryset=pathogen.objects.all(),label="Main pathogen",required=True,help_text="Pathogen identified as major cause of BRD",widget=forms.Select(attrs={'class': 'form-control'}))
	mean_dur_CS=forms.IntegerField(widget=forms.HiddenInput(),required=False)
	viral_infection=forms.IntegerField(widget=forms.HiddenInput(),required=False)
	pasteurella=forms.IntegerField(widget=forms.HiddenInput(),required=False)
	mycoplasma=forms.IntegerField(widget=forms.HiddenInput(),required=False)
	p_severe_clinical=forms.FloatField(widget=forms.HiddenInput(),required=False)
	dur_I_shape=forms.FloatField(widget=forms.HiddenInput(),required=False)
	dur_I_scale=forms.FloatField(widget=forms.HiddenInput(),required=False)
	p_reactivation_HighRisk=forms.FloatField(widget=forms.HiddenInput(),required=False)
	p_reactivation_MediumRisk=forms.FloatField(widget=forms.HiddenInput(),required=False)
	p_reactivation_LowRisk=forms.FloatField(widget=forms.HiddenInput(),required=False)
	mean_dur_E=forms.IntegerField(widget=forms.HiddenInput(),required=False)
	transmission_lowrisk1=forms.FloatField(widget=forms.HiddenInput(),required=False)
	transmission_mediumrisk1=forms.FloatField(widget=forms.HiddenInput(),required=False)
	transmission_highrisk1=forms.FloatField(widget=forms.HiddenInput(),required=False)
	p_recovery=forms.FloatField(widget=forms.HiddenInput(),required=False)
	proportion_carrier_low=forms.FloatField(widget=forms.HiddenInput(),required=False)
	proportion_carrier_medium=forms.FloatField(widget=forms.HiddenInput(),required=False)
	proportion_carrier_high=forms.FloatField(widget=forms.HiddenInput(),required=False)
	proportion_recovered_low=forms.FloatField(widget=forms.HiddenInput(),required=False)
	proportion_recovered_medium=forms.FloatField(widget=forms.HiddenInput(),required=False)
	proportion_recovered_high=forms.FloatField(widget=forms.HiddenInput(),required=False)
	cost_of_antiobiotic_dose=forms.IntegerField(label="Cost of antiobiotic dose",required=True,help_text="cost_of_antiobiotic_dose",widget=NumberInput(attrs={'class': 'form-control'}),initial=10)
	cost_purchase_calf=forms.IntegerField(label="Cost of a 7 month-beef calf",required=True,help_text="Cost of a 7 month-beef calf",widget=NumberInput(attrs={'class': 'form-control'}),initial=950)
	selling_price_bull=forms.IntegerField(label="Selling price of fattened bull",required=True,help_text="selling_price_bull",widget=NumberInput(attrs={'class': 'form-control'}),initial=1330)
	total_feeding_cost=forms.IntegerField(label="Total feeding cost",required=True,help_text="total_feeding_cost",widget=NumberInput(attrs={'class': 'form-control'}),initial=312)
	daily_disease_impact=forms.FloatField(label="Daily disease impact (€)",required=True,help_text="daily_disease_impact",widget=NumberInput(attrs={'class': 'form-control', 'step': 0.0001, 'max': 1.0, 'min': 0.0}),initial=0.2)
	number_batches=forms.IntegerField(label="Number of simultaneous batches",required=True,widget=NumberInput(attrs={'data-min': 1, 'data-step': 1, 'data-value': 3, 'data-skin': 'round', 'data-label': 'batches', 'data-grid': 'true', 'data-grid-num': 9, 'data-max': 10, 'data-postfix': ' batches', 'class': 'js-range-slider', 'data-from': 3}),initial=3)
	batch_size=forms.IntegerField(label="Number of calves per batch",required=True,widget=NumberInput(attrs={'data-min': 0, 'data-step': 1, 'data-value': 10, 'data-skin': 'round', 'data-label': 'animals per batch', 'data-grid': 'true', 'data-grid-num': 6, 'data-max': 30, 'data-postfix': ' animals per batch', 'class': 'js-range-slider', 'data-from': 10}),initial=10)

class select_scenario(forms.Form):
	selecting_scenarios_to_run = forms.MultipleChoiceField(
	required=False,
	help_text="When a box is selected, this scenario is added to the reference scenarios and run in Emulsion",
	widget=forms.CheckboxSelectMultiple(attrs={'class': 'inline'}),
	choices=(('disease_mixed_batches', 'disease_mixed_batches'), ('disease_sorted_batches', 'disease_sorted_batches')))
class execution(forms.Form):
	total_duration=forms.IntegerField(label="Simulation duration (half days)",required=True,widget=NumberInput(attrs={'data-min': 0, 'data-step': 2, 'data-value': 60, 'data-skin': 'round', 'data-label': 'half days', 'data-grid': 'true', 'data-grid-num': 5, 'data-max': 100, 'data-postfix': ' half days', 'class': 'js-range-slider'}))
	simulation_name=forms.CharField(label="Simulation name",required=True,widget=forms.TextInput(attrs={'class': 'form-control'}))
	simulation_description=forms.CharField(label="Simulation description",required=False,widget=forms.Textarea(attrs={'class': 'form-control'}))
