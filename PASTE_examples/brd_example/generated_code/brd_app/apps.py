from django.apps import AppConfig


class BrdAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'brd_app'
