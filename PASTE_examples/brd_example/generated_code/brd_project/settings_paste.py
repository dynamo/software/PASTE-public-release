from .settings import *
INSTALLED_APPS.extend(['channels','formtools','django_select2', 'mathfilters'])
ASGI_APPLICATION = 'brd_project.routing.application'
CHANNEL_LAYERS = {
		'default': {
			'BACKEND': 'channels_redis.core.RedisChannelLayer',
			'CONFIG': {
				'hosts': [('127.0.0.1', 6379),],
		}
	}
}
#Register your new serializer methods into kombu
from kombu.serialization import register
from .myjson import my_dumps, my_loads
register('myjson', my_dumps, my_loads,
		content_type='application/x-myjson',
		content_encoding='utf-8')
#Tell celery to use your new serializer:
CELERY_ACCEPT_CONTENT = ['myjson']
CELERY_TASK_SERIALIZER = 'myjson'
CELERY_RESULT_SERIALIZER = 'myjson'
#celery
CELERY_BROKER_URL = 'redis://localhost:6379'
CELERY_RESULT_BACKEND = 'redis://localhost:6379'
SESSION_SERIALIZER = 'django.contrib.sessions.serializers.PickleSerializer'
import os
STATICFILES_DIRS = [
	os.path.join(BASE_DIR, 'brd_app/static'),
]
STATIC_ROOT  = os.path.join(BASE_DIR, 'staticfiles')