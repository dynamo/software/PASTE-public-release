from   os.path        import exists
import numpy as np

from emulsion.agent.managers import MultiProcessManager
from emulsion.tools.debug    import debuginfo

#===============================================================
# CLASS Pen (LEVEL 'pen')
#===============================================================
class Pen(MultiProcessManager):

    """Level of the pen.

    """

    #----------------------------------------------------------------
    # Level initialization
    #----------------------------------------------------------------
    def initialize_level(self, **others):
        """Initialize an instance of Pen. Especially, after all regular
        initial conditions have been applied, pick random animals
        according to initial prevalence to infect them. At least one animal will be infected.

        """


        ## TODO revise calculation
        lowp = self.get_model_value('initial_prevalence_LowRisk')
        mediump = self.get_model_value('initial_prevalence_MediumRisk')
        highp = self.get_model_value('initial_prevalence_HighRisk')

        nb_lowrisk = int(self.get_information('total_LowRisk'))
        nb_mediumrisk = int(self.get_information('total_MediumRisk'))
        nb_highrisk = int(self.get_information('total_HighRisk'))

        expected_mean = lowp * nb_lowrisk + mediump * nb_mediumrisk + highp * nb_highrisk

        if self.get_model_value('use_antibioprevention'):
            expected_mean = expected_mean * self.get_model_value('antibioprevention_prevalence_factor')

        debuginfo("Expected mean = ", expected_mean)
        # choose how many animals will get infected
        if expected_mean < 1:
            nb_to_infect = 1
        else:
            if np.random.random_sample() < expected_mean - np.floor(expected_mean):
                nb_to_infect = int(np.ceil(expected_mean))
            else:
                nb_to_infect = int(np.floor(expected_mean))
        debuginfo("Nb to infect = ", nb_to_infect)

        available = int(self.get_model_value('pen_size'))
        ratios = [lowp * nb_lowrisk / available, mediump * nb_mediumrisk / available, highp * nb_highrisk / available]
        if sum(ratios) == 0:
            probas = [0, 0, 0]
        else:
            probas = [val / sum(ratios) for val in ratios]
        debuginfo("Probas:", *probas)
        nb_low_to_infect, nb_medium_to_infect, nb_high_to_infect = np.random.multinomial(nb_to_infect, probas)
        debuginfo('Infecting', nb_low_to_infect, 'low-risk animals over', nb_lowrisk)
        debuginfo('Infecting', nb_medium_to_infect, 'medium-risk animals over', nb_mediumrisk)
        debuginfo('Infecting', nb_high_to_infect, 'high-risk animals over', nb_highrisk)

        l_animals_to_infect = []

        l_random_animals_low = self.select_atoms(variable='risk_status', state='LowRisk')
        np.random.shuffle(l_random_animals_low)
        l_animals_to_infect += l_random_animals_low[:nb_low_to_infect]

        l_random_animals_medium = self.select_atoms(variable='risk_status', state='MediumRisk')
        np.random.shuffle(l_random_animals_medium)
        l_animals_to_infect += l_random_animals_medium[:nb_medium_to_infect]

        l_random_animals_high = self.select_atoms(variable='risk_status', state='HighRisk')
        np.random.shuffle(l_random_animals_high)
        l_animals_to_infect += l_random_animals_high[:nb_high_to_infect]

        debuginfo('infecting:', l_animals_to_infect)
        for animal in l_animals_to_infect:
            animal.apply_prototype('introduced_infected_beef', execute_actions=True)

    # def finalize_level(self, simulation=None, **others):
    #     """Store final information in log file"""
    #     log_path = simulation.log_path()
    #     header = not(exists(log_path))
    #     values = [
    #         self.statevars.simu_id,
    #         self.statevars.step,
    #         self.get_information("total_pen"),
    #         self.get_information("pen_time_first_detection"),
    #         self.get_information("pen_nb_deaths"),
    #         self.get_information("pen_force_of_infection"),
    #         self.get_information("pen_nb_detections"),
    #         self.get_information("pen_nb_false_positive"),
    #         self.get_information("pen_nb_treated_distinct"),
    #         self.get_information("pen_nb_infected_distinct"),
    #         self.get_information("pen_infection_duration"),
    #         self.get_information("pen_hyperthermia_duration"),
    #         self.get_information("episode_start_time"),
    #         self.get_information("pen_nb_doses"),
    #         self.get_information("pen_nb_clinical_episodes"),
    #         self.get_information("pen_nb_infection_episodes"),
    #         self.get_information("pen_nb_hyperthermia_episodes"),
    #         self.get_information("pen_nb_treatment_episodes"),
    #         self.get_information("pen_nb_clinical_detections"),
    #         ]
    #     message = (','.join(['{}'] * len(values))).format(*values)
    #     with open(self.log_path(), 'a') as logfile:
    #         if header:
    #             logfile.write('{}\n'.format(",".join([
    #                 "simu_id",
    #                 "step",
    #                 "total_pen",
    #                 "pen_time_first_detection",
    #                 "pen_nb_deaths",
    #                 "pen_force_of_infection",
    #                 "pen_nb_detections",
    #                 "pen_nb_false_positive",
    #                 "pen_nb_treated_distinct",
    #                 "pen_nb_infected_distinct",
    #                 "pen_infection_duration",
    #                 "pen_hyperthermia_duration",
    #                 "episode_start_time",
    #                 "pen_nb_doses",
    #                 "pen_nb_clinical_episodes",
    #                 "pen_nb_infection_episodes",
    #                 "pen_nb_hyperthermia_episodes",
    #                 "pen_nb_treatment_episodes",
    #                 "pen_nb_clinical_detections",
    #             ])))
    #         logfile.write('{}\n'.format(message))
